import {Component, OnInit, Injector, ElementRef, ViewChild, AfterViewInit, OnDestroy, Renderer2} from '@angular/core';
import {BaseComponent} from '../../share/components/common.component';
import {ActivatedRoute, NavigationEnd} from '@angular/router';
import {BetplaceComponent} from '../../share/components/bottom-sheet/betplace.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {AlertService, APIService, SharedataService} from '../../core/services';

declare let $;

@Component({
  selector: 'app-binary',
  templateUrl: './binary.component.html',
  styleUrls: ['./binary.component.css']
})
export class BinaryComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('pRef', {static: false}) pRef: ElementRef;
  @ViewChild('bookmakerRef', {static: false}) bookmakerRef: ElementRef;
  @ViewChild('fancyRef', {static: false}) fancyRef: ElementRef;
  @ViewChild('fancy2Ref', {static: false}) fancy2Ref: ElementRef;
  @ViewChild('fancy3Ref', {static: false}) fancy3Ref: ElementRef;
  @ViewChild('khadoRef', {static: false}) khadoRef: ElementRef;
  @ViewChild('ballbyballRef', {static: false}) ballbyballRef: ElementRef;
  @ViewChild('profitLossModal', {static: false}) profitLossModal: ElementRef;


  public oldGdata: any = {};
  public oldGdataCnt: number = 0;
  public mHtml: string;

  // **VARIABLE-DECLARATION************************************

  // **DATA-STORAGE************************************
  public matchData: any;
  public oddsData: any;
  public marketArr: any;
  public currantBets: any;
  public fancy2PL: any = [];
  public fancyBets: any = [];
  public primaryColor: string;

  // **PROFIT-LOSS BOOK-GLOBLE************************************
  public GAME_BOOKDATA: any = [];

  // **SINGLE-LINE-DATA************************************
  public eventId: string;
  public eventTitle: string;
  public marketId: string;
  public localComentory: string;
  public eventMsg: string;
  public updatedTime: number;
  public annimationClass: string;

  // **FLAGE************************************
  public isScoreBoard: boolean;
  public isTv: boolean;
  public isTabs: string;
  public isFirstLoad: boolean;
  public isDestroy: boolean;
  public isLoadFirst: boolean;
  public isLoadFancyPl: boolean;
  public isFancy3: boolean;
  public loaderShow: boolean;
  public isActivePlaceBet: boolean;
  public isloadBetList: boolean;

  // **INTERVAL******************************
  public interval1000: any;
  public interval300: any;
  public isActivePlaceBetIndex: string;
  public isActivePlaceBetSType: string;
  private onUpdate: number;
  private navigationSubscription;

  constructor(inj: Injector, private route: ActivatedRoute,
              private alert: AlertService,
              private endpoint: APIService,
              private share: SharedataService,
              private elRef: ElementRef,
              private renderer: Renderer2,
              private placeBetSheet: MatBottomSheet) {
    super(inj);
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.eventId = this.route.snapshot.params.id;
    this.share.eventId.emit(this.route.snapshot.params.id);
    this.marketArr = [];
    this.valInit();
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        console.log(this.activatedRoute.snapshot.params.id);
        if (this.eventId !== undefined && this.eventId !== this.activatedRoute.snapshot.params.id) {
          this.reloadCurrentRoute();
        }
      }
    });
  }

  ngOnInit() {
    this.spinner.show();
    this.getEventDetail();

    this.share.betStatus.subscribe((res) => { // Bet Flags Manage
      if (res === 1) {
        this.isActivePlaceBet = true;
      } else if (res === 2) {
        this.isActivePlaceBet = false;
        this.spinner.show();
      } else if (res === 3) {
        this.spinner.hide();
        // this.getBetList();
      }
    });
  }

  ngAfterViewInit(): void {
    // this.commonService.betslist.subscribe((res) => {
    //     this.currantBets = res;
    // });
    // this.getBetList();
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
    this.matchData = [];
    this.marketArr = [];
    this.oddsData = [];
    clearTimeout(this.interval1000);
    clearTimeout(this.interval300);
    this.share.eventId.emit('');
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  valInit() {
    this.isScoreBoard = false;
    this.isTv = false;
    this.isTabs = 'live-market';
    this.isFirstLoad = false;
    this.isDestroy = false;
    this.isLoadFirst = true;
    this.annimationClass = 'fadeInUp';
    this.isActivePlaceBet = false;
  }

  reloadCurrentRoute() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  changeTabs(tab: string) {
    this.isTabs = tab;
    this.share.setRouterChange('1'); // CALL TO HEADER API
    this.getBetList();
  }

  getEventDetail(): void {
    this.endpoint.getBinaryDetail(this.eventId).subscribe((res) => {
      if (res.status === 1 && res.data != null) {
        try {
          this.matchData = res.data.items;
          // this.marketArr = res.data.marketIdsArr; // SET MARKET ARRAY
          this.eventTitle = res.data.items.title; // SET MARKET ARRAY

          this.eventMsg = '';
          if (res.data.marketIdsArr === null) {
            res.data.marketIdsArr = [];
            this.spinner.hide();
            this.eventMsg = 'No Data';
          }

          if (this.marketArr.length === 0) { // INIT MARKET ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
          } else if (this.marketArr.length !== res.data.marketIdsArr.length) { // CHECK DIFF. OF CURRANT AND OLD ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
            clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          } else if (this.marketArr.length === res.data.marketIdsArr.length && res.data.items.ballbyball) {
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = true;
            // clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          }

          if (!this.isFirstLoad && this.marketArr.length) { // LOAD FIRST TIME ODDS FUNCTIONS
            this.changedRef.detectChanges();
            this.isFirstLoad = true;
            this.getOdds();
          }


          if (this.onUpdate === undefined && res.updatedOn !== null) {  // set gameover time
            this.onUpdate = res.updatedOn;
          } else if (this.onUpdate !== null && this.onUpdate !== res.updatedOn) { // CHECK IF GAMEOVER TIME CHANGE OR NOT
            this.onUpdate = res.updatedOn;
            this.share.setRouterChange('3');
            // this.getBetList();
          }
          this.clearSesssionBind();
        } catch (e) {
          console.log('Failed to set event data', e);
        }

      } else {  // AFTER GAME OVER  TO RECALL OPEN EVENT
        this.clearSesssionBind();
        console.error('Error-detail1', res.message);
        this.eventMsg = res.msg;
        this.eventTitle = '';
        this.matchData = [];
        this.marketArr = [];
        if (!this.isDestroy && this.eventMsg !== '') {
          this.interval1000 = setTimeout(() => {
            this.getEventDetail();
            // tslint:disable-next-line:radix
          }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
        }
      }
      if (!this.isDestroy) { // API CALL TO GET REAL TIME UPDATES
        this.interval1000 = setTimeout(() => {
          this.getEventDetail();
          // tslint:disable-next-line:radix
        }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
      }
    }, (error) => {
      if (!this.loaderShow) {
        this.loaderShow = true;
        this.spinner.hide();
      }
      this.clearSesssionBind();
      console.log('Error-detail', error);
    });
  }

  getOdds() {
    this.endpoint.getOddsData(this.marketArr).subscribe((resData) => {
        try {
          this.oddsData = resData.data.items;
          // this.matchData.match_odd.marketId

          if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
            this.share.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
          }

          if (resData.data.items.binary && this.matchData.binary) {
            this.bindBinaryData(resData.data.items.binary, this.matchData.binary);
          }

          const tThis = this;
          const clickButtons = this.elRef.nativeElement.querySelectorAll('.bet-sec');
          const clickButtonsModelPl = this.elRef.nativeElement.querySelectorAll('.bookModalDetail');
          for (let i = 0; i < clickButtons.length; i++) {
            this.renderer.listen(clickButtons[i], 'click', ($event) => {
              tThis._placebet(clickButtons[i].dataset);
            });
          }

          for (let i = 0; i < clickButtonsModelPl.length; i++) {
            this.renderer.listen(clickButtonsModelPl[i], 'click', ($event) => {
              tThis.getFancyTwoPL(clickButtonsModelPl[i].dataset);
            });
          }

          this.annimationClass = '';
          if (!this.isDestroy) { // && this.isScreenActive
            this.interval300 = setTimeout(() => {
              clearTimeout(this.interval300);
              this.getOdds();
            }, this.getToken('oddsTiming') !== null ? parseInt(this.getToken('oddsTiming')) : 400); //
          }
          if (!this.loaderShow) {
            this.loaderShow = true;
            this.spinner.hide();
          }
        } catch (e) {
          console.log('Failed to set odds data', e);
        }
      },
      (error) => {
        if (!this.loaderShow) {
          this.loaderShow = true;
          this.spinner.hide();
        }
        console.log('error-Odds', error);
      });
  }

  clearSesssionBind() {
    if (this.matchData.binary === null) {
      this.fancy2Ref.nativeElement.innerHTML = '';
    }
  }

  bindBinaryData(dataOdd, fancy2) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      if (this.oldGdataCnt === 0) { // INIT ODDS DATA
        this.oldGdata = dataOdd;
        this.oldGdataCnt = 1;
      } else if (this.oldGdataCnt && this.oldGdata.length !== dataOdd.length) {
        this.oldGdata = dataOdd;
        this.oldGdataCnt = 2;
      }
      let fancy2DataHtml = '';
      fancy2DataHtml = '<div class="market-section">\n' +
        '<div class="row align-items-center text-uppercase text-dark market-title-row">\n' +
        '<div class="col-md-7 col-7"><p class="market-title">Binary</p></div>\n' +
        '<div class="col-md-5 text-center col-5">\n' +
        '<p class="market-back"><span>no</span> <span>yes</span></p>\n' +
        '</div></div>';

      for (let i = 0; i < fancy2.length; i++) {
        let data: any = {};
        let indexOFMarket = -1;
        if (dataOdd[i].market_id === fancy2[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
          indexOFMarket = i;
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy2[i].marketId) {
              data = dataOdd[ii];
              indexOFMarket = ii;
              break; // <=== breaks out of the loop early
            }
          }
        }
        let noblink = '';
        let yesblink = '';
        if (data.no !== '0') { // CHECK PRICE CHANGE TO BLINK
          if (this.oldGdata[indexOFMarket].no !== data.no) {
            noblink = 'blink-lay';
          }
        }
        if (data.yes !== '0') { // CHECK PRICE CHANGE TO BLINK
          if (this.oldGdata[indexOFMarket].yes !== data.yes) {
            yesblink = 'blink-back';
          }
        }
        this.oldGdata[indexOFMarket].no = data.no; // REPLACE TO OLD TO NEW PRICE
        this.oldGdata[indexOFMarket].yes = data.yes;

        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy2[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy2DataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">\n' +
          '<div class="col-md-6 col-5"><p class="market-name">' + fancy2[i].title + '</p></div>';
        fancy2DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail"  ' + bookActiveStatus + ' data-title="' + fancy2[i].title + '"  data-marketid="' + data.market_id + '" data-mtype="' + fancy2[i].mType + '">book</button></div>';
        fancy2DataHtml += '<div class="col-md-5 text-center col-5"><div class="bl-buttons">';
        fancy2DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec ' + noblink + '" data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.no + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.no + ' <span>' + data.no_rate + '</span></button>';
        fancy2DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec ' + yesblink + '" data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.yes + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy2DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</span></div>';
        }
        fancy2DataHtml += '</div></div></div>';
        if (fancy2[i].info) {
          fancy2DataHtml += '<p class="alert-text">' + fancy2[i].info + '</p>';
        }
      }
      fancy2DataHtml += '</div>';
      this.fancy2Ref.nativeElement.innerHTML = fancy2DataHtml;
    } catch (e) {
      console.log('Failed to Bind Fancy-2 ELE', e);
    }
  }

  _placebet(e) {
    this.isActivePlaceBet = true;
    this.isActivePlaceBetIndex = e.index;
    this.isActivePlaceBetSType = e.slug;
    if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
      this.share.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
    }

    const secId = e.marketid;

    const initBet = {
      price: e.rate,
      size: '',
      runner: e.runnername,
      bet_type: e.btype,
      sec_id: secId,
      market_id: e.marketid,
      event_id: this.eventId,
      m_type: e.mtype,
      eventname: this.eventTitle,
      market_name: e.marketname,
      slug: e.slug,
      rate: e.rrate,
      sport_id: e.sportid
    };

    const plArr: any = [];
    // console.log(initBet);
    const plLimit = {minStack: e.minstack, maxStack: e.maxstack, maxProfitLimit: e.maxprofitlimit};
    this.placeBetSheet.open(BetplaceComponent, {
      disableClose: true,
      data: {int: initBet, market: plArr, limit: plLimit, index: e.index}
    });
  }

  // **Fancy two profit loss******************************/
  getFancyTwoPL(e) {
    this.isLoadFancyPl = false;
    $('#Book-Unbook').modal('show');
    // if (e.is_book.toLocaleString() === '1') {
    $('#session-title').html('Book <i class="mdi mdi-arrow-right"></i>' + e.title);
    this.fancy2PL = [];
    this.fancyBets = [];
    if (e.marketid) {
      const data = {event_id: this.eventId, market_id: e.marketid, session_type: e.mtype};
      this.endpoint.getBetList(data).subscribe((response: any) => {
        if (response.status === 1 && response.data.betList !== null) {
          const plData = this.fancyBookCal(response.data.betList);
          plData.forEach(item => {
            this.fancy2PL.push(item);
          });
          this.fancyBets = response.data.betList;
        }
        this.isLoadFancyPl = true;
        this.changedRef.detectChanges();
      });
      // }
    }
  }

  getBetList() {
    this.isloadBetList = true;
    $('#Matched-Unmatched').modal('show');
    this.endpoint.getMatchedUnmatchedBet(this.eventId).subscribe((res) => {
      if (res.status === 1 && res.data !== null && res.data.matched !== null && res.data.matched[15]) {
        try {
          this.currantBets = res.data.matched[15];
          this.isloadBetList = false;
        } catch (e) {
          console.log('Failed to get bet list', e);
        }
      } else {
        this.isloadBetList = false;
      }
      this.changedRef.detectChanges();
    });
  }
}
