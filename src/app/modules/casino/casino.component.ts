import {Component, OnInit, OnDestroy, Injector, ViewChild, ElementRef} from '@angular/core';
import {Location} from '@angular/common';
import {BaseComponent} from '../../share/components/common.component';
import {AuthService, SharedataService, APIService} from '../../core/services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-casino',
  templateUrl: './casino.component.html',
  styleUrls: ['./casino.component.css']
})
export class CasinoComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('iframeLoad', {static: false}) iframeLoad: ElementRef;
  public gameid: string;
  public userToken: string;
  public iurl: string;
  public yt: any;
  public isDestroy = false;
  public cDetails: any;
  public eventId = '';
  public loaderShow: boolean;
  public interval2000: any;
  public isScreenActive = true;
  public url: string;
  public isLoadFirst: boolean;

  constructor(inj: Injector,
              private share: SharedataService, private location: Location, private apiservice: APIService, private hapi: AuthService) {
    super(inj);
    this.gameid = this.activatedRoute.snapshot.params.id;
    this.loaderShow = true;
    this.isLoadFirst = true;

  }

  ngOnInit(): void {
    this.spinner.show();
    this.getBalance();
  }


  getBalance() { // user-data?type=teenpatti'
    this.hapi.headerCall().subscribe((response: any) => {
      console.log('BL-EX', response.data.balance.balance + ' - ' + response.data.balance.expose);
      this.share.setBalanceChange(response.data.balance.balance);
      this.share.setExposeChange(response.data.balance.expose);
      if (this.loaderShow && this.isLoadFirst) {
        this.isLoadFirst = false;
        this.getCasinoUrl();
      }
      if (!this.isDestroy) {
        this.interval2000 = setTimeout(() => {
          this.getBalance();
        }, this.sysConfigDt.GET_BALANCE_TIME || 1000);
      }
    });
  }

  getCasinoUrl() {
    const platformName = document.getElementsByTagName('body')[0];
    let platform1 = '';
    if (platformName.dataset.platform === 'desktop') {
      platform1 = 'GPL_DESKTOP';
    } else {
      platform1 = 'GPL_MOBILE';
    }
    const data = {gameId: this.gameid, platform: platform1};
    this.apiservice.getCasionUrl(data).subscribe((res) => {
      if (res.status === 1 && res.gameUrl !== null) {
        this.openCasino(res.gameUrl);
      }
    });
  }

  openCasino(iurl) { // INIT IFRAME
    this.loaderShow = false;
    this.yt = '<iframe class="w-100" src="' + iurl + '" frameborder="0" id="casino-fram" allow="autoplay; encrypted-media" allowfullscreen style="height: 100%;"></iframe>';
    this.iframeLoad.nativeElement.innerHTML = this.yt;
  }

  ngOnDestroy() {
    this.share.setRouterChange('1'); // CALL HEADER APIS
    this.isDestroy = true;
    clearTimeout(this.interval2000);
  }

  myWalletTansfre() {
    Swal.mixin({
      footer: '<a>Your balance Transfer </a>',
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1', '2'],
      allowOutsideClick: false,
      showLoaderOnConfirm: true
    }).queue([
      {
        title: 'Transaction Type',
        text: 'withdraw or deposit',
        input: 'select',
        inputOptions: ['Deposit', 'Withdraw'],
        inputPlaceholder: 'Select Type',
        inputValidator: (value) => {
          return new Promise((resolve) => {
            if (value !== '') {
              resolve();
            } else {
              resolve('You need to select type :)');
            }
          });
        }
      },
      {
        title: 'Balance',
        text: 'Transfer balance',
        input: 'number',
        preConfirm: (data) => {
          console.log('Balance', data);
          return new Promise((resolve) => {
            if (data !== '') {
              resolve();
            } else {
              resolve('You need to select type :)');
            }
          });
        },
        inputValidator: (value) => {
          if (!value) {
            return 'You need to write something!';
          }
        }
      }
    ]).then((result: any) => {
      if (result.value !== undefined && result.value[0] !== '' && result.value[1] !== '') {
        const answers = JSON.stringify(result.value);
        let msg = result.value[1] + ' add to your Casino balance';
        if (result.value[0] === '1') {
          msg = result.value[1] + ' add to  Your Main balance';
        }
        const trnc = result.value[0] === '0' ? 1 : 2;
        const data = {amount: result.value[1], transactionType: trnc};
        this.apiservice.fundTransfer(data).subscribe((res) => {
          if (res.status === 1) {
            Swal.fire({
              icon: 'success',
              title: res.message,
              html: msg,
              confirmButtonText: 'Done!'
            }).then(() => {
              this.getBalance();
              const ifram: any = document.getElementById('casino-fram');
              ifram.src += '';
            });
          } else {
            Swal.fire({
              title: 'Cancelled',
              html: res.message,
              icon: 'error'
            });
          }
        });
      }
      // } else {
      //   Swal.fire({
      //     title: 'Cancelled',
      //     html: 'Please fill input & try again :)',
      //     icon: 'error'
      //   }).then((data) => {
      //     this.myWalletTansfre();
      //   });
      // }
    });
  }

}
