import {Component, OnInit, Injector, ElementRef, TemplateRef, ViewChild, AfterViewInit, OnDestroy, Renderer2} from '@angular/core';
import {BaseComponent} from '../../share/components/common.component';
import {ActivatedRoute, NavigationEnd} from '@angular/router';
import {BetplaceComponent} from '../../share/components/bottom-sheet/betplace.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {APIService, SharedataService, AlertService} from '../../core/services';

declare let $;

@Component({
  selector: 'app-election',
  templateUrl: './election.component.html',
  styleUrls: ['./election.component.css']
})
export class ElectionComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('pRef', {static: false}) pRef: ElementRef;
  @ViewChild('bookmakerRef', {static: false}) bookmakerRef: ElementRef;
  @ViewChild('fancyRef', {static: false}) fancyRef: ElementRef;
  @ViewChild('fancy2Ref', {static: false}) fancy2Ref: ElementRef;
  @ViewChild('fancy3Ref', {static: false}) fancy3Ref: ElementRef;
  @ViewChild('khadoRef', {static: false}) khadoRef: ElementRef;
  @ViewChild('ballbyballRef', {static: false}) ballbyballRef: ElementRef;
  @ViewChild('profitLossModal', {static: false}) profitLossModal: ElementRef;


  public oldGdata: any = {};
  public oldGdataCnt: number = 0;
  public mHtml: string;

  // **VARIABLE-DECLARATION************************************

  // **DATA-STORAGE************************************
  public matchData: any;
  public oddsData: any;
  public marketArr: any;
  public currantBets: any;
  public fancy2PL: any = [];
  public fancyBets: any = [];
  public primaryColor: string;

  // **PROFIT-LOSS BOOK-GLOBLE************************************
  public GAME_BOOKDATA: any = [];

  // **SINGLE-LINE-DATA************************************
  public eventId: string;
  public eventTitle: string;
  public marketId: string;
  public localComentory: string;
  public eventMsg: string;
  public updatedTime: number;
  public annimationClass: string;

  // **FLAGE************************************
  public isScoreBoard: boolean;
  public isTv: boolean;
  public isTabs: string;
  public isFirstLoad: boolean;
  public isDestroy: boolean;
  public isLoadFirst: boolean;
  public isLoadFancyPl: boolean;
  public isFancy3: boolean;
  public loaderShow: boolean;
  public isActivePlaceBet: boolean;
  public isloadBetList: boolean;

  // **INTERVAL******************************
  public interval1000: any;
  public interval300: any;
  public isActivePlaceBetIndex: string;
  public isActivePlaceBetSType: string;
  private onUpdate: number;
  private navigationSubscription;

  constructor(inj: Injector, private route: ActivatedRoute,
              private alert: AlertService,
              private share: SharedataService,
              private endPoint: APIService,
              private elRef: ElementRef,
              private renderer: Renderer2,
              private placeBetSheet: MatBottomSheet) {
    super(inj);

    this.eventId = this.route.snapshot.params.id;
    this.share.eventId.emit(this.route.snapshot.params.id);
    this.marketArr = [];
    this.valInit();
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        console.log(this.activatedRoute.snapshot.params.id);
        if (this.eventId !== undefined && this.eventId !== this.activatedRoute.snapshot.params.id) {
          this.reloadCurrentRoute();
        }
      }
    });
  }

  ngOnInit() {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.spinner.show();
    this.getEventDetail();
    this.getProfitLoss();
    // this.oddsChange();

    this.share.betStatus.subscribe((res) => { // Bet Flags Manage
      console.log('Test', res);
      if (res === 1) {
        this.isActivePlaceBet = true;
      } else if (res === 2) {
        this.isActivePlaceBet = false;
        this.loaderShow = true;
        this.spinner.show();
      } else if (res === 3) {
        this.loaderShow = false;
        this.spinner.hide();
        this.getProfitLoss();
        // this.getBetList();
      }
    });

  }

  ngAfterViewInit(): void {
    // this.getBetList();
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
    this.matchData = [];
    this.marketArr = [];
    this.oddsData = [];
    clearTimeout(this.interval1000);
    clearTimeout(this.interval300);
    this.share.eventId.emit('');
    this.share.betslist.emit('');
    this.share.localCommantry.emit('');
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  valInit() {
    this.isScoreBoard = false;
    this.isTv = false;
    this.isTabs = 'live-market';
    this.isFirstLoad = false;
    this.isDestroy = false;
    this.isLoadFirst = true;
    this.annimationClass = 'fadeInUp';
    this.isActivePlaceBet = false;
    this.isloadBetList = true;
    this.loaderShow = false;
  }

  reloadCurrentRoute() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  changeTabs(tab: string) {
    this.isTabs = tab;
    this.share.setRouterChange('1'); // CALL TO HEADER API
    this.getBetList();
  }

  getEventDetail(): void {
    this.endPoint.getElectionDetail(this.eventId).subscribe((res) => {
      if (res.status === 1 && res.data != null) {
        try {
          this.matchData = res.data.items;
          // this.marketArr = res.data.marketIdsArr; // SET MARKET ARRAY
          this.eventTitle = res.data.items.title; // SET MARKET ARRAY

          if (this.marketArr === null || this.marketArr.length === 0) { // INIT MARKET ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
          } else if (this.marketArr.length !== res.data.marketIdsArr.length) { // CHECK DIFF. OF CURRANT AND OLD ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
            clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          } else if (this.marketArr.length === res.data.marketIdsArr.length && res.data.items.ballbyball) {
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = true;
            // clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          }

          if (!this.isFirstLoad && this.marketArr !== null && this.marketArr.length) { // LOAD FIRST TIME ODDS FUNCTIONS
            this.isFirstLoad = true;
            this.getOdds();
          }

          if (this.onUpdate === undefined && res.updatedOn !== null) {  // set gameover time
            this.onUpdate = res.updatedOn;
          } else if (this.onUpdate !== null && this.onUpdate !== res.updatedOn) { // CHECK IF GAMEOVER TIME CHANGE OR NOT
            this.onUpdate = res.updatedOn;
            this.share.setRouterChange('3');
            this.getProfitLoss();
          }
          this.clearSesssionBind();
          // this.spinner.hide();
          if (!this.loaderShow) {
            this.spinner.hide();
          }
        } catch (e) {
          console.log('Failed to set event data', e);
        }

      } else {  // AFTER GAME OVER  TO RECALL OPEN EVENT
        this.clearSesssionBind();
        console.error('Error-detail1', res.message);
        this.eventMsg = res.msg;
        this.eventTitle = '';
        this.matchData = [];
        this.marketArr = [];
        if (!this.isDestroy && this.eventMsg !== '') {
          this.interval1000 = setTimeout(() => {
            this.getEventDetail();
          }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
        }
      }
      if (!this.isDestroy) { // API CALL TO GET REAL TIME UPDATES
        this.interval1000 = setTimeout(() => {
          this.getEventDetail();
        }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
      }
    }, (error) => {
      if (!this.loaderShow) {
        this.loaderShow = true;
        this.spinner.hide();
      }
      this.clearSesssionBind();
      console.log('Error-detail', error);
    });
  }

  getOdds() {
    const demo = [{market_id: '1.167211709'}];
    this.endPoint.getOddsData(this.marketArr).subscribe((resData) => {
        try {
          this.oddsData = resData.data.items;
          // this.matchData.match_odd.marketId

          if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
            this.share.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
          }

          if (resData.data.items.bookmaker && resData.data.items.bookmaker.length && this.matchData.bookmaker) {
            this.bindBookmakerData(resData.data.items.bookmaker, this.matchData.bookmaker);
          }

          if (resData.data.items.fancy2 && this.matchData.fancy2) {
            this.bindFancy2Data(resData.data.items.fancy2, this.matchData.fancy2);
          }

          if (resData.data.items.fancy3 && this.matchData.fancy3) {
            this.bindFancy3Data(resData.data.items.fancy3, this.matchData.fancy3);
          }

          const tThis = this;
          const clickButtons = this.elRef.nativeElement.querySelectorAll('.bet-sec');
          const clickButtonsModelPl = this.elRef.nativeElement.querySelectorAll('.bookModalDetail');
          for (let i = 0; i < clickButtons.length; i++) {
            this.renderer.listen(clickButtons[i], 'click', ($event) => {
              tThis._placebet(clickButtons[i].dataset);
            });
          }

          for (let i = 0; i < clickButtonsModelPl.length; i++) {
            this.renderer.listen(clickButtonsModelPl[i], 'click', ($event) => {
              tThis.getFancyTwoPL(clickButtonsModelPl[i].dataset);
            });
          }

          this.annimationClass = '';
          if (!this.isDestroy) { // && this.isScreenActive
            this.interval300 = setTimeout(() => {
              clearTimeout(this.interval300);
              this.getOdds();
            }, this.getToken('oddsTiming') !== null ? parseInt(this.getToken('oddsTiming')) : 400); //
          }
          if (!this.loaderShow) {
            this.loaderShow = true;
            this.spinner.hide();
          }
        } catch (e) {
          console.log('Failed to set odds data', e);
        }
      },
      (error) => {
        if (!this.loaderShow) {
          this.loaderShow = true;
          this.spinner.hide();
        }
        console.log('error-Odds', error);
      });
  }

  clearSesssionBind() {
    if (this.matchData.bookmaker === null) {
      this.bookmakerRef.nativeElement.innerHTML = '';
    }
    if (this.matchData.fancy === null) {
      this.fancyRef.nativeElement.innerHTML = '';
    }
    if (this.matchData.fancy2 === null) {
      this.fancy2Ref.nativeElement.innerHTML = '';
    }
    if (this.matchData.fancy3 === null) {
      this.fancy3Ref.nativeElement.innerHTML = '';
    }
  }

  bindBookmakerData(dataOdd, bookmakerData) {
    try {
      // fadeInUp
      let bookmakerHtmlData = '';
      for (let i = 0; i < bookmakerData.length; i++) {

        bookmakerHtmlData += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
          '<div class="col-md-4 col-7"><p class="market-title">' + bookmakerData[i].marketName + '</p></div>' +
          '<div class="col-md-6 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

        for (let j = 0; j < bookmakerData[i].runners.length; j++) {
          let data: any = {};
          if (dataOdd[i].market_id === bookmakerData[i].market_id) { // CHECK MARKET ID
            data = dataOdd[i];
          } else {
            for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
              if (dataOdd[ii].market_id === bookmakerData[i].market_id) {
                data = dataOdd[ii];
                break; // <=== breaks out of the loop early
              }
            }
          }
          let suspended = '';
          if (data && data.runners[j] && (data.runners[j].suspended !== 0 || data.runners[j].ballrunning !== 0)) {
            suspended = data.runners[j].suspended === 1 ? 'Suspended' : 'Ball Running';
          }
          let bdata = 0;
          let bcolor = '';
          if (this.GAME_BOOKDATA[bookmakerData[i].runners[j].mType + '_' + bookmakerData[i].runners[j].sec_id] !== undefined) {
            bdata = this.GAME_BOOKDATA[bookmakerData[i].runners[j].mType + '_' + bookmakerData[i].runners[j].sec_id];
          }

          if (bdata <= -1) {
            bcolor = 'red';
          } else if (bdata >= 1) {
            bcolor = 'green';
          } else {
            bcolor = 'black';
          }

          let borderClass = '';
          if (i <= 1) {
            borderClass = 'border-bottom';
          }

          bookmakerHtmlData += '<div data-title="' + suspended + '"  id="' + bookmakerData[i].runners[j].sec_id + '" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
          bookmakerHtmlData += '<div class="col-md-4 col-7"><p class="market-name">' + bookmakerData[i].runners[j].runner + '<span class="odd-number ' + bcolor + '">' + bdata + '</span></p></div>';
          bookmakerHtmlData += '<div class="col-md-6 text-center col-5"> <div class="bl-buttons">';
          bookmakerHtmlData += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + bookmakerData[i].marketName + '" data-sec="' + bookmakerData[i].runners[j].sec_id + '" data-btype="back" data-rate="' + data.runners[j].back + '" data-runnerName="' + bookmakerData[i].runners[j].runner + '" data-mtype="' + bookmakerData[i].runners[j].mType + '"  data-marketId="' + bookmakerData[i].market_id + '" data-slug="' + bookmakerData[i].slug + '" data-sportId="' + bookmakerData[i].sportId + '" data-minstack="' + bookmakerData[i].runners[j].minStack + '" data-maxstack="' + bookmakerData[i].runners[j].maxStack + '" data-maxprofitlimit="' + bookmakerData[i].runners[j].maxProfitLimit + '">' + (data.runners[j].back === undefined ? '0' : data.runners[j].back) + ' <span>00</span></button>';
          bookmakerHtmlData += '<button class="bl-btn lay waves-effect waves-light bet-sec"  data-index="' + i + '" data-marketname="' + bookmakerData[i].marketName + '" data-sec="' + bookmakerData[i].runners[j].sec_id + '" data-btype="lay" data-rate="' + data.runners[j].lay + '" data-runnerName="' + bookmakerData[i].runners[j].runner + '" data-mtype="' + bookmakerData[i].runners[j].mType + '"  data-marketId="' + bookmakerData[i].market_id + '" data-slug="' + bookmakerData[i].slug + '" data-sportId="' + bookmakerData[i].sportId + '" data-minstack="' + bookmakerData[i].runners[j].minStack + '" data-maxstack="' + bookmakerData[i].runners[j].maxStack + '" data-maxprofitlimit="' + bookmakerData[i].runners[j].maxProfitLimit + '">' + (data.runners[j].lay === undefined ? '0' : data.runners[j].lay) + ' <span>00</span></button>';
          if (suspended !== '') {
            bookmakerHtmlData += '<div class="suspended"> <span>' + suspended + '</span></div> \n';
          }
          bookmakerHtmlData += '</div></div>' +
            '<div class="col-md-2 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b> ' + bookmakerData[i].runners[j].minStack + ' </b></span><span>max stakes: <b> ' + bookmakerData[i].runners[j].maxStack + ' </b></span></p></div></div>';
        }
        if (bookmakerData[i].info !== null && bookmakerData[i].info !== 'null' && bookmakerData[i].info !== '') {
          bookmakerHtmlData += '<p class="alert-text">' + bookmakerData[i].info + '</p>';
        }
        bookmakerHtmlData += '</div>';
      }
      this.bookmakerRef.nativeElement.innerHTML = bookmakerHtmlData;
    } catch (e) {
      console.log('Failed to bind bookmaker', e);
    }
  }

  bindFancy2Data(dataOdd, fancy2) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let fancy2DataHtml = '';
      fancy2DataHtml = '<div class="market-section">\n' +
        '<div class="row align-items-center text-uppercase text-dark market-title-row">\n' +
        '<div class="col-md-4 col-7"><p class="market-title">Session</p></div>\n' +
        '<div class="col-md-6 text-center col-5">\n' +
        '<p class="market-back"><span>no</span> <span>yes</span></p>\n' +
        '</div></div>';

      for (let i = 0; i < fancy2.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy2[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy2[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy2[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy2DataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">\n' +
          '<div class="col-md-4 col-5"><p class="market-name">' + fancy2[i].title + '</p></div>';
        fancy2DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail"  ' + bookActiveStatus + ' data-title="' + fancy2[i].title + '"  data-marketid="' + data.market_id + '" data-mtype="' + fancy2[i].mType + '">book</button></div>';
        fancy2DataHtml += '<div class="col-md-4 text-center col-5"><div class="bl-buttons">';
        fancy2DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec"  data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.no + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.no + ' <span>' + data.no_rate + '</span></button>';
        fancy2DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.yes + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy2DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</span></div>';
        }
        fancy2DataHtml += '</div></div>' +
          '<div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b> ' + fancy2.minStack + ' </b></span><span>max stakes: <b> ' + fancy2.maxStack + ' </b></span></p></div></div>';
        if (fancy2[i].info) {
          fancy2DataHtml += '<p class="alert-text">' + fancy2[i].info + '</p>';
        }
      }
      fancy2DataHtml += '</div>';
      this.fancy2Ref.nativeElement.innerHTML = fancy2DataHtml;
    } catch (e) {
      console.log('Failed to Bind Fancy-2 ELE', e);
    }
  }

  bindFancy3Data(dataOdd, fancy3) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let fancy3DataHtml = '';
      fancy3DataHtml += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">';
      fancy3DataHtml += '<div class="col-md-4 col-7"><p class="market-title">other market</p></div>';
      fancy3DataHtml += '<div class="col-md-6 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < fancy3.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy3[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy3[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy3[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }
        fancy3DataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        fancy3DataHtml += '<p class="market-name">' + fancy3[i].title + '</p></div>';
        fancy3DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-title="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-mtype="' + fancy3[i].mType + '">book</button></div>';
        fancy3DataHtml += '<div class="col-md-4 col-5"><div class="bl-buttons">';
        fancy3DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="back" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.back + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.back + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.back + '<span> 00</span></button>';
        fancy3DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="lay" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.lay + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.lay + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.lay + ' <span>00</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy3DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        fancy3DataHtml += '</div>' +
          '</div><div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b> ' + fancy3.minStack + ' </b></span><span>max stakes: <b> ' + fancy3.maxStack + ' </b></span></p></div>';
        if (fancy3[i].info) {
          fancy3DataHtml += '<div class="col-12"><p class="alert-text">' + fancy3[i].info + '</p></div>';
        }
        fancy3DataHtml += '</div>';
      }
      fancy3DataHtml += '</div>';
      this.fancy3Ref.nativeElement.innerHTML = fancy3DataHtml;
    } catch (e) {
      console.log('Failed to Bind Fancy-3 ELE', e);
    }
  }

  _placebet(e) {
    this.isActivePlaceBet = true;
    this.isActivePlaceBetIndex = e.index;
    this.isActivePlaceBetSType = e.mtype;

    if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
      this.share.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
    }

    let secId = e.sec;
    if (e.mtype === 'fancy' || e.mtype === 'fancy2' || e.mtype === 'fancy3') {
      secId = e.marketid;
    }

    const initBet = {
      price: e.rate,
      size: '',
      runner: e.runnername,
      bet_type: e.btype,
      sec_id: secId,
      market_id: e.marketid,
      event_id: this.eventId,
      m_type: e.mtype,
      eventname: this.eventTitle,
      market_name: e.marketname,
      slug: e.slug,
      rate: e.rrate,
      sport_id: e.sportid
    };

    const plArr: any = [];
    if (e.mtype === 'bookmaker') {
      for (let i = 0; i < this.matchData[e.mtype].length; i++) {
        for (let ii = 0; ii < this.matchData[e.mtype][i].runners.length; ii++) {
          const rData = {
            runner: this.matchData[e.mtype][i].runners[ii].runner,
            secId: this.matchData[e.mtype][i].runners[ii].sec_id,
            profit_loss: this.GAME_BOOKDATA[e.mtype + '_' + this.matchData[e.mtype][i].runners[ii].sec_id] ? this.GAME_BOOKDATA[e.mtype + '_' + this.matchData[e.mtype][i].runners[ii].sec_id] : 0,
          };
          plArr.push(rData);
        }
      }
    }
    // console.log(this.GAME_BOOKDATA);
    const plLimit = {minStack: e.minstack, maxStack: e.maxstack, maxProfitLimit: e.maxprofitlimit};
    this.placeBetSheet.open(BetplaceComponent, {
      // panelClass: 'LayBetting',
      disableClose: true,
      data: {int: initBet, market: plArr, limit: plLimit, index: e.index}
    });
  }

  getProfitLoss() {
    const data = {
      event_id: this.eventId
    };
    this.endPoint.getPLMatchoddANDBookmaker(data).subscribe((res) => {
      if (res.status === 1 && res.data !== null) {
        try {
          res.data.match_odd.forEach((item) => {
            if (this.GAME_BOOKDATA['match_odd_' + item.secId] !== undefined) {
              this.GAME_BOOKDATA['match_odd_' + item.secId] = parseInt(item.profit_loss);
            } else {
              this.GAME_BOOKDATA['match_odd_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
            }
          });
          res.data.bookmaker.forEach((item) => {
            if (this.GAME_BOOKDATA['bookmaker_' + item.secId] !== undefined) {
              this.GAME_BOOKDATA['bookmaker_' + item.secId] = parseInt(item.profit_loss);
            } else {
              this.GAME_BOOKDATA['bookmaker_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
            }
          });
        } catch (e) {
          console.log('Failed to set profitloss ELE', e);
        }
      }
    });
  }

  // **Fancy two profit loss******************************/
  getFancyTwoPL(e) {
    this.isLoadFancyPl = false;
    $('#Book-Unbook').modal('show');
    // if (e.is_book.toLocaleString() === '1') {
    $('#session-title').html('Book <i class="mdi mdi-arrow-right"></i>' + e.title);
    this.isFancy3 = e.mtype === 'fancy3' ? true : false;
    this.fancy2PL = [];
    this.fancyBets = [];
    if (e.marketid) {
      const data = {event_id: this.eventId, market_id: e.marketid, session_type: e.mtype};
      this.endPoint.getBetList(data).subscribe((response: any) => {
        if (response.status === 1) {
          if (!this.isFancy3 && data.session_type !== 'khado' && data.session_type !== 'fancy3') {
            const plData = this.fancyBookCal(response.data.betList);
            plData.forEach(item => {
              this.fancy2PL.push(item);
            });
          } else if (!this.isFancy3 && data.session_type === 'khado') {
            const plData = this.getProfitLossKhado(response.data.betList);
            plData.forEach(item => {
              this.fancy2PL.push(item);
            });
          } else {
            const plData = this.getProfitLossOtherMarket(response.data.betList);
            this.fancy2PL = plData;
          }
          // if (!this.isFancy3) {
          //   response.data.profit_loss.forEach(item => {
          //     this.fancy2PL.push(item);
          //   });
          // } else {
          //   this.fancy2PL = response.data.profit_loss;
          // }
          this.fancyBets = response.data.betList;
        }
        this.isLoadFancyPl = true;
      });
      // }
    }
  }

  // **BETLIST**************************/
  getBetList() {
    this.isloadBetList = true;
    $('#Matched-Unmatched').modal('show');
    this.endPoint.getMatchedUnmatchedBet(this.eventId).subscribe((res) => {
      if (res.status === 1 && res.data !== null) {
        this.currantBets = res.data;
      }
      this.isloadBetList = false;
      this.changedRef.detectChanges();
    });
  }
}
