import {Component, OnInit, Injector, ElementRef, TemplateRef, ViewChild, AfterViewInit, OnDestroy, Renderer2} from '@angular/core';
import {BaseComponent} from '../../share/components/common.component';
import {BetplaceComponent} from '../../share/components/bottom-sheet/betplace.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {AlertService, APIService, SharedataService} from '../../core/services';
import {NavigationEnd} from '@angular/router';

declare let $;

@Component({
  selector: 'app-jackpot',
  templateUrl: './grid-list.component.html',
  styleUrls: ['./jackpot.component.css']
})
export class JackpotComponent extends BaseComponent implements OnInit, OnDestroy {

  public gList: any = [];
  private jId: any;
  public title: string;
  public isFancy: number;
  public isKhado: number;
  public primaryColor: string;
  private navigationSubscription;

  constructor(inj: Injector, private commonService: SharedataService, private api: APIService) {
    super(inj);
    this.jId = this.activatedRoute.snapshot.params.eid;
    this.commonService.setRouterChange('1'); // CALL TO HEADER API
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.isFancy = 0;
    this.isKhado = 0;

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        console.log(this.activatedRoute.snapshot.params.eid);
        if (this.jId !== undefined && this.jId !== this.activatedRoute.snapshot.params.eid) {
          this.reloadCurrentRoute();
        }
      }
    });
  }

  ngOnInit() {
    this.spinner.show();
    this.getJackpotGridList();
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  reloadCurrentRoute() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  getJackpotGridList() {
    this.api.getJackportGrid(this.jId).subscribe((res) => {
      if (res.status === 1 && res.data) {
        this.title = res.data.title;
        this.isFancy = res.data.fancycount;
        this.isKhado = res.data.khadocount;
        this.gList = res.data.items;
        this.spinner.hide();
        this.changedRef.detectChanges();
      } else {
        this.spinner.hide();
        this.changedRef.detectChanges();
      }
    }, (error) => {
      this.spinner.hide();
    });
  }

}


@Component({
  selector: 'app-jackpot-list',
  templateUrl: './list.component.html',
  styleUrls: ['./jackpot.component.css']
})
export class JackpotListComponent extends BaseComponent implements OnInit {
  public jList: any = [];
  public jId: any;
  public slug: string;

  constructor(inj: Injector, private commonService: SharedataService, private api: APIService) {
    super(inj);
    this.jId = this.activatedRoute.snapshot.params.eid;
    this.slug = this.activatedRoute.snapshot.params.slug;
    this.commonService.setRouterChange('1'); // CALL TO HEADER API
  }

  ngOnInit() {
    this.getJackpotList();
  }

  getJackpotList() {
    const data = {event_id: this.jId, slug: this.slug};
    this.api.getJackportList(data).subscribe((res) => {
      if (res.status === 1 && res.data) {
        this.jList = res.data;
        this.changedRef.detectChanges();
      }
    });
  }
}

@Component({
  selector: 'app-jackpot-fancy',
  templateUrl: './fancy.component.html',
  styleUrls: ['./jackpot.component.css']
})
export class JackpotFancyComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('fancy2Ref', {static: false}) fancy2Ref: ElementRef;
  @ViewChild('fancy3Ref', {static: false}) fancy3Ref: ElementRef;
  @ViewChild('khadoRef', {static: false}) khadoRef: ElementRef;

  // **DATA-STORAGE************************************
  public matchData: any;
  public oddsData: any;
  public marketArr: any;
  public currantBets: any;
  public fancy2PL: any = [];
  public fancyBets: any = [];
  public primaryColor: string;

  // **PROFIT-LOSS BOOK-GLOBLE************************************
  public GAME_BOOKDATA: any = [];

  // **SINGLE-LINE-DATA************************************
  public eventId: string;
  public eventTitle: string;
  public marketId: string;
  public localComentory: string;
  public eventMsg: string;
  public updatedTime: number;
  public annimationClass: string;
  // **FLAGE************************************
  public isScoreBoard: boolean;
  public isTv: boolean;
  public isTabs: string;
  public isFirstLoad: boolean;
  public isDestroy: boolean;
  public isLoadFirst: boolean;
  public isLoadFancyPl: boolean;
  public isFancy3: boolean;
  public loaderShow: boolean;
  public isActivePlaceBet: boolean;
  public betlistLoading: boolean;

  // **INTERVAL******************************
  public interval1000: any;
  public interval300: any;
  public isActivePlaceBetIndex: string;
  public isActivePlaceBetSType: string;
  public slug: string;
  private onUpdate: number;

  constructor(inj: Injector,
              private alert: AlertService,
              private commonService: SharedataService,
              private api: APIService,
              private elRef: ElementRef,
              private renderer: Renderer2,
              private placeBetSheet: MatBottomSheet) {
    super(inj);
    this.slug = this.activatedRoute.snapshot.params.slug;
    this.eventId = this.activatedRoute.snapshot.params.eid;
    this.commonService.eventId.emit(this.activatedRoute.snapshot.params.eid);
    this.marketArr = [];
    this.valInit();
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
  }

  ngOnInit() {
    this.spinner.show();
    this.getEventDetail();

    this.commonService.betStatus.subscribe((res) => { // Bet Flags Manage
      console.log(res);
      if (res === 1) {
        this.isActivePlaceBet = true;
      } else if (res === 2) {
        this.loaderShow = true;
        this.isActivePlaceBet = false;
        this.spinner.show();
      } else if (res === 3) {
        this.loaderShow = false;
        this.spinner.hide();
        // this.getBetList();
      }
    });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
    this.matchData = [];
    this.marketArr = [];
    this.oddsData = [];
    clearTimeout(this.interval1000);
    clearTimeout(this.interval300);
    this.commonService.eventId.emit('');
    this.commonService.betslist.emit('');
  }

  valInit() {
    this.isTabs = 'live-market';
    this.isFirstLoad = false;
    this.isDestroy = false;
    this.isLoadFirst = true;
    this.annimationClass = 'fadeInUp';
    this.isActivePlaceBet = false;
    this.loaderShow = false;
    this.betlistLoading = false;
  }

  changeTab(tab: string) {
    this.isTabs = tab;
    this.commonService.setRouterChange('1'); // CALL TO HEADER API
    this.getBetList();
  }

  getEventDetail(): void {
    this.api.getJackpotFanyKhado(this.eventId, this.slug).subscribe((res) => {
      if (res.status === 1 && res.data != null) {
        try {
          this.matchData = res.data.items;
          this.eventTitle = res.data.title; // SET MARKET ARRAY

          if (this.marketArr.length === 0) { // INIT MARKET ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
          } else if (this.marketArr.length !== res.data.marketIdsArr.length) { // CHECK DIFF. OF CURRANT AND OLD ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
            clearTimeout(this.interval300); // API CALLBACK INTERVAL CLEAR
          }

          if (!this.isFirstLoad && this.marketArr.length) { // LOAD FIRST TIME ODDS FUNCTIONS
            this.isFirstLoad = true;
            this.getOdds();
          } else {
            if (!this.loaderShow) {
              this.loaderShow = true;
              this.spinner.hide();
            }
          }

          if (this.onUpdate === undefined && res.updatedOn !== null) {  // set gameover time
            this.onUpdate = res.updatedOn;
          } else if (this.onUpdate !== null && this.onUpdate !== res.updatedOn) { // CHECK IF GAMEOVER TIME CHANGE OR NOT
            this.onUpdate = res.updatedOn;
            this.commonService.setRouterChange('3');
          }
          this.clearSesssionBind();
        } catch (e) {
          if (!this.loaderShow) {
            this.loaderShow = true;
            this.spinner.hide();
          }
          console.log('Failed to set Event-Fancy Api-Jackpot', e);
        }
      } else {  // AFTER GAME OVER  TO RECALL OPEN EVENT
        this.spinner.hide();
        this.clearSesssionBind();
        // console.error('Error-detail1', res.message);
        this.eventMsg = res.msg;
        this.eventTitle = '';
        this.matchData = [];
        this.marketArr = [];
        if (!this.isDestroy && this.eventMsg !== '') {
          this.interval1000 = setTimeout(() => {
            this.getEventDetail();
            // tslint:disable-next-line:radix
          }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
        }
      }
      if (!this.isDestroy && (this.eventMsg === undefined || this.eventMsg === '')) { // API CALL TO GET REAL TIME UPDATES
        this.interval1000 = setTimeout(() => {
          this.getEventDetail();
          // tslint:disable-next-line:radix
        }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
      }
    }, (error) => {
      if (!this.loaderShow) {
        this.loaderShow = true;
        this.spinner.hide();
      }
      this.clearSesssionBind();
      // console.log('Error-detail', error);
    });
  }

  getOdds() {
    this.api.getOddsData(this.marketArr).subscribe((resData) => {
        try {
          this.oddsData = resData.data.items;

          if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
            this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
          }

          if (resData.data.items.fancy2 && resData.data.items.fancy2 && this.slug === 'fancy') {
            this.bindFancy2Data(resData.data.items.fancy2, this.matchData.fancy2);
          }

          if (resData.data.items.fancy3 && resData.data.items.fancy3 && this.slug === 'fancy') {
            this.bindFancy3Data(resData.data.items.fancy3, this.matchData.fancy3);
          }

          if (resData.data.items.khado && resData.data.items.khado && this.slug === 'khado') {
            this.bindKhadoData(resData.data.items.khado, this.matchData.khado);
          }

          const tThis = this;
          const clickButtons = this.elRef.nativeElement.querySelectorAll('.bet-sec');
          const clickButtonsModelPl = this.elRef.nativeElement.querySelectorAll('.bookModalDetail');
          let i = 0;
          let ii = 0;
          for (const clickbt of clickButtons) {
            this.renderer.listen(clickbt, 'click', ($event) => {
              tThis._placebet(clickbt.dataset);
            });
            i++;
          }
          // for (let i = 0; i < clickButtons.length; i++) {
          //   this.renderer.listen(clickButtons[i], 'click', ($event) => {
          //     tThis._placebet(clickButtons[i].dataset);
          //   });
          // }

          for (const clickbt of clickButtonsModelPl) {
            this.renderer.listen(clickbt, 'click', ($event) => {
              tThis.getFancyTwoPL(clickbt.dataset);
            });
            ii++;
          }
          // for (let i = 0; i < clickButtonsModelPl.length; i++) {
          //   this.renderer.listen(clickButtonsModelPl[i], 'click', ($event) => {
          //     tThis.getFancyTwoPL(clickButtonsModelPl[i].dataset);
          //   });
          // }

          this.annimationClass = '';
          if (!this.isDestroy) { // && this.isScreenActive
            this.interval300 = setTimeout(() => {
              clearTimeout(this.interval300);
              this.getOdds();
              // tslint:disable-next-line:radix
            }, this.getToken('oddsTiming') !== null ? parseInt(this.getToken('oddsTiming')) : 400); //
          }
          if (!this.loaderShow) {
            this.loaderShow = true;
            this.spinner.hide();
          }
        } catch (e) {
          if (!this.loaderShow) {
            this.loaderShow = true;
            this.spinner.hide();
          }
          console.log('Failed to Odds Api-Jackpot', e);
        }
      },
      (error) => {
        if (!this.loaderShow) {
          this.loaderShow = true;
          this.spinner.hide();
        }
        console.log('error-Odds', error);
      });
  }

  clearSesssionBind() {
    if (this.slug === 'fancy') {
      if (this.matchData && this.matchData.fancy2 && !this.matchData.fancy2.length) {
        this.fancy2Ref.nativeElement.innerHTML = '';
      }
      if (this.matchData && this.matchData.fancy3 && !this.matchData.fancy3.length) {
        this.fancy3Ref.nativeElement.innerHTML = '';
      }
    } else {
      if (this.matchData && this.matchData.khado && !this.matchData.khado.length) {
        this.khadoRef.nativeElement.innerHTML = '';
      }
    }
  }

  bindFancy2Data(dataOdd, fancy2) {
    try {
      if (fancy2 === undefined || fancy2 === null) {
        return false;
      }
      if (dataOdd === undefined || dataOdd.length === 0 || fancy2 === null) {
        return false;
      }
      let fancy2DataHtml = '';
      fancy2DataHtml = '<div class="market-section">\n' +
        '<div class="row align-items-center text-uppercase text-dark market-title-row">\n' +
        '<div class="col-md-7 col-7"><p class="market-title">session</p></div>\n' +
        '<div class="col-md-5 text-center col-5">\n' +
        '<p class="market-back"><span>no</span> <span>yes</span></p>\n' +
        '</div></div>';

      for (let i = 0; i < fancy2.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy2[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy2[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }

        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy2[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy2DataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">\n' +
          '<div class="col-md-6 col-5"><p class="market-name">' + fancy2[i].title + '</p></div>';
        fancy2DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail"  ' + bookActiveStatus + ' data-title="' + fancy2[i].title + '"  data-marketid="' + data.market_id + '" data-mtype="' + fancy2[i].mType + '">book</button></div>';
        fancy2DataHtml += '<div class="col-md-5 text-center col-5"><div class="bl-buttons">';
        fancy2DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec"  data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.no + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.no + ' <span>' + data.no_rate + '</span></button>';
        fancy2DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.yes + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy2DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</span></div>';
        }
        fancy2DataHtml += '</div></div></div>';
        if (fancy2[i].info) {
          fancy2DataHtml += '<p class="alert-text">' + fancy2[i].info + '</p>';
        }
      }
      fancy2DataHtml += '</div>';
      this.fancy2Ref.nativeElement.innerHTML = fancy2DataHtml;
    } catch (e) {
      console.log('Failed to bind session', e);
    }
  }

  bindFancy3Data(dataOdd, fancy3) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let fancy3DataHtml = '';
      fancy3DataHtml += '<div class="market-section"><div class="row mx-lg-0 align-items-center text-uppercase text-dark market-title-row">';
      fancy3DataHtml += '<div class="col-md-5 col-7"><p class="market-title">other market</p></div>';
      fancy3DataHtml += '<div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < fancy3.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy3[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy3[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy3[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy3DataHtml += '<div class="row mx-lg-0 my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        fancy3DataHtml += '<p class="market-name">' + fancy3[i].title + '</p></div>';
        fancy3DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-title="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-mtype="' + fancy3[i].mType + '">book</button></div>';
        fancy3DataHtml += '<div class="col-md-7 col-5"><div class="bl-buttons">';
        fancy3DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="back" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.back + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.back + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.back + '<span> 00</span></button>';
        fancy3DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="lay" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.lay + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.lay + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.lay + ' <span>00</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy3DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        fancy3DataHtml += '</div></div>';
        if (fancy3[i].info) {
          fancy3DataHtml += '<div class="col-12"><p class="alert-text">' + fancy3[i].info + '</p></div>';
        }
        fancy3DataHtml += '</div>';
      }
      fancy3DataHtml += '</div>';
      this.fancy3Ref.nativeElement.innerHTML = fancy3DataHtml;
    } catch (e) {
      console.log('Failed to bind Fancy3', e);
    }
  }

  bindKhadoData(dataOdd, khado) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let khadoDataHtml = '';
      khadoDataHtml = '<div class="market-section"><div class="row mx-lg-0 align-items-center text-uppercase text-dark market-title-row">';
      khadoDataHtml += '<div class="col-md-5 col-7"><p class="market-title">Khado market</p></div>';
      khadoDataHtml += ' <div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < khado.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === khado[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === khado[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (khado[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        khadoDataHtml += '<div class="row mx-lg-0 my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        khadoDataHtml += '<p class="market-name">' + khado[i].title + '<span class="khado-number">' + data.difference + '</span></p></div>';
        khadoDataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + '  data-title="' + khado[i].title + '-' + data.difference + '" data-marketid="' + data.market_id + '" data-mtype="' + khado[i].mType + '">book</button></div>';
        khadoDataHtml += '<div class="col-md-7 col-5"><div class="bl-buttons">';
        khadoDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + khado[i].title + '-' + data.difference + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + khado[i].title + '-' + data.difference + '" data-rate="' + data.yes + '" data-mtype="' + khado[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + khado[i].slug + '"  data-sportId="' + khado[i].sportId + '" data-minstack="' + khado[i].minStack + '"   data-maxstack="' + khado[i].maxStack + '"   data-maxprofitlimit="' + khado[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        khadoDataHtml += '<button class="bl-btn lay waves-effect waves-light" data-index="' + i + '">- <span>--</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          khadoDataHtml += '<div class="suspended"><span></span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        khadoDataHtml += '</div></div>';
        if (khado[i].info) {
          khadoDataHtml += '<div class="col-12"><p class="alert-text">' + khado[i].info + '</p></div>';
        }
        khadoDataHtml += '</div>';
      }
      khadoDataHtml += '</div>';
      this.khadoRef.nativeElement.innerHTML = khadoDataHtml;
    } catch (e) {
      console.log('Failed to bind Khado session', e);
    }
  }

  _placebet(e) {
    this.isActivePlaceBet = true;
    this.isActivePlaceBetIndex = e.index;
    this.isActivePlaceBetSType = e.mtype;

    if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
      this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
    }

    const initBet = {
      price: e.rate,
      size: '',
      runner: e.runnername,
      bet_type: e.btype,
      sec_id: e.marketid,
      market_id: e.marketid,
      event_id: this.eventId,
      m_type: e.mtype,
      eventname: this.eventTitle,
      market_name: e.marketname,
      slug: e.slug,
      rate: e.rrate,
      sport_id: e.sportid
    };

    console.log(initBet);

    const plLimit = {minStack: e.minstack, maxStack: e.maxstack, maxProfitLimit: e.maxprofitlimit};
    this.placeBetSheet.open(BetplaceComponent, {
      // panelClass: 'LayBetting',
      disableClose: true,
      data: {int: initBet, market: [], limit: plLimit, index: e.index}
    });
  }

  // **Fancy two profit loss******************************/
  getFancyTwoPL(e) {
    $('#Book-Unbook').modal('show');
    // if (e.is_book.toLocaleString() === '1') {
    $('#session-title').html('Book <i class="mdi mdi-arrow-right"></i>' + e.title);
    this.isFancy3 = e.mtype === 'fancy3' ? true : false;
    this.fancy2PL = [];
    this.fancyBets = [];
    if (e.marketid) {
      const data = {event_id: this.eventId, market_id: e.marketid, session_type: e.mtype};
      this.api.getBetList(data).subscribe((response: any) => {
        this.isLoadFancyPl = true;
        if (response.status === 1) {
          if (!this.isFancy3 && data.session_type !== 'khado' && data.session_type !== 'fancy3') {
            const plData = this.fancyBookCal(response.data.betList);
            plData.forEach(item => {
              this.fancy2PL.push(item);
            });
          } else if (!this.isFancy3 && data.session_type === 'khado') {
            const plData = this.getProfitLossKhado(response.data.betList);
            plData.forEach(item => {
              this.fancy2PL.push(item);
            });
          } else {
            const plData = this.getProfitLossOtherMarket(response.data.betList);
            this.fancy2PL = plData;
          }
          this.fancyBets = response.data.betList;
        }
      });
      // }
    }
  }

  // **BETLIST**************************/
  getBetList() {
    this.betlistLoading = true;
    $('#Matched-Unmatched').modal('show');
    this.changedRef.detectChanges();
    this.api.getMatchedUnmatchedBet(this.eventId).subscribe((res) => {
      if (res.status === 1 && res.data !== null && res.data.matched) {
        this.currantBets = [];
        if (this.slug === 'fancy') {
          this.currantBets.push(res.data.matched[this.sysConfigDt.JFANCY_BET_INDEX]);
          this.currantBets.push(res.data.matched[this.sysConfigDt.JOTHERCC_BET_INDEX]);
        } else {
          this.currantBets.push(res.data.matched[this.sysConfigDt.JKHADO_BET_INDEX]);
        }
      }
      this.betlistLoading = false;
      this.changedRef.detectChanges();
    });
  }
}


@Component({
  selector: 'app-jackpot-detail',
  templateUrl: './jackpot-detail.component.html',
  styleUrls: ['./jackpot.component.css']
})
export class JackpotDetailComponent extends BaseComponent implements OnInit, OnDestroy {
  public jList: any = [];
  public bList: any = [];
  public isDestroy: boolean;
  public isEventOpen: boolean;
  public message: string;
  public onUpdate: number;
  public timeout1000: any;
  private jId: any;
  private mId: any;
  private gId: any;
  private slug: string;
  public isLoad: boolean;
  public loaderShow: boolean;
  public primaryColor: string;

  constructor(inj: Injector,
              private placeBetSheet: MatBottomSheet,
              private commonService: SharedataService,
              private api: APIService) {
    super(inj);
    this.jId = this.activatedRoute.snapshot.params.eid;
    this.slug = this.activatedRoute.snapshot.params.slug;
    this.mId = this.activatedRoute.snapshot.params.mid;
    this.gId = this.activatedRoute.snapshot.params.gid;
    this.loaderShow = false;

    this.commonService.setRouterChange('1'); // CALL TO HEADER API
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;

    this.isDestroy = false;
    this.isEventOpen = true;
    this.isLoad = false;
    this.loaderShow = false;
  }

  ngOnInit() {
    this.spinner.show();
    this.getJackpotDetail();

    this.commonService.betStatus.subscribe((res) => { // Bet Flags Manage
      if (res === 1) {
      } else if (res === 2) {
        this.spinner.show();
      } else if (res === 3) {
        this.spinner.hide();
      }
    });
  }

  ngOnDestroy() {
    this.isDestroy = true;
    clearTimeout(this.timeout1000);
  }

  getJackpotDetail() {
    const data = {id: this.jId, market_id: this.mId, gameid: this.gId};
    this.api.getJackpotDetail(data).subscribe((res) => {
      try {
        this.isLoad = true;
        if (res.status === 1 && res.data !== null) {

          this.isEventOpen = true;
          this.message = '';

          if (this.onUpdate === undefined && res.updatedOn !== null) {  // set gameover time
            this.onUpdate = res.updatedOn;
          } else if (this.onUpdate !== null && this.onUpdate !== res.updatedOn) { // CHECK IF GAMEOVER TIME CHANGE OR NOT
            this.onUpdate = res.updatedOn;
            this.commonService.setRouterChange('3');
          }

          this.jList = res.data.items;
          this.bList = res.data.betList;
        } else if (res.msg !== undefined) { // CHECK GAME OVER
          this.jList = [];
          this.bList = [];
          this.isEventOpen = false;
          this.message = res.msg;
          if (this.onUpdate !== null && this.onUpdate !== res.updatedOn) { // CHECK IF GAMEOVER TIME CHANGE OR NOT
            this.onUpdate = res.updatedOn;
            this.commonService.setRouterChange('3');
          }
        }
        this.changedRef.detectChanges();

        if (!this.isDestroy) {
          this.timeout1000 = setTimeout(() => {
            this.getJackpotDetail();
            // tslint:disable-next-line:radix
          }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 2000);
        }
        if (!this.loaderShow) {
          this.loaderShow = true;
          this.spinner.hide();
        }
      } catch (e) {


        if (!this.loaderShow) {
          this.loaderShow = true;
          this.spinner.hide();
        }
        if (!this.isDestroy) {
          this.timeout1000 = setTimeout(() => {
            this.getJackpotDetail();
            // tslint:disable-next-line:radix
          }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 2000);
        }
        console.log('Failed to load Jackpot-detail api', e);
      }
    }, (error: any) => {
      if (!this.loaderShow) {
        this.loaderShow = true;
        this.spinner.hide();
      }
      if (!this.isDestroy) {
        this.timeout1000 = setTimeout(() => {
          this.getJackpotDetail();
          // tslint:disable-next-line:radix
        }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 2000);
      }
      console.log(error);
    });
  }

  openBetSlip(index, data, title) {
    // let runner = 'Jackpot - ' + this.jList.sub_title + ' (';
    let runners = this.jList.sub_title + ' (';
    runners += data.team_a + '-' + data.team_a_player + ' / ' + data.team_b + '-' + data.team_b_player + ')';
    const initBet = {
      price: Number(data.rate),
      size: 0,
      runner: runners,
      bet_type: 'back',
      sec_id: data.sec_id,
      market_id: data.market_id,
      event_id: data.event_id,
      m_type: data.slug ? data.slug : 'jackpot',
      market_name: this.slug,
      eventname: title,
      slug: data.slug,
      rate: Number(data.rate),
      sport_id: data.sportId,
      profit: '',
      gameId: this.gId
    };
    // console.log(JSON.stringify(this.placeBet));

    const plLimit = {
      minStack: String(this.jList.setting.min_stack),
      maxStack: String(this.jList.setting.max_stack),
      maxProfitLimit: String(this.jList.setting.max_profit_limit)
    };
    // console.log(plLimit);
    this.placeBetSheet.open(BetplaceComponent, {
      // panelClass: 'LayBetting',
      disableClose: true,
      data: {int: initBet, market: [], limit: plLimit, index: 0}
    });
  }
}
