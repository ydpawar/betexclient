import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import {ModulesComponent} from './index.component';
import {CanDeactivateGuard} from '../share/guard/can-deactivate-guard';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HeaderComponent} from '../core/include/header/header.component';
import {FooterComponent} from '../core/include/footer/footer.component';
import {SidemenuComponent} from '../core/include/sidemenu/sidemenu.component';
import {EventListComponent} from '../share/inner/event-list.component';
import {InplayComponent} from './inplay/inplay.component';
import {RulesComponent} from './rules/rules.component';
import {DetailsComponent} from './details/details.component';
import {MymarketComponent} from './mymarket/mymarket.component';
import {ProfileComponent} from './account/profile/profile.component';
import {LiveBetsComponent} from './account/live-bets/live-bets.component';
import {AccountStatementComponent, AccountStatementDetailComponent} from './account/account-statement/account-statement.component';
import {ProfitLossBetComponent, ProfitLossComponent, ProfitLossMarketComponent} from './account/profit-loss/profit-loss.component';
import {BetHistoryComponent, CasinoBetHistoryComponent} from './account/bet-history/bet-history.component';
import {TeenpattiBetHistoryComponent} from './account/teenpatti-bet-history/teenpatti-bet-history.component';
import {FilterComponent} from '../share/inner/filter.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ElectionComponent} from './election/election.component';
import {CcDetailsComponent} from './cc-details/cc-details.component';
import {BinaryComponent} from './binary/binary.component';
import {CasinoComponent} from './casino/casino.component';
import {CardGamesComponent} from './card-games/card-games.component';
import {JackpotComponent, JackpotListComponent, JackpotDetailComponent, JackpotFancyComponent} from './jackpot/jackpot.component';
import {ResultComponent} from './account/result/result.component';
import {LiveTvScoreComponent} from '../share/inner/tvscore.component';

import {InfiniteScrollModule} from 'ngx-infinite-scroll';

const routes: Routes = [
  {
    path: '',
    component: ModulesComponent,
    canDeactivate: [CanDeactivateGuard],
    children: [
      {
        path: 'main',
        component: DashboardComponent
      },
      {
        path: 'main/:slug',
        component: DashboardComponent
      },
      {
        path: 'inplay',
        component: InplayComponent
      },
      {
        path: 'details/:id',
        component: DetailsComponent
      },
      {
        path: 'cc-details/:id',
        component: CcDetailsComponent
      },
      {
        path: 'binary-details/:id',
        component: BinaryComponent
      },
      {
        path: 'election-details/:id',
        component: ElectionComponent
      },
      {
        path: 'main/casino/detail/:id',
        component: CasinoComponent
      },
      {
        path: 'teenpatti',
        component: CardGamesComponent
      },
      {
        path: 'teenpatti/:id',
        component: CardGamesComponent
      },
      {
        path: 'mymarket',
        component: MymarketComponent
      },
      {
        path: 'jackpot/:eid',
        component: JackpotComponent
      },
      {
        path: 'jackpot/:eid/list/:slug',
        component: JackpotListComponent
      },
      {
        path: 'jackpot/:eid/list/:slug/:mid/:gid',
        component: JackpotDetailComponent
      },
      {
        path: 'jackpot/:eid/fdetail/:slug',
        component: JackpotFancyComponent
      },
      {
        path: 'rules',
        component: RulesComponent
      },
      {
        path: 'live-bets',
        component: LiveBetsComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'statement',
        component: AccountStatementComponent
      },
      {
        path: 'statement/detail/:id',
        component: AccountStatementDetailComponent
      },
      {
        path: 'profitloss',
        component: ProfitLossComponent
      },
      {
        path: 'profitloss/:id/:eid',
        component: ProfitLossMarketComponent
      },
      {
        path: 'profitloss/:id/:eid/:mtype/:mid',
        component: ProfitLossBetComponent
      },
      {
        path: 'profitloss/teenpatti/:mtype/:mid',
        component: ProfitLossBetComponent
      },
      {
        path: 'bet-history',
        component: BetHistoryComponent
      },
      {
        path: 'teenpatti-history',
        component: TeenpattiBetHistoryComponent
      },
      {
        path: 'casino-history',
        component: CasinoBetHistoryComponent
      },
      {
        path: 'result',
        component: ResultComponent
      },
      // {
      //   path: 'terms',
      //   component: TermsComponent
      // },
      {path: '', redirectTo: 'main', pathMatch: 'full'},
    ]
  }
];

@NgModule({
  declarations: [
    ModulesComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    SidemenuComponent,
    LiveTvScoreComponent,
    EventListComponent,
    FilterComponent,
    InplayComponent,
    RulesComponent,
    DetailsComponent,
    MymarketComponent,
    ProfileComponent,
    LiveBetsComponent,
    AccountStatementComponent,
    AccountStatementDetailComponent,
    ProfitLossComponent,
    BetHistoryComponent,
    TeenpattiBetHistoryComponent,
    CasinoBetHistoryComponent,
    ElectionComponent,
    CcDetailsComponent,
    BinaryComponent,
    CasinoComponent,
    CardGamesComponent,
    JackpotComponent,
    JackpotListComponent,
    JackpotDetailComponent,
    JackpotFancyComponent,
    ResultComponent,
    ProfitLossMarketComponent,
    ProfitLossBetComponent,
    // TermsComponent
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NgxSpinnerModule, InfiniteScrollModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ModulesModule {
}
