import {Component, OnInit, Inject, Renderer2, HostListener} from '@angular/core';
import {AuthService, EnvService, ModalService, SharedataService} from '../core/services/index';
import {NavigationEnd, Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modules',
  templateUrl: './index.component.html',
})
export class ModulesComponent implements OnInit {
  public routeUrl: any;
  isInactive: boolean;
  userActivityThrottlerTimeout: any = null;
  userActivityTimeout: any = null;
  platforms: string = 'desktop';

  constructor(private renderer: Renderer2,
              private env: EnvService,
              private sharleData: SharedataService,
              private modaService: ModalService,
              private service: AuthService,
              private router: Router) {

    const body = document.getElementsByTagName('body')[0];
    this.platforms = body.dataset.platform;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeUrl = event.urlAfterRedirects;
        const routerTmp = event.urlAfterRedirects.split('/');
        if (routerTmp.constructor === Array && routerTmp.length > 1 && routerTmp[1] !== 'home'
          && (routerTmp[1] === 'details' || routerTmp[1] === 'jackpot' || routerTmp[1] === 'election' || routerTmp[1] === 'binary' || routerTmp[1] === 'cricket_casino')) {
          this.routeUrl = routerTmp[1];
        }
        if (routerTmp.constructor === Array && routerTmp.length > 1 && routerTmp[2] === 'casino' && routerTmp[3] === 'detail') {
          this.routeUrl = routerTmp[2];
        }
      }
    });

  }

  ngOnInit() {
    this.isInactive = false;
  }

  canDeactivate() {
    console.log(this.service.isLoggedIn);
    if (!this.service.isLoggedIn) {
      location.reload();
      return false;
    }
    if (this.service.isLoggedIn) {
      Swal.fire({
        title: 'Session Close',
        text: 'Are you sure you want to leave?.',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, logout it!',
        allowOutsideClick: false,
      }).then((result) => {
        if (result.isConfirmed) {
          localStorage.clear();
          sessionStorage.clear();
          location.reload();
          return true;
        } else {
          location.reload();
        }
      });
    }
  }

  @HostListener('mousemove', ['$event']) onMousemove($event: MouseEvent) {
    this.userActivityThrottler();
  }

  @HostListener('scroll', ['$event']) onScroll($event: MouseEvent) {
    this.userActivityThrottler();
  }

  @HostListener('touchend', ['$event']) onTouchend($event: MouseEvent) {
    this.userActivityThrottler();
  }

  @HostListener('keydown', ['$event']) onKeydown($event: MouseEvent) {
    this.userActivityThrottler();
  }

  @HostListener('touchmove', ['$event']) onTouchmove($event: MouseEvent) {
    this.userActivityThrottler();
  }

  @HostListener('touchstart', ['$event']) onTouchstart($event: MouseEvent) {
    this.userActivityThrottler();
  }

  resetUserActivityTimeout() {
    const USER_TIME_THRESHOLD = sessionStorage.getItem('userstay') ? Number(sessionStorage.getItem('userstay')) : this.env.INACTIVE_USER_TIME_THRESHOLD;
    clearTimeout(this.userActivityTimeout);
    this.userActivityTimeout = setTimeout(() => {
      clearTimeout(this.userActivityTimeout);
      this.userActivityThrottler();
      this.inactiveUserAction();
    }, USER_TIME_THRESHOLD);
  }

  userActivityThrottler() {
    if (this.isInactive) {
      this.isInactive = false;
      document.getElementsByTagName('body')[0].classList.remove('is-faded');
    }

    if (!this.userActivityThrottlerTimeout) {
      this.userActivityThrottlerTimeout = setTimeout(() => {
        this.resetUserActivityTimeout();
        clearTimeout(this.userActivityThrottlerTimeout);
        this.userActivityThrottlerTimeout = null;
      }, this.env.USER_ACTIVITY_THROTTLER_TIME);
    }
  }

  inactiveUserAction() {
    if (!localStorage.getItem('currentUser')) {
      return null;
    }
    this.isInactive = true;
    // console.log('Inactive-', this.isInactive);
    // document.getElementsByTagName('body')[0].classList.add('is-faded');
    // const result = confirm('you are inactive for longtime (inactive timeout).');
    Swal.fire({
      title: 'Inactive?',
      text: 'you are inactive for longtime (inactive timeout).',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, logout it!',
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        this.logout1();
      } else {
        location.reload();
      }
    });
  }

  logout1() {
    this.service.logout().subscribe((response: any) => {
      if (response.status === 1) {
        localStorage.clear();
        sessionStorage.clear();
        document.cookie.split(';').forEach((c) => {
          document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
        });
        this.router.navigate(['/auth']);
      }
    });
  }

  logout(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    Swal.fire({
      title: 'Are you sure?',
      text: 'You Want to be Log Out!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, logout it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.logout().subscribe((res) => {
          localStorage.clear();
          sessionStorage.clear();
          this.router.navigate(['/auth']);
        });
      }
    });
  }

}
