import {Component, ElementRef, Injector, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {APIService, AuthService, config, EnvService, SharedataService} from '../../core/services';
import {BaseComponent} from '../../share/components/common.component';

@Component({
  selector: 'app-card-games',
  templateUrl: './card-games.component.html',
  styleUrls: ['./card-games.component.css']
})
export class CardGamesComponent extends BaseComponent implements OnInit, OnDestroy {

  @ViewChild('iframeLoad', {static: false}) iframeLoad: ElementRef;
  @ViewChild('iframeLoad1', {static: false}) iframeLoad1: ElementRef;
  public userToken: string;
  public iurl: string;
  public yt: any;
  public isDestroy = false;
  public teenPattiStattus: string;
  public cDetails: any;
  public operaterId = '';
  public eventId = '';
  public loaderShow: boolean;
  public interval5000: any;
  public interval2000: any;
  public isScreenActive = true;
  public url: string;
  public isNewGame: boolean;
  public primaryColor: string;
  platforms: string = 'desktop';

  constructor(inj: Injector,
              private share: SharedataService,
              private apiservice: APIService,
              private env: EnvService,
              private hapi: AuthService) {
    super(inj);
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.loaderShow = true;
    this.share.teenPattiOpen.emit(true); // CALL TO HEADER FULL SCREEN
    this.isNewGame = this.activatedRoute.snapshot.params.id ? true : false;
    this.url = env.LIVE_GAME_2_URL; // https://faas.sports999.in/#/fs?
    this.url += 'token=' + JSON.parse(localStorage.getItem('currentUser')).token;
    this.url += '&operatorId=1356'; // 1222
    this.url += '&language=en';
    this.url += '&stakes=' + this.getToken('betstack');
    this.url += '&onclickstake=' + this.getToken('betstack');
  }

  ngOnInit(): void {
    const body = document.getElementsByTagName('body')[0];
    this.platforms = body.dataset.platform;
    this.changedRef.detectChanges();
    this.spinner.show();
    this.getBalance();
  }

  getBalance() { // user-data'
    this.hapi.headerCall('?type=teenpatti').subscribe((response: any) => {
      this.cDetails = {
        expose: response.data.balance.expose,
        detail: response.data.teenPattiData,
        status: response.data.teenPattiStatus,
        status2: response.data.live_game_2_status
      };
      this.share.setBalanceChange(response.data.balance.balance);
      this.share.setExposeChange(response.data.balance.expose);
      this.operaterId = response.data.teenpatti_operatorId;
      if (this.loaderShow && this.operaterId !== null) {
        this.openTeenPatti();
      }
      this.changedRef.detectChanges();
      if (!this.isDestroy) {
        this.interval2000 = setTimeout(() => {
          this.getBalance();
        }, config.getBalanceTime);
      }
    });
  }

  openTeenPatti() { // INIT IFRAME
    const body = document.getElementsByTagName('body')[0];
    this.userToken = JSON.parse(localStorage.getItem('currentUser')).token;
    this.userToken += '/' + this.operaterId;
    let iurl = this.env.LIVE_GAME_1_D_URL + this.userToken;
    if (body.dataset.platform === 'mobile') {
      iurl = this.env.LIVE_GAME_1_M_URL + this.userToken;
    }
    if (this.isNewGame) {
      iurl = this.url;
    }
    this.loaderShow = false;
    this.yt = '<iframe class="w-100" src="' + iurl + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="height: -webkit-fill-available;"></iframe>';
    if (body.dataset.platform === 'mobile') {
      this.changedRef.detectChanges();
      this.iframeLoad1.nativeElement.innerHTML = this.yt;
    } else if (this.iframeLoad) {
      this.changedRef.detectChanges();
      this.iframeLoad.nativeElement.innerHTML = this.yt;
    }
    this.spinner.hide();
  }

  backToHome() { // BACK TO HOME SCREEN
    this.router.navigate(['main']);
  }

  ngOnDestroy() {
    this.share.teenPattiOpen.emit(false);
    this.share.setRouterChange('1'); // CALL HEADER APIS
    this.isDestroy = true;
    clearTimeout(this.interval2000);
  }

}
