import {Component, OnInit, OnDestroy, Injector} from '@angular/core';
import {BaseComponent} from './../../share/components/common.component';
import {AlertService, APIService, SharedataService, sysconfig} from '../../core/services';
import {BetplaceComponent} from '../../share/components/bottom-sheet/betplace.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {NavigationEnd} from '@angular/router';

declare var $;

@Component({
  selector: 'app-cc-details',
  templateUrl: './cc-details.component.html',
  styleUrls: ['./cc-details.component.css']
})
export class CcDetailsComponent extends BaseComponent implements OnInit, OnDestroy {

  public ccList: any;
  public plTitle: string;
  public eventMsg: string;
  public currantBets: any;
  public runnerName: string;
  public fancy2PL: any;
  public fancyBets: any;

  // FLAG
  public isDestroy: boolean;
  public isBetOpen: boolean;
  public isLoadFancyPl: boolean;
  public isActiveTab: string;
  public isActiveList: number = 0;
  private lId: string;
  public loaderShow: boolean;
  public betIndex: number = -1;
  private navigationSubscription;
  public primaryColor: string;

  constructor(inj: Injector, private alert: AlertService, private share: SharedataService, private commonService: APIService, private placeBetSheet: MatBottomSheet) {
    super(inj);
    this.lId = this.activatedRoute.snapshot.params.id;
    if (this.lId) {
      this.share.eventId.emit(this.lId); // CALL TO BALANCE API
    }
    // this.commonService.setRouterChange('1'); // CALL TO HEADER API
    this.isActiveTab = 'live';
    this.loaderShow = false;
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        console.log(this.activatedRoute.snapshot.params.id);
        if (this.lId !== undefined && this.lId !== this.activatedRoute.snapshot.params.id) {
          this.reloadCurrentRoute();
        }
      }
    });
  }

  ngOnInit() {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.spinner.show();
    this.getCricketCasino(); // CALL TO EVENT DATA
    this.getBetList(); // GET CURRANT EVENTS BETS LIST

    this.share.betStatus.subscribe((res) => { // Bet Flags Manage
      if (res === 1) {
      } else if (res === 2) {
        this.loaderShow = true;
        this.spinner.show();
      } else if (res === 3) {
        this.spinner.hide();
        this.loaderShow = false;
        this.getCricketCasino();
        this.share.setRouterChange('1'); // CALL TO HEADER API
        this.getBetList();
      }
    });
  }

  ngOnDestroy() { // CLEAR ALL VARIABLES AND SERVICE
    this.isDestroy = true;
    this.share.eventId.emit('');
    this.share.betslist.emit('');
    this.clearVal();
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  back() {
    this.loc.back();
  }

  clearVal() {
    this.eventMsg = '';
    this.ccList = [];
    this.isActiveTab = '';
  }

  reloadCurrentRoute() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  getCricketCasino() {
    this.commonService.getCCDetail(this.lId).subscribe((res) => {
      if (res.status === 1 && res.data !== null) {
        this.ccList = res.data.items;
        res.data.items.lottery.forEach((item, index) => {
          if (index === 0 && this.isActiveList === 0) {
            this.ccList.lottery[index].isActive = true;
          } else if (index === this.isActiveList) {
            this.ccList.lottery[index].isActive = true;
          } else {
            this.ccList.lottery[index].isActive = false;
          }
        });

      } else {
        console.log(res.message);
        this.eventMsg = res.message;
        this.ccList = [];
        if (!this.isDestroy && this.eventMsg !== '') {
          // this.interval10000 = setTimeout(() => { this.getLotteryData()}, this.getToken('eventDetailTiming') !== null ?
          // parseInt(this.getToken('eventDetailTiming')) : config.detailTime);
        }
      }
      this.spinner.hide();
      this.changedRef.detectChanges();
    });
  }

  togglePanle(index: number) {
    this.ccList.lottery[index].isActive = !this.ccList.lottery[index].isActive;
    this.isActiveList = index;
    this.changedRef.detectChanges();
  }

  isActive(type: string) {
    this.isActiveTab = type;
    this.share.setRouterChange('1'); // CALL TO HEADER API
    this.getBetList();
  }

  /**
   * ADD TO BET SLIP
   * @param index : CURRANT SELECTION RAW
   * @param mid : MARKET ID
   * @param rate : CURRANT RAW RATE
   * @param number :  Number
   */
  openbet(index, mid, rates, numbers) {
    this.betIndex = index;
    this.isActiveList = index;
    const initBet = {
      price: numbers,
      size: '',
      runner: this.ccList.lottery[index].title,
      bet_type: 'back',
      sec_id: numbers,
      market_id: this.ccList.lottery[index].market_id,
      event_id: this.ccList.lottery[index].event_id,
      m_type: 'cricket_casino',
      eventname: this.ccList.title,
      market_name: this.ccList.title,
      slug: 'cricket_casino',
      rate: rates,
      sport_id: this.ccList.lottery[index].sportId
    };

    const plLimit = {
      minStack: this.ccList.lottery[index].minStack.toString(),
      maxStack: this.ccList.lottery[index].maxStack.toString(),
      maxProfitLimit: this.ccList.lottery[index].maxProfitLimit.toString()
    };
    this.placeBetSheet.open(BetplaceComponent, {
      disableClose: true,
      data: {int: initBet, market: [], limit: plLimit, index: index}
    });
  }

  minusData(id: number) { // NUMBER CHANGE EVENTS
    if (!this.isBetOpen) {
      const elemnt: any = <HTMLElement> document.getElementById('lnumber-' + id);
      let tmp1: any = Number(elemnt.value);
      if (tmp1 > 0) {
        tmp1 -= 1;
        this.ccList.lottery[id].number = tmp1;
      }
    } else {
      this.alert.error('First close bet slip and try again..!');
    }
  }

  plusData(id: number) { // NUMBER PLUSE EVENST
    if (!this.isBetOpen) {
      const elemnt: any = <HTMLElement> document.getElementById('lnumber-' + id);
      let tmp1: any = Number(elemnt.value);
      if (tmp1 < 9) {
        tmp1 += 1;
        this.ccList.lottery[id].number = tmp1;
      } else {
        this.alert.error('Maximum number is 9');
      }
    } else {
      this.alert.success('First close bet slip and try again..!');
    }
  }

  showBook(index: number) { // OPEN BOOKS
    if (this.ccList.lottery[index].is_book === '1') { // CHECK IS BOOK REQUEST IS VALID OR NOT
      this.isLoadFancyPl = false;
      this.runnerName = this.ccList.lottery[index].title;
      $('#Book-Unbook').modal('show'); // OPEN MODAL WINDOW
      if (this.ccList.lottery[index].market_id) {
        const data = {
          event_id: this.ccList.lottery[index].event_id,
          market_id: this.ccList.lottery[index].market_id,
          session_type: 'cricket_casino',
          price: this.ccList.lottery[index].rate
        };

        this.commonService.getBetList(data).subscribe((response: any) => {
          this.isLoadFancyPl = true;
          if (response.status === 1 && response.data) {
            // this.fancy2PL = response.data.profit_loss;
            this.fancy2PL = this.getProfitLossLotteryBook(response.data.betList);
            console.log(this.fancy2PL);
            this.fancyBets = response.data.betList;
          }
          this.changedRef.detectChanges();
        });
      }
    }
  }

  // **BETLIST**************************/
  getBetList() {
    this.commonService.getMatchedUnmatchedBet(this.lId).subscribe((res) => {
      if (res.status === 1 && res.data !== null && res.data.matched !== null && res.data.matched[11]) {
        this.currantBets = res.data.matched[this.sysConfigDt.CC_BET_INDEX];
      }
      this.changedRef.detectChanges();
    });
  }
}

