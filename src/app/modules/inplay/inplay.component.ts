import { Component, OnInit, Injector } from '@angular/core';
import {APIService} from '../../core/services';
import {BaseComponent} from '../../share/components/common.component';

@Component({
  selector: 'app-inplay',
  templateUrl: './inplay.component.html',
  styleUrls: ['./inplay.component.css']
})
export class InplayComponent extends BaseComponent implements OnInit {

  sportName: string;
  public inplayList: any;
  public inplayListTmp: any;
  public primaryColor: string;
  public assetsUrl: string;

  constructor(inj: Injector, private apiservice: APIService) {
    super(inj);
    this.sportName = 'inplay';
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
  }

  ngOnInit(): void {
    this.spinner.show();
    this.getInplayMatches();
  }

  onDashboradClick(sport: string) {
    this.sportName = sport;
    this.inplayList = this.inplayListTmp[sport];
    this.changedRef.detectChanges();
  }

  getInplayMatches() {
    this.apiservice.getInplaylist().subscribe((res) => {
      this.inplayList = res.data[this.sportName];
      this.inplayListTmp = res.data;
      let tmp = 0;
      res.data.inplay.forEach((item) => {
        tmp += item.list.length;
      });
      // this.commonService.setInplayChange(tmp);
      this.spinner.hide();
      this.changedRef.detectChanges();
    }, (error) => {
      this.spinner.hide();
    });
  }

}
