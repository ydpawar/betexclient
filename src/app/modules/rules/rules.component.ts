import {Component, OnInit, Injector} from '@angular/core';
import {APIService} from '../../core/services';
import {BaseComponent} from '../../share/components/common.component';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent extends BaseComponent implements OnInit {

  public rulesList: any;
  public primaryColor: string;

  constructor(inj: Injector, private apiservice: APIService) {
    super(inj);
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
  }

  ngOnInit(): void {
    this.spinner.show();
    this.getRulesList();
  }

  getRulesList() {
    this.apiservice.getRules().subscribe((res) => {
      if (res.status === 1) {
        this.rulesList = res.data;
      }
      this.changedRef.detectChanges();
      this.spinner.hide();
    });
  }

}
