import {Component, OnInit, Injector} from '@angular/core';
import {ACAPIService, ExcelService} from '../../../core/services';
import {BaseComponent} from '../../../share/components/common.component';
import {ProfitLossBetObj, ProfitLossBetList, CasinoBetObj, CasinoBetList} from '../../../share/models';

@Component({
  selector: 'app-bet-history',
  templateUrl: './bet-history.component.html',
  styleUrls: ['./bet-history.component.css']
})
export class BetHistoryComponent extends BaseComponent implements OnInit {
  public sportId: string;
  public cList: ProfitLossBetList[];
  public filter: any = {};
  public isFilter: boolean = true;
  public loading = false;
  public isLoading: boolean;
  public title: string = 'Bet History';
  public filterData: object;
  public isFirst: number = 1;
  public primaryColor: string;
  public isDownload: number;

  constructor(inj: Injector, private service: ACAPIService, private excel: ExcelService) {
    super(inj);
    this.isLoading = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    this.isDownload = 1;
  }

  ngOnInit(): void {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.sportId = this.activatedRoute.snapshot.params.sid;
  }

  /**
   * DETECT FILTER DATA
   * @param filter GET DATES
   */
  filterChangedHandler(filter: any) {
    this.filterData = {start_date: filter.start_date, end_date: filter.end_date, isFirst: filter.isFirst, cancel: filter.isCancel};
    if (filter === 1) {
      this.excelDownload();
    }
    if (filter.start_date && filter.end_date) {
      this.getEventProfitLoss();
    }
  }

  getEventProfitLoss() {
    this.spinner.show();
    this.service.getBetHistory(this.filterData, 'bet-history').subscribe((res) => this.intProfitLoss(res));
  }

  intProfitLoss(res) {
    this.cList = [];
    if (res.status === 1 && res.data !== null) {
      for (const item of res.data.items) {
        const cData = new ProfitLossBetObj(item);
        cData.betId = item._id.$oid;
        cData.createdOn = item.created_on;
        this.cList.push(cData);
      }
    } else {
      this.cList = null;
    }
    this.changedRef.detectChanges();
    this.spinner.hide();
  }

  excelDownload() {
    const tmpData = [];
    const tmp = {
      ID: '',
      DESCRIPTION: '',
      SIDE: '',
      PRICE: '',
      STAKE: '',
      DATE: '',
      PL: '',
      STATUS: ''
    };
    tmpData.push(tmp);
    this.cList.forEach((item) => {
      const pl = item.result === 'WIN' ? item.win : item.loss;
      const tmp = {
        ID: item.betId,
        DESCRIPTION: item.description,
        SIDE: item.bType,
        PRICE: item.price,
        STAKE: item.size,
        DATE: item.createdOn,
        PL: pl,
        STATUS: item.result

      };
      tmpData.push(tmp);
    });
    this.excel.exportAsExcelFile(tmpData, 'Bet History');
  }
}

// tslint:disable-next-line:jsdoc-format
/** CASINO-BET-HISTORY************************************************************/

@Component({
  selector: 'app-casino-history',
  templateUrl: './casino.component.html'
})
export class CasinoBetHistoryComponent extends BaseComponent implements OnInit {
  public sportId: string;
  public cList: CasinoBetList[];
  public filter: any = {};
  public isFilter: boolean = true;
  public loading = false;
  public isLoading: boolean;
  public title: string = 'Casino Bet History';
  public filterData: object;
  public isFirst: number = 1;
  public primaryColor: string;
  public isDownload: number;

  constructor(inj: Injector, private service: ACAPIService, private excel: ExcelService) {
    super(inj);
    this.isLoading = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    this.isDownload = 1;
  }

  ngOnInit(): void {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.sportId = this.activatedRoute.snapshot.params.sid;
  }

  /**
   * DETECT FILTER DATA
   * @param filter GET DATES
   */
  filterChangedHandler(filter: any) {
    this.filterData = {start_date: filter.start_date, end_date: filter.end_date, isFirst: filter.isFirst, cancel: filter.isCancel};
    if (filter === 1) {
      this.excelDownload();
    }
    if (filter.start_date && filter.end_date) {
      this.getEventProfitLoss();
    }
  }

  getEventProfitLoss() {
    this.spinner.show();
    this.service.getBetHistory(this.filterData, 'casino-bet-history').subscribe((res) => this.intCasinoData(res));
  }

  intCasinoData(res) {
    this.cList = [];
    if (res.status === 1 && res.data !== null) {
      for (const item of res.data.items) {
        const cData = new CasinoBetObj(item);
        cData.betId = item._id.$oid;
        cData.win = item.amount;
        cData.loss = item.amount;
        cData.result = item.type === 'DEBIT' ? 'LOSS' : 'WIN';
        this.cList.push(cData);
        console.log(this.cList);
      }
    } else {
      this.cList = null;
    }
    this.changedRef.detectChanges();
    this.spinner.hide();
  }

  excelDownload() {
    const tmpData = [];
    const tmp = {
      ID: '',
      DESCRIPTION: '',
      SIDE: '',
      PRICE: '',
      STAKE: '',
      DATE: '',
      PL: '',
      STATUS: ''
    };
    tmpData.push(tmp);
    this.cList.forEach((item) => {
      const tmp = {
        ID: item.betId,
        DESCRIPTION: item.description,
        SIDE: item.bType,
        PRICE: item.amount,
        STAKE: item.amount,
        DATE: item.date,
        PL: item.amount,
        STATUS: item.result
      };
      tmpData.push(tmp);
    });
    this.excel.exportAsExcelFile(tmpData, 'casino-bet-history');
  }
}
