import {Component, OnInit, Injector, ViewChild, ElementRef, ChangeDetectionStrategy} from '@angular/core';
import {AuthService} from '../../../core/services';
import {BaseComponent} from '../../../share/components/common.component';
import {FormBuilder, FormGroup, Validators, AbstractControl} from '@angular/forms';
import * as $ from 'jquery';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent extends BaseComponent implements OnInit {

  @ViewChild('stackValue', {static: false}) stackValues: ElementRef;
  public amount: number = 0;
  public stackValue: number;
  public errorMsg: string;
  public errorMsg1: string;
  public stackPriceArr: any = [];
  public bindButton: any;
  public profileData: any;

  public frm: FormGroup;
  public opasswordType: string;
  public passwordType: string;
  public cpasswordType: string;
  public submitted: boolean;

  constructor(inj: Injector, private service: AuthService, private form: FormBuilder) {
    super(inj);
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    this.submitted = false;
    this.createForm();
  }

  ngOnInit(): void {
    this.intiCOMP();
    this.getProfileData();
    this.getAndSetBetOptions();
  }

  intiCOMP() {
    this.opasswordType = 'password';
    this.passwordType = 'password';
    this.cpasswordType = 'password';
    this.submitted = false;
  }

  getProfileData() {
    this.service.getUserProfile().subscribe((res) => {
      if (res.status === 1) {
        this.profileData = res.data;
      }
      this.changedRef.detectChanges();
    });
  }

  getAndSetBetOptions() {
    if (this.getToken('betstack') !== null) {
      // this.stackPriceArr = this.getToken('betstack').split(',');
      this.stackPriceArr = this.optionSplit(this.getToken('betstack'));
    } else {
      this.service.headerCall().subscribe((res) => {
        if (res.status === 1) {
          if (res.data.betOption) {
            this.stackPriceArr = this.optionSplit(res.data.betOption);
            this.setToken('betstack', res.data.betOption);
          }
        }
      });
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  optionSplit(data) {
    const opData = data.split(',');
    const tmp = [];
    let strOption = '';
    opData.forEach((item, index) => {
      if (index < 5) {
        tmp.push(item);
        if (strOption === '') {
          strOption += item;
        } else {
          strOption += ',' + item;
        }
      }
    });
    this.setToken('betstack', strOption);
    return tmp;
  }

  saveStack() {
    if (this.stackPriceArr.length > 0) {
      const strOptions = this.stackPriceArr + '';
      const data = {bet_option: strOptions};
      this.setToken('betstack', strOptions);
      this.service.updateBetOptions(data).subscribe((response: any) => {
        if (response.status === 1) {
          // this.alert.success(response.message);
        }
      });
    }
  }

  addStackPrice() {
    this.errorMsg1 = '';
    this.stackValue = Number(this.stackValues.nativeElement.value);
    if (this.stackValue !== null && this.stackValue !== 0 && this.stackValue !== undefined && this.stackValue >= 10) {
      if (this.stackPriceArr.length <= 4) {
        let index = $.map(this.stackPriceArr, (item) => {
          return item;
        }).indexOf(this.stackValue);
        if (index < 0) {
          this.stackPriceArr.push(this.stackValue);
          this.stackPriceArr = this.stackPriceArr.sort((a, b) => a - b);
          this.stackValue = -1;
          // this.updateOprionInLocalStorage();
          this.stackValues.nativeElement.value = '';
          this.saveStack();
        } else {
          this.errorMsg1 = 'Already exist..!! Please try another..';
        }
      } else {
        this.errorMsg1 = 'Minimum stack button Five!';
      }
    } else if (this.stackValue !== null && (this.stackValue === 0 || this.stackValue === undefined)) {
      this.errorMsg1 = 'stack button value zero or empty not valid!';
    } else if (this.stackValue !== null && this.stackValue < 10) {
      this.errorMsg1 = 'Minimum stack button value is greater than 10 or equal!';
    }
  }

  /**
   * Remove stack in array
   * @param index pass stack amount index to remove from list
   */
  removeStackPrice(index: number) {
    delete this.stackPriceArr[index];
    let i = 0;
    const newArr = new Array();
    this.bindButton = '';
    this.stackPriceArr.forEach(item => {
      if (this.stackPriceArr[i] !== '') {
        newArr.push(item);
      }
      i++;
    });
    this.stackPriceArr = newArr;

    if (this.stackPriceArr.length === 0) {
    }
    this.saveStack();
  }

  get frmopassword() {
    return this.frm.get('oldpassword');
  }

  get frmpassword() {
    return this.frm.get('password');
  }

  get frmcpassword() {
    return this.frm.get('cpassword');
  }

  createForm() {
    this.frm = this.form.group({
      oldpassword: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6),
        Validators.maxLength(20),
        Validators.pattern(new RegExp('^((?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#\$%\^&\*]).{6,20})'))]],
      cpassword: ['', Validators.required]
    });
  }

  changeUserPassword() {
    this.submitted = true;
    if (this.frm.valid && (this.frm.value.password === this.frm.value.cpassword)) {
      this.service.changePassword(this.frm.value).subscribe(success => {
        if (success.status === 1) {
          this.removeToken('mobileUser');
          this.removeToken('events');
          this.removeToken('myExpose');
          sessionStorage.clear();
          localStorage.clear();
          this.router.navigate(['/auth']);
        } else {
          this.errorMsg = success.message;
        }
      });
    }
  }
}

