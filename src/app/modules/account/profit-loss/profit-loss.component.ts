import {Component, OnInit, Injector, OnDestroy} from '@angular/core';
import {BaseComponent} from '../../../share/components/common.component';
import {ACAPIService, ExcelService, SharedataService} from '../../../core/services';
import {
  ProfitLossSportList, ProfitLossSportObj, ProfitLossEventList, ProfitLossEventObj,
  ProfitLossMarketObj, ProfitLossMarketList, ProfitLossBetList, ProfitLossBetObj, AccountStatementList
} from '../../../share/models';

@Component({
  selector: 'app-profit-loss',
  templateUrl: './profit-loss.component.html',
  styleUrls: ['./profit-loss.component.css']
})
export class ProfitLossComponent extends BaseComponent implements OnInit, OnDestroy {

  public cList: ProfitLossSportList[];
  public eList: ProfitLossEventList[];
  public isLoadToOtherPRNT: any;
  public loading = false;
  public filter: any = {};
  public isLoading = false;
  public eventId: string;
  public marketId: string;
  public mtype: string;
  public sId: string;
  public totalProfit: number;
  public totalAmount: number;
  public title: string;
  public isDownload: number;
  public isFirst: number = 1;
  public filterData: object;
  public eventdata: any = {};
  public type: number;
  public collapseCls: string = '';
  public spoName: string;
  public isLoadFirst: boolean;

  constructor(
    inj: Injector,
    private service: ACAPIService,
    private excel: ExcelService
  ) {
    super(inj);
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    this.isLoading = false;
    this.sId = this.activatedRoute.snapshot.params.sid;
    this.eventId = this.activatedRoute.snapshot.params.eid;
    this.marketId = this.activatedRoute.snapshot.params.mid;
    this.mtype = this.activatedRoute.snapshot.params.mtype;

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.ac !== undefined && params.ac === '1') {
        this.isLoadToOtherPRNT = true;
      }
    });
    if (!this.isLoadToOtherPRNT) {
    }
    this.isLoadFirst = true;
  }

  ngOnInit(): void {
    this.title = 'Profit-Loss';
    this.isDownload = 0;
  }

  /**
   * DETECT FILTER DATA
   * @param filter GET DATES
   */
  filterChangedHandler(filter: any) {
    this.filterData = {start_date: filter.start_date, end_date: filter.end_date, isFirst: filter.isFirst};
    this.getEventProfitLoss();
    if (!this.isLoadFirst && this.eList !== undefined && this.spoName !== undefined && this.type !== undefined) {
      this.event(this.type, this.spoName, 1);
    }
  }

  getEventProfitLoss() {
    this.isLoadFirst = false;
    this.service.getProfitLoss(this.filterData).subscribe((res) => this.intProfitLoss(res));
  }

  ngOnDestroy(): void {
  }

  intProfitLoss(res) {
    this.spinner.show();
    this.isLoading = false;
    this.cList = [];
    if (res.status === 1 && res.data) {
      for (const item of res.data.item) {
        const cData = new ProfitLossSportObj(item);
        this.cList.push(cData);
      }
      this.totalProfit = res.data.totalAmount;
    } else {
      this.cList = null;
    }
    this.changedRef.detectChanges();
    this.loading = false;
    this.spinner.hide();
  }

  scrollToQuestionNode(id) {
    const doc = document.getElementsByTagName('body')[0];
    if (doc.dataset.platform === 'mobile') {
      const element = document.getElementById(id);
      element.scrollIntoView({block: 'end', behavior: 'smooth'});
    }
  }

  event(id, sName, type = 0) {
    this.spoName = sName;
    this.collapseCls = 'show';
    if (this.type === id && !type) {
      this.collapseCls = '';
      this.type = 0;
    } else {
      this.type = id;
    }

    this.eventdata = this.filterData;
    const data = {start_date: this.eventdata.start_date, end_date: this.eventdata.end_date, sid: id, type: this.type};
    this.service.getProfitlossEvent(data).subscribe((res) => {
      this.eList = [];
      if (res.status === 1 && res.data && res.data.item !== null) {
        this.totalAmount = res.data.totalAmount;
        for (const item of res.data.item) {
          const cData = new ProfitLossEventObj(item);
          cData.sportName = item.sport_name;
          cData.settledDate = item.settled_date;
          this.eList.push(cData);
        }
      } else {
        this.eList = null;
      }
      this.scrollToQuestionNode('Profit-Loss');
      this.spinner.hide();
      this.changedRef.detectChanges();
    }, (error) => {
      this.spinner.hide();
    });
  }

  excelDownload() {
    const tmpData = [];
    const tmp = {
      SETTLED_DATE: '',
      EVENT_ID: '',
      EVENT_NAME: '',
      PROFIT_LOSS: ''
    };
    tmpData.push(tmp);
    this.eList.forEach((item: ProfitLossEventList) => {

      // tslint:disable-next-line:no-shadowed-variable
      const tmp = {
        SETTLED_DATE: item.settledDate,
        EVENT_ID: item.eventId,
        EVENT_NAME: item.eventName,
        PROFIT_LOSS: item.profitLoss
      };
      tmpData.push(tmp);
    });
    this.excel.exportAsExcelFile(tmpData, 'profitloss-eventlist');
  }
}

@Component({
  selector: 'app-profit-loss-market',
  templateUrl: './profit-loss-market.component.html',
  styleUrls: ['./profit-loss.component.css']
})
export class ProfitLossMarketComponent extends BaseComponent implements OnInit {

  public sportId: string;
  public eId: string;
  public plList: ProfitLossMarketList[];
  public title: string;
  public isDownload: number;
  public filterData: object;
  public isLoading: boolean;
  public primaryColor: string;
  public totalAmount: number;

  constructor(inj: Injector, private share: SharedataService, private service: ACAPIService) {
    super(inj);
    this.share.setRouterChange('1'); // CALL TO HEADER API
    this.sportId = this.activatedRoute.snapshot.params.id;
    this.eId = this.activatedRoute.snapshot.params.eid;
    this.isLoading = true;
  }

  ngOnInit() {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.spinner.show();
    this.title = 'Profit Loss';
    this.isDownload = 0;
    if (this.sportId === '999' || this.sportId === '9999') {
      this.filterData = {eid: this.eId};
      this.getProfitLoss();
    } else {
      this.filterData = {eid: this.eId};
      this.getProfitLoss();
    }
  }

  /**
   * DETECT FILTER DATA
   * @param filter GET DATES
   */
  filterChangedHandler(filter: any) {
    this.filterData = {start_date: filter.start_date, end_date: filter.end_date, eid: this.eId};
    this.getProfitLoss();
  }

  getProfitLoss() {
    this.service.getProfitLossMarket(this.filterData).subscribe((res) => {
      this.plList = [];
      if (res.status === 1 && res.data !== null) {
        this.totalAmount = res.data.totalAmount;
        for (const item of res.data.items) {
          const cData = new ProfitLossMarketObj(item);
          cData.settledDate = item.settled_time;
          this.plList.push(cData);
        }
      } else {
        this.plList = null;
      }
      this.changedRef.detectChanges();
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
  }

}

@Component({
  selector: 'app-profit-loss-bet',
  templateUrl: './profit-loss-bet.component.html',
  styleUrls: ['./profit-loss.component.css']
})
export class ProfitLossBetComponent extends BaseComponent implements OnInit {
  public type: string;
  public mType: string;
  public mId: string;
  public slug: string;
  public plList: ProfitLossBetList[];
  public title: string;
  public filterData: object;
  public isLoading: boolean;
  public isFirst: number = 1;
  public primaryColor: string;
  public isDownload: number;

  constructor(inj: Injector, private share: SharedataService, private service: ACAPIService) {
    super(inj);
    this.share.setRouterChange('1'); // CALL TO HEADER API
    this.mType = this.activatedRoute.snapshot.params.mtype;
    this.mId = this.activatedRoute.snapshot.params.mid;
    const sid = this.activatedRoute.snapshot.params.id;
    this.type = sid === '99' ? 'livegame1' : (sid === '999' ? 'livegame2' : (sid === '9999' ? 'casino' : '')) || '';
    this.isLoading = true;
    this.activatedRoute.url.subscribe((res: any) => {
      if (res && res[1].path === 'teenpatti') {
        this.slug = 'teenpatti';
      }
    });
  }

  ngOnInit() {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.spinner.show();
    this.title = 'Profit Loss Bet';
    this.isDownload = 0;
    if (this.type !== 'livegame1') {
      this.filterData = {
        mType: this.mType,
        market_id: this.mId,
        type: this.type
      };
      this.getProfitLoss();
    }
  }

  /**
   * DETECT FILTER DATA
   * @param filter GET DATES
   */
  filterChangedHandler(filter: any) {
    this.filterData = {
      start_date: filter.start_date,
      end_date: filter.end_date,
      mType: this.mType,
      event_id: this.mId,
      isFirst: 0,
      type: this.type
    };
    this.getProfitLoss();
  }

  getProfitLoss() {
    const data = this.filterData;
    // if (this.slug !== 'teenpatti') {
    //   data = {market_id: this.mId, mType: this.mType}; // event_id: ''
    // }
    this.service.getProfitLossBets(data).subscribe((res) => {
      this.isFirst = 0;
      this.plList = [];
      if (res.status === 1 && res.data !== null) {
        for (const item of res.data) {
          const cData = new ProfitLossBetObj(item);
          cData.createdOn = item.created_on;
          cData.betId = item._id.$oid;
          this.plList.push(cData);
        }
      } else {
        this.plList = null;
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
  }

}
