import {Component, OnInit, Injector} from '@angular/core';
import {ACAPIService, ExcelService} from '../../../core/services';
import {BaseComponent} from '../../../share/components/common.component';
import {ProfitLossBetList, ProfitLossBetObj} from '../../../share/models';

@Component({
  selector: 'app-teenpatti-bet-history',
  templateUrl: './teenpatti-bet-history.component.html',
  styleUrls: ['./teenpatti-bet-history.component.css']
})
export class TeenpattiBetHistoryComponent extends BaseComponent implements OnInit {
  public sportId: string;
  public filter: any = {};
  public isFilter: boolean = true;
  public loading = false;
  public isLoading: boolean;
  public title: string;
  public filterData: any;
  public lbList: ProfitLossBetList[];
  private isFirst: number;
  public primaryColor: string;
  public isDownload: number;

  constructor(inj: Injector, private service: ACAPIService, private excel: ExcelService) {
    super(inj);
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    this.title = 'Teenpatti Bet History';
    this.isDownload = 1;
    this.isFirst = 1;
  }

  ngOnInit(): void {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.isLoading = true;
  }

  /**
   * DETECT FILTER DATA
   * @param filter GET DATES
   */
  filterChangedHandler(filter: any) {
    this.filterData = {start_date: filter.start_date, end_date: filter.end_date, isFirst: filter.isFirst, cancel: filter.isCancel};
    if (filter === 1) {
      this.excelDownload();
    }
    if (filter.start_date && filter.end_date) {
      this.getLiveBets();
    }
  }

  getLiveBets() {
    this.isLoading = false;
    this.spinner.show();
    this.service.getTeenpattiBetHistroy(this.filterData).subscribe((res) => {
      this.lbList = [];
      this.isFirst = 0;
      if (res.status === 1 && res.data) {
        for (const item of res.data.items) {
          const cData = new ProfitLossBetObj(item);
          cData.betId = item._id.$oid;
          cData.createdOn = item.created_on;
          this.lbList.push(cData);
        }
      } else {
        this.lbList = null;
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
    this.loading = false;
  }

  excelDownload() {
    const tmpData = [];
    const tmp = {
      ID: '',
      DESCRIPTION: '',
      SIDE: '',
      PRICE: '',
      STAKE: '',
      DATE: '',
      PL: '',
      STATUS: ''
    };
    tmpData.push(tmp);
    this.lbList.forEach((item) => {
      const tmp = {
        ID: item.betId,
        DESCRIPTION: item.description,
        SIDE: item.bType,
        PRICE: item.price,
        STAKE: item.size,
        DATE: item.date,
        PL: item.result === 'WIN' ? item.win : item.loss,
        STATUS: item.result

      };
      tmpData.push(tmp);
    });
    this.excel.exportAsExcelFile(tmpData, 'Teenpatti Bet History');
  }
}
