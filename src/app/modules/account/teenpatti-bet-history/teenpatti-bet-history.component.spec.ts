import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeenpattiBetHistoryComponent } from './teenpatti-bet-history.component';

describe('TeenpattiBetHistoryComponent', () => {
  let component: TeenpattiBetHistoryComponent;
  let fixture: ComponentFixture<TeenpattiBetHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeenpattiBetHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeenpattiBetHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
