import {Component, OnInit, Injector} from '@angular/core';
import {ACAPIService} from '../../../core/services/acapi.service';
import {BaseComponent} from '../../../share/components/common.component';
import {ProfitLossBetList, ProfitLossBetObj} from '../../../share/models';

@Component({
  selector: 'app-live-bets',
  templateUrl: './live-bets.component.html',
  styleUrls: ['./live-bets.component.css']
})
export class LiveBetsComponent extends BaseComponent implements OnInit {

  public liveBetList: ProfitLossBetList[];
  public primaryColor: string;
  public isNoData: boolean;

  constructor(inj: Injector, private service: ACAPIService) {
    super(inj);
    this.isNoData = false;
  }

  ngOnInit(): void {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.spinner.show();
    this.getLiveBet();
  }

  getLiveBet() {
    this.service.getLiveBets({}).subscribe((res) => {
      if (res.status === 1 && res.data !== null) {
        this.isNoData = false;
        this.liveBetList = [];
        for (const item of res.data) {
          const cData = new ProfitLossBetObj(item);
          cData.betId = item._id.$oid;
          cData.createdOn = item.created_on;
          this.liveBetList.push(cData);
        }
      } else {
        this.isNoData = true;
        this.liveBetList = null;
      }
      this.spinner.hide();
      this.changedRef.detectChanges();
    });
  }

}
