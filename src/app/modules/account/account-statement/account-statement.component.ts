import {Component, OnInit, Injector, ViewChild, ElementRef} from '@angular/core';
import {BaseComponent} from '../../../share/components/common.component';
import {ACAPIService, ExcelService} from '../../../core/services';
import {AccountStatementList, AccountStatementObj, AccountStatementBetObj, AccountStatementBetList} from '../../../share/models';


@Component({
  selector: 'app-account-statement',
  templateUrl: './account-statement.component.html',
  styles: ['.cancel a{ text-decoration: line-through !important; color: #898686 !important;}']
})
export class AccountStatementComponent extends BaseComponent implements OnInit {
  @ViewChild('epltable') epltable: ElementRef;
  @ViewChild('bBody') bBody: ElementRef;
  public page: number;
  public acmId: string;
  public acList: AccountStatementList[];
  public title: string;
  public filterData: object;
  public isLoading: boolean;
  public isFirst: number;
  public primaryColor: string;
  public filter: any = {};
  public loading = false;
  public notscrolly: boolean;
  public notEmptyPost: boolean;
  public pageNo: number;
  public filterType: string;
  public exceldownload: number;
  public isDownload: number;

  constructor(inj: Injector, private service: ACAPIService, private excel: ExcelService) {
    super(inj);
    this.isLoading = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    this.isFirst = 1;
    this.page = 1;
    this.notscrolly = true;
    this.notEmptyPost = true;
    this.pageNo = 1;
    this.filterType = 'today';
    this.acList = [];
  }

  ngOnInit(): void {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.spinner.show();
    this.title = 'Account Statement';
    this.isDownload = 1;
  }

  /**
   * DETECT FILTER DATA
   * @param filter GET DATES
   */
  filterChangedHandler(filter: any) {
    this.filterData = {start_date: filter.start_date, end_date: filter.end_date, isFirst: filter.isFirst};
    if (filter === 1) {
      this.excelDownload();
    }
    if (filter.start_date && filter.end_date) {
      this.getAccountDetail();
    }
  }

  getAccountDetail() {
    this.isLoading = false;
    this.isFirst = 0;
    this.service.getAccountStatment(this.filterData).subscribe((res) => {
      this.acList = [];
      if (res.status === 1 && res.data !== null) {
        for (const item of res.data) {
          const cData = new AccountStatementObj(item);
          cData.createdOn = item.created_on;
          this.acList.push(cData);
        }
      }
      this.changedRef.detectChanges();
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });

    this.loading = false;
  }

  onScroll() {
    if (this.notscrolly && this.notEmptyPost) {
      // this.spinner.show();
      this.notscrolly = false;
      this.pageNo++;
      this.loadNextPost();
    }
  }

  loadNextPost() {
    this.isLoading = false;
    this.isFirst = 0;
    let data = this.filterData;
    data['page'] = this.pageNo;
    this.service.getAccountStatment(data).subscribe((res) => {
      if (res.status === 1 && res.data !== null) {
        const newPost = res.data;
        this.spinner.hide();
        if (newPost.length === 0) {
          this.notEmptyPost = false;
        }
        for (const item of res.data) {
          const cData = new AccountStatementObj(item);
          cData.createdOn = item.created_on;
          this.acList.push(cData);
        }
        // add newly fetched posts to the existing post
        // this.acList = this.acList.concat(newPost);
        this.notscrolly = true;
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
    });
  }

  excelDownload() {
    const tmpData = [];
    const tmp = {
      VOUCER_ID: '',
      SETTLED_DATE: '',
      NARRATION: '',
      DEBIT: '',
      CREADIT: '',
      RUNNING_BALANCE: ''
    };
    tmpData.push(tmp);
    this.acList.forEach((item: AccountStatementList) => {
      const credit = item.type === 'CREDIT' ? item.amount : '0.00';
      const debit = item.type === 'DEBIT' ? item.amount : '0.00';
      let desc = item.description;
      if (item.status !== 1) {
        desc += '  ( Canceled )';
      }
      const tmp = {
        VOUCER_ID: item.id,
        SETTLED_DATE: item.createdOn,
        NARRATION: desc,
        DEBIT: Number(debit),
        CREADIT: Number(credit),
        RUNNING_BALANCE: item.balance
      };
      tmpData.push(tmp);
    });
    this.excel.exportAsExcelFile(tmpData, 'accountstatment');
  }


}

/** ACCOUNT-STATEMENT-DETAILS***************************************************************************/

@Component({
  selector: 'app-account-statement-detail',
  templateUrl: './account-statement-detail.component.html',
  // styleUrls: ['./account-statement.component.css']
})
export class AccountStatementDetailComponent extends BaseComponent implements OnInit {

  @ViewChild('epltable') epltable: ElementRef;
  @ViewChild('bBody') bBody: ElementRef;
  public page: number = 2;
  public acmId: string;
  public type: number;
  public roundId: number;
  public acList: AccountStatementBetList[];
  public title: string;
  public isLoading: boolean;
  public primaryColor: string;
  public loading = false;
  private filterData: any;

  constructor(inj: Injector, private service: ACAPIService) {
    super(inj);
    this.acmId = this.activatedRoute.snapshot.params.id;
    this.type = this.activatedRoute.snapshot.queryParams.type ? Number(this.activatedRoute.snapshot.queryParams.type) : 0;
    this.roundId = this.activatedRoute.snapshot.queryParams.round ? this.activatedRoute.snapshot.queryParams.round : 0;
    this.isLoading = true;
  }

  ngOnInit(): void {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.title = 'Account Statement';
    this.filterData = {
      market_id: this.acmId,
      type: (this.type === 99 ? 'livegame1' : (this.type === 999 ? 'livegame2' : (this.type === 9999 ? 'casino' : ''))) || ''
    };
    this.getAccountStatementDetail();
  }

  getAccountStatementDetail() {
    this.spinner.show();
    this.service.getAccountStatmentBet(this.filterData).subscribe((res) => {
      this.acList = [];
      if (res.status === 1 && res.data !== null) {
        for (const item of res.data) {
          const cData = new AccountStatementBetObj(item);
          cData.betId = item.betId.$oid;
          this.acList.push(cData);
        }
      } else {
        this.acList = null;
      }
      this.spinner.hide();
      this.changedRef.detectChanges();
    }, (error) => {
      this.spinner.hide();
    });
  }


}
