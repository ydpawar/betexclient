import {Component, OnInit, Injector, ElementRef, ViewChild, Output, EventEmitter, Input, AfterViewInit} from '@angular/core';
import {DatePipe} from '@angular/common';
import * as moment from 'moment'; // Momentjs
import {ACAPIService} from 'src/app/core/services';
import {BaseComponent} from '../../../share/components/common.component';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent extends BaseComponent implements OnInit, AfterViewInit {

  public rList: any = [];
  public filter: any = {};
  @ViewChild('startdate') startdate: ElementRef;
  @ViewChild('enddate') enddate: ElementRef;
  @Input() title: string;

  public filterBtn: string = 'today';
  public class: string = 'col-6';

  constructor(inj: Injector, private service: ACAPIService) {
    super(inj);
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('right-bar-enabled');
    const now = moment();
    const today = now.format('YYYY-MM-DD');
    setTimeout(() => {
      this.startdate.nativeElement.value = today;
      this.enddate.nativeElement.value = today;
    }, 500);
    const data = {start_date: today, end_date: today, type: '', isFirst: 1};
    this.filter = data;
  }

  ngOnInit() {
    this.getResults();
  }

  ngAfterViewInit() {

  }

  onChangeDate($event) {
    if ($event.target.name === 'From') {
      this.filter.start_date = $event.target.value;
    } else {
      this.filter.end_date = $event.target.value;
    }
    this.getResults();
  }

  onChangeType($event) {
    this.filter.type = $event.target.options[$event.target.options.selectedIndex].value;
    this.getResults();
  }

  getResults() {
    this.service.getresults(this.filter).subscribe((res) => this.initResults(res));
  }

  initResults(res) {
    this.rList = [];
    if (res.status === 1 && res.data !== null) {
      this.filter.isFirst = 0;
      this.rList = res.data;
    } else {
      this.filter.isFirst = 0;
    }
    this.changedRef.detectChanges();
  }

  /**
   * FILTER ON SINGLE CLICK
   * @param search TYPE LIKE TODAY|LAST 7 DAYS|30 DAYS
   */
  filters($event: any) {
    this.filterBtn = $event.target.options[$event.target.options.selectedIndex].value;
    switch (this.filterBtn) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY/MM/DD');
        this.filter.end_date = moment().format('YYYY/MM/DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY/MM/DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY/MM/DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;

      case '7_day' :
        this.filter.end_date = moment().format('YYYY/MM/DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY/MM/DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;

      case '30_day' :
        this.filter.end_date = moment().format('YYYY/MM/DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY/MM/DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;

      case '3_month' :
        this.filter.end_date = moment().format('YYYY/MM/DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY/MM/DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    //  this.filterChanged.emit(this.filter);
    this.getResults();
  }

  setInputDate(sDate: Date, eDate: Date) {
    const dateSendingToServer = new DatePipe('en-US').transform(sDate, 'mm/dd/yyyy');
    console.log(dateSendingToServer);

    setTimeout(() => {
      this.startdate.nativeElement.value = moment(sDate).format('YYYY-MM-DD');
      this.enddate.nativeElement.value = moment(eDate).format('YYYY-MM-DD');
    }, 500);

  }

  generateTitle(slug: string) {
    const words = slug.split(' ');
    for (let i = 0; i < words.length; i++) {
      const word = words[i];
      words[i] = word.charAt(0).toUpperCase() + word.slice(1);
    }
    return words.join('_');
  }
}
