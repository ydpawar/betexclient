import {Component, OnInit, OnDestroy, ElementRef, ViewChild, Renderer2, Injector, ChangeDetectorRef} from '@angular/core';
import {BaseComponent} from '../../share/components/common.component';
import {
  ModalService,
  SharedataService,
  ConstantsService,
  APIService
} from '../../core/services/index';
import {SportList, SportListObj} from '../../share/models/dashboard';

declare var $;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('indicaters') indicaters: ElementRef;
  @ViewChild('carouselInner') carouselInner: ElementRef;
  public name: string;
  public sList: SportList[] = [];
  public isModalOpen: boolean = false;
  public orderOption: string;
  public orderDetail: any;
  private isDestroy: boolean;
  private isLoadFirst: boolean;
  private isOrderIndex: number;

  constructor(inj: Injector, private modalService: ModalService,
              private CONSTANT: ConstantsService,
              private elRef: ElementRef,
              private renderer: Renderer2,
              private serice: APIService,
              private cdref: ChangeDetectorRef,
              private shareData: SharedataService) {
    super(inj);
    this.isLoadFirst = false;
    this.isDestroy = false;

    this.bindSliderHTML();

  }

  ngOnInit() {
    this.activatedRoute.params.forEach(params => {
      this.name = params['slug'] !== undefined ? params['slug'] : 'all';
    });
    if (this.getToken('sportlist')) {
      this.sList = JSON.parse(this.getToken('sportlist'));
      this.getDetails();
    } else {
      this.getDetails();
    }
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
  }

  getDetails() {
    this.serice.getDashboard().subscribe((res) => {
      if (res.status === 1) {
        this.sList = [];
        for (const data of res.data) {
          const cData = new SportListObj(data);
          this.sList.push(cData);
        }
        this.cdref.detectChanges();
        this.setToken('sportlist', JSON.stringify(this.sList));
        this.setToken('dash-slider', JSON.stringify(res.slider));
      }
    });
  }

  bindSliderHTML() {
    const sliderImages = JSON.parse(sessionStorage.getItem('dash-slider'));
    if (sliderImages !== null) {
      let indicaterHTML = '';
      let carouselBodyHTML = '';
      let i = 0;
      const images = [];
      for (const item of sliderImages) {
        images[i] = new Image();
        images[i].src = item.image; // + '?v=' + this.timestamp
        let className = '';
        if (i === 0) {
          className = 'active';
        }
        indicaterHTML += '<li data-target="#DashSlider" data-slide-to="' + i + '" class="' + className + '"></li>';
        carouselBodyHTML += '<div class="carousel-item ' + className + '">';
        carouselBodyHTML += '<img class="d-block img-fluid" loading="lazy" rel="preload" src="' + item.image + '" alt="">';
        carouselBodyHTML += '</div>';
        i++;
      }

      if (this.indicaters && this.carouselInner) {
        this.indicaters.nativeElement.innerHTML = indicaterHTML;
        this.carouselInner.nativeElement.innerHTML = carouselBodyHTML;
        $('#DashSlider').carousel({
          interval: 3000,
          cycle: true
        });
      } else {
        const xhm = setTimeout(() => {
          clearTimeout(xhm);
          this.bindSliderHTML();
        }, 100);
      }
    } else {
      const xhm = setTimeout(() => {
        clearTimeout(xhm);
        this.bindSliderHTML();
      }, 100);
    }
  }

}
