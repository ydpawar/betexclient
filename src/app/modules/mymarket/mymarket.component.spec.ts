import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MymarketComponent } from './mymarket.component';

describe('MymarketComponent', () => {
  let component: MymarketComponent;
  let fixture: ComponentFixture<MymarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MymarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MymarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
