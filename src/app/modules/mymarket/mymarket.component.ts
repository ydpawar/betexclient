import {Component, OnInit, Injector, ElementRef, TemplateRef, ViewChild, AfterViewInit, OnDestroy, Renderer2} from '@angular/core';
import {BaseComponent} from '../../share/components/common.component';
import {BetplaceComponent} from '../../share/components/bottom-sheet/betplace.component';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {AlertService, APIService, SharedataService} from '../../core/services';

declare let $;

@Component({
  selector: 'app-mymarket',
  templateUrl: './mymarket.component.html',
  styleUrls: ['./mymarket.component.css']
})
export class MymarketComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('myMarket', {static: false}) myMarket: ElementRef;
  @ViewChild('pRef', {static: false}) pRef: ElementRef;
  @ViewChild('bookmakerRef', {static: false}) bookmakerRef: ElementRef;
  @ViewChild('fancyRef', {static: false}) fancyRef: ElementRef;
  @ViewChild('fancy2Ref', {static: false}) fancy2Ref: ElementRef;
  @ViewChild('fancy3Ref', {static: false}) fancy3Ref: ElementRef;
  @ViewChild('khadoRef', {static: false}) khadoRef: ElementRef;
  @ViewChild('ballbyballRef', {static: false}) ballbyballRef: ElementRef;
  @ViewChild('profitLossModal', {static: false}) profitLossModal: ElementRef;

  public oldGdata: any = {};
  public oldGdataCnt: number = 0;

  // **VARIABLE-DECLARATION************************************
  // **DATA-STORAGE************************************
  public matchData: any;
  public oddsData: any;
  public marketArr: any;
  public fancy2PL: any = [];
  public fancyBets: any = [];

  // **PROFIT-LOSS BOOK-GLOBLE************************************
  public GAME_BOOKDATA: any = [];

  // **SINGLE-LINE-DATA************************************
  public eventId: string;
  public eventTitle: string;
  public marketId: string;
  public eventMsg: string;
  public updatedTime: number;
  public annimationClass: string;
  public assetsUrl: string;

  // **FLAGE************************************
  public isTabs: string;
  public isFirstLoad: boolean;
  public isDestroy: boolean;
  public isNoDataFound: boolean;
  public isLoadFancyPl: boolean;
  public isFancy3: boolean;
  public loaderShow: boolean;
  public isBetloaderShow: boolean;
  public isActivePlaceBet: boolean;

  // **INTERVAL******************************
  public interval1000: any;
  public interval300: any;
  public isActivePlaceBetIndex: string;
  public isActivePlaceBetSType: string;
  public isActivePlaceBetMarketId: string;
  private onUpdate: number;
  public primaryColor: string;

  constructor(inj: Injector,
              private commonService: SharedataService,
              private service: APIService,
              private alert: AlertService,
              private elRef: ElementRef,
              private renderer: Renderer2,
              private placeBetSheet: MatBottomSheet) {
    super(inj);
    this.commonService.setRouterChange('1'); // CALL TO HEADER API
    this.assetsUrl = '';
    this.valInit();
  }

  ngOnInit() {
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.spinner.show();
    this.getEventDetail();

    this.commonService.betStatus.subscribe((res) => { // Bet Flags Manage
      if (res === 1) {
        this.isActivePlaceBet = true;
      } else if (res === 2) {
        this.isBetloaderShow = true;
        this.isActivePlaceBet = false;
        this.spinner.show();
      } else if (res === 3) {
        this.isBetloaderShow = false;
        this.spinner.hide();
        this.getProfitLoss();
      }
    });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
    this.matchData = [];
    this.marketArr = [];
    this.oddsData = [];
    clearTimeout(this.interval1000);
    clearTimeout(this.interval300);
    this.commonService.eventId.emit('');
  }

  valInit() {
    this.isFirstLoad = false;
    this.isDestroy = false;
    this.isNoDataFound = false;
    this.annimationClass = 'fadeInUp';
    this.isActivePlaceBet = false;
    this.loaderShow = false;
    this.isBetloaderShow = false;
  }

  getEventDetail(): void {
    this.service.getMyMarketDetail().subscribe((res) => {
        if (res.status === 1 && res.data.items.length) {
          this.isNoDataFound = false;
          this.matchData = res.data.items;
          this.marketArr = res.data.marketIdsArr; // SET MARKET ARRAY
          this.eventTitle = res.data.items.title; // SET MARKET ARRAY

          if (this.marketArr.length === 0) { // INIT MARKET ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
          } else if (this.marketArr.length !== res.data.marketIdsArr.length) { // CHECK DIFF. OF CURRANT AND OLD ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
            clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          } else if (this.marketArr.length === res.data.marketIdsArr.length && res.data.items.ballbyball) {
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = true;
            // clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          }

          if (!this.isFirstLoad && this.marketArr.length) { // LOAD FIRST TIME ODDS FUNCTIONS
            this.isFirstLoad = true;
            this.getProfitLoss();
            this.getOdds();
          } else {
            if (!this.loaderShow) {
              this.loaderShow = true;
              this.spinner.hide();
            }
          }

          if (this.onUpdate === undefined && res.updatedOn !== null) {  // set gameover time
            this.onUpdate = res.updatedOn;
          } else if (this.onUpdate !== null && this.onUpdate !== res.updatedOn) { // CHECK IF GAMEOVER TIME CHANGE OR NOT
            this.onUpdate = res.updatedOn;
            this.commonService.setRouterChange('3');
            this.getProfitLoss();
          }
          this.clearSesssionBind();
        } else {  // AFTER GAME OVER  TO RECALL OPEN EVENT
          this.isNoDataFound = true;
          this.spinner.hide();
          this.clearSesssionBind();
          this.eventMsg = res.msg;
          this.eventTitle = '';
          this.matchData = [];
          this.marketArr = [];
          // if (!this.isDestroy && this.eventMsg !== '') {
          //   this.interval1000 = setTimeout(() => {
          //     this.getEventDetail();
          //   }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
          // }
        }
        if (!this.isDestroy) { // API CALL TO GET REAL TIME UPDATES
          this.interval1000 = setTimeout(() => {
            this.getEventDetail();
          }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
        }
      }, (error) => {
        if (!this.loaderShow) {
          this.loaderShow = true;
          this.spinner.hide();
        }
        this.clearSesssionBind();
        console.log('Error-detail', error);
      });
  }

  getOdds() {
    this.service.getOddsData(this.marketArr).subscribe((resData) => {
          this.oddsData = resData.data.items;
          // this.matchData.match_odd.marketId
          let index = 0;
          this.myMarket.nativeElement.innerHTML = '';
          let innerHTMLVal = '';
          this.matchData.forEach((oddItems) => {
            innerHTMLVal += this.bindEventHeader(oddItems.title, oddItems.slug);

            if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
              // this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
              if (this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex].market_id == this.isActivePlaceBetMarketId) {
                this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
              } else {
                for (let ii = 0; ii < this.oddsData[this.isActivePlaceBetSType].length; ii++) { // CHECK MARKET ID
                  if (this.oddsData[this.isActivePlaceBetSType][ii].market_id == this.isActivePlaceBetMarketId) {
                    this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][ii]);
                    break;  // <=== breaks out of the loop early
                  }
                }
              }
            }

            if (resData.data.items.match_odd.length && this.matchData[index].matchodd) {
              if (this.oldGdataCnt === 0) {
                this.oldGdata = resData.data.items.match_odd;
                this.oldGdataCnt = 1;
              }
              innerHTMLVal += this.bindMatchData(resData.data.items.match_odd, this.matchData[index].matchodd[0], index, oddItems.title);
            }

            if (resData.data.items.bookmaker && resData.data.items.bookmaker.length && this.matchData[index].bookmaker) {
              innerHTMLVal += this.bindBookmakerData(resData.data.items.bookmaker, this.matchData[index].bookmaker, index, oddItems.title);
            }

            if (resData.data.items.fancy2 && resData.data.items.fancy2.length) {
              innerHTMLVal += this.bindFancy2Data(resData.data.items.fancy2, this.matchData[index].fancy2, oddItems.title);
            }

            if (resData.data.items.fancy && resData.data.items.fancy.length) {
              innerHTMLVal += this.bindFancyData(resData.data.items.fancy, this.matchData[index].fancy, oddItems.title);
            }

            if (resData.data.items.ballbyball && resData.data.items.ballbyball.length) {
              innerHTMLVal += this.bindBallByBallData(resData.data.items.ballbyball, this.matchData[index].ballbyball, oddItems.title);
            }

            if (resData.data.items.fancy3 && resData.data.items.fancy3.length) {
              innerHTMLVal += this.bindFancy3Data(resData.data.items.fancy3, this.matchData[index].fancy3, oddItems.title);
            }

            if (resData.data.items.khado && resData.data.items.khado.length) {
              innerHTMLVal += this.bindKhadoData(resData.data.items.khado, this.matchData[index].khado, oddItems.title);
            }

            innerHTMLVal += this.bindCloseEvent();
            index++;
          });
          this.myMarket.nativeElement.innerHTML = innerHTMLVal;
          const tThis = this;
          const clickButtons = this.elRef.nativeElement.querySelectorAll('.bet-sec');
          const clickButtonsModelPl = this.elRef.nativeElement.querySelectorAll('.bookModalDetail');
          for (let i = 0; i < clickButtons.length; i++) {
            this.renderer.listen(clickButtons[i], 'click', ($event) => {
              tThis._placebet(clickButtons[i].dataset);
            });
          }

          for (let i = 0; i < clickButtonsModelPl.length; i++) {
            this.renderer.listen(clickButtonsModelPl[i], 'click', ($event) => {
              tThis.getFancyTwoPL(clickButtonsModelPl[i].dataset);
            });
          }

          this.annimationClass = '';
          if (!this.isDestroy) { // && this.isScreenActive
            this.interval300 = setTimeout(() => {
              clearTimeout(this.interval300);
              this.getOdds();
            }, this.getToken('oddsTiming') !== null ? parseInt(this.getToken('oddsTiming')) : 400); //
          }
          if (!this.loaderShow) {
            this.loaderShow = true;
            this.spinner.hide();
          }
        },
        (error) => {
          if (!this.loaderShow) {
            this.loaderShow = true;
            this.spinner.hide();
          }
          console.log('error-Odds', error);
        });
  }

  oddsChange() {
    let tmpTimeOut = setTimeout(() => {
      $.ajax({
        url: 'http://34.244.130.223/php_session/public/api/demo-odds?event_id=1581658920',
        type: 'get',
        success: data => {
          // console.log(data);
        }, error: errro => {
          console.log(errro);
        }, complete: res => {
          this.oddsChange();
        }
      });
      clearTimeout(tmpTimeOut);
    }, 5000);
  }

  clearSesssionBind() {
    if (this.matchData && this.matchData.match_odd === null) {
      this.pRef.nativeElement.innerHTML = '';
    }
    if (this.matchData && this.matchData.bookmaker === null) {
      this.bookmakerRef.nativeElement.innerHTML = '';
    }
    if (this.matchData && this.matchData.fancy === null) {
      this.fancyRef.nativeElement.innerHTML = '';
    }
    if (this.matchData && this.matchData.fancy2 === null) {
      this.fancy2Ref.nativeElement.innerHTML = '';
    }
    if (this.matchData && this.matchData.fancy3 === null) {
      this.fancy3Ref.nativeElement.innerHTML = '';
    }
    if (this.matchData && this.matchData.khado === null) {
      this.khadoRef.nativeElement.innerHTML = '';
    }
    if (this.matchData && this.matchData.ballbyball === null) {
      this.ballbyballRef.nativeElement.innerHTML = '';
    }
  }

  bindEventHeader(title, slug) {
    let html = '<div class="col-12">';
    html += '<div class="row mt-0 mt-lg-3"><div class="col-12"><h2 class="event-title text-left pl-3 text-uppercase"><i class="mdi mdi-cricket mr-1"></i> ' + slug + '</h2></div></div>';
    html += '<div class="card"><div class="card-body market-ui">\n' +
      '<h2 class="event-title border-light border-bottom m-0">' + title + '</h2>\n' +
      '<div class="row mx-lg-0"><div class="col-md-12">';
    return html;
  }

  bindCloseEvent() {
    const html = '</div></div></div></div></div>';
    return html;
  }

  bindMatchData(dataOdd, matchData, eIndex, eventTitle) {
    try {
      // debugger;
      let html = '';
      html += '<div class="market-section mt-1"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
        '<div class="col-md-5 col-7"><p class="market-title">Match odds</p></div>' +
        '<div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < matchData.runners.length; i++) {

        let data: any = {};
        if (dataOdd[eIndex] && dataOdd[eIndex].market_id === matchData.marketId) { // CHECK MARKET ID
          data = dataOdd[eIndex];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === matchData.marketId) {
              data = dataOdd[ii];
              eIndex = ii;
              break;       // <=== breaks out of the loop early
            }
          }
        }
        if (dataOdd[eIndex].odds && dataOdd[eIndex].odds[i].selectionId === undefined) {
          return false;
        }

        const id = data.odds[i].selectionId.toString();
        let backblink = '';
        let layblink = '';
        if (data.odds[i].backPrice1 !== '0.00') {
          if (this.oldGdata[eIndex].odds[i].backPrice1 !== data.odds[i].backPrice1) {
            backblink = 'blink-back';
          }
        }
        if (data.odds[i].layPrice1 !== '0.0') {
          if (this.oldGdata[eIndex].odds[i].layPrice1 !== data.odds[i].layPrice1) {
            layblink = 'blink-lay';
          }
        }

        this.oldGdata[eIndex].odds[i].backPrice1 = data.odds[i].backPrice1;
        this.oldGdata[eIndex].odds[i].layPrice1 = data.odds[i].layPrice1;

        let suspended = '';
        let suspendedTitle = 'SUSPENDED';
        if (matchData.runners[i].suspended === 1 || data.status === 'CLOSED' || data.status === 'SUSPENDED') {
          suspended = 'suspended';
          suspendedTitle = data.status;
        }

        let borderClass = '';
        if (i <= 1) {
          borderClass = 'border-bottom';
        }

        let pdata = 0;
        if (this.GAME_BOOKDATA[matchData.marketId + data.odds[i].selectionId]) {
          pdata = this.GAME_BOOKDATA[matchData.marketId + data.odds[i].selectionId];
        }

        let bcolor = '';
        if (pdata <= -1) {
          bcolor = 'red';
        } else if (pdata >= 1) {
          bcolor = 'green';
        } else {
          bcolor = 'black';
        }

        html += '<div data-title="' + suspendedTitle + '" id="match-odds' + matchData.marketId + '" data-sessiontype="match_odd" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
        html += '<div class="col-md-5 col-7"><p class="market-name">' + matchData.runners[i].runnerName + '<span class="odd-number ' + bcolor + '">' + pdata + '</span></p></div>';
        html += '<div class="col-md-7 col-5"><div class="bl-buttons">';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice3 === '-' ? '0' : data.odds[i].backPrice3) + ' <span>' + (data.odds[i].backPrice3 === '-' ? '00' : data.odds[i].backSize3) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice2 === '-' ? '0' : data.odds[i].backPrice2) + ' <span>' + (data.odds[i].backPrice2 === '-' ? '00' : data.odds[i].backSize2) + '</span></button>';
        html += '<button class="bl-btn back waves-effect waves-light bet-sec ' + backblink + '" data-index="0" data-eindex="' + eIndex + '" data-eventid="' + matchData.eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].backPrice1 + '" data-btype="back" data-mtype="match_odd" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].backPrice1 === '-' ? '0' : data.odds[i].backPrice1) + ' <span>' + (data.odds[i].backPrice1 === '-' ? '00' : data.odds[i].backSize1) + '</span></button>';
        html += '<button class="bl-btn lay waves-effect waves-light bet-sec ' + layblink + '"   data-index="0" data-eindex="' + eIndex + '"  data-eventid="' + matchData.eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].layPrice1 + '" data-btype="lay" data-mtype="match_odd" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].layPrice1 === '-' ? '0' : data.odds[i].layPrice1) + ' <span>' + (data.odds[i].layPrice1 === '-' ? '00' : data.odds[i].laySize1) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice2 === '-' ? '0' : data.odds[i].layPrice2) + ' <span>' + (data.odds[i].layPrice2 === '-' ? '00' : data.odds[i].laySize2) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice3 === '-' ? '0' : data.odds[i].layPrice3) + ' <span>' + (data.odds[i].layPrice3 === '-' ? '00' : data.odds[i].laySize3) + '</span></button>';
        if (suspended !== '') {
          html += '<div class="suspended"><span>' + suspended + '</span></div>';
        }
        html += '</div></div></div>';
      }

      if (matchData.info && matchData.info !== null && matchData.info !== 'null' && matchData.info !== '') {
        html += '<p class="alert-text">' + matchData.info + '</p>';
      }

      html += '</div>';
      return html;
    } catch (e) {
      console.log('[MyMarket]-Failed to bind Match-odd', e);
    }
  }

  bindBookmakerData(dataOdd, bookmakerData, eIndex, eventTitle) {
    try {
      // fadeInUp
      let bookmakerHtmlData = '';
      for (let i = 0; i < bookmakerData.length; i++) {

        bookmakerHtmlData += '<div class="market-section mt-1"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
          '<div class="col-md-5 col-7"><p class="market-title">' + bookmakerData[i].marketName + '</p></div>' +
          '<div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

        for (let j = 0; j < bookmakerData[i].runners.length; j++) {

          let data: any = {};
          if (dataOdd[i].market_id === bookmakerData[i].market_id) { // CHECK MARKET ID
            data = dataOdd[i];
          } else {
            for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
              if (dataOdd[ii].market_id === bookmakerData[i].market_id) {
                data = dataOdd[ii];
                break;       // <=== breaks out of the loop early
              }
            }
          }

          let suspended = '';
          if (data && data.runners[j] && (data.runners[j].suspended !== 0 || data.runners[j].ballrunning !== 0)) {
            suspended = data.runners[j].suspended === 1 ? 'Suspended' : 'Ball Running';
          }
          let bdata = 0;
          let bcolor = '';
          if (this.GAME_BOOKDATA[bookmakerData[i].market_id + bookmakerData[i].runners[j].sec_id] !== undefined) {
            bdata = this.GAME_BOOKDATA[bookmakerData[i].market_id + bookmakerData[i].runners[j].sec_id];
          }

          if (bdata <= -1) {
            bcolor = 'red';
          } else if (bdata >= 1) {
            bcolor = 'green';
          } else {
            bcolor = 'black';
          }

          let borderClass = '';
          if (i <= 1) {
            borderClass = 'border-bottom';
          }


          bookmakerHtmlData += '<div data-title="' + suspended + '"  id="' + bookmakerData[i].runners[j].sec_id + '" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
          bookmakerHtmlData += '<div class="col-md-5 col-7"><p class="market-name">' + bookmakerData[i].runners[j].runner + '<span class="odd-number ' + bcolor + '">' + bdata + '</span></p></div>';
          bookmakerHtmlData += '<div class="col-md-7 text-center col-5"> <div class="bl-buttons">';
          bookmakerHtmlData += '<button class="bl-btn back waves-effect waves-light bet-sec"  data-index="' + i + '"  data-eindex="' + eIndex + '" data-eventid="' + bookmakerData[i].event_id + '" data-eventtitle="' + eventTitle + '"  data-marketname="' + bookmakerData[i].marketName + '" data-sec="' + bookmakerData[i].runners[j].sec_id + '" data-btype="back" data-rate="' + data.runners[j].back + '" data-runnerName="' + bookmakerData[i].runners[j].runner + '" data-mtype="' + bookmakerData[i].runners[j].mType + '"  data-marketId="' + bookmakerData[i].market_id + '" data-slug="' + bookmakerData[i].slug + '" data-sportId="' + bookmakerData[i].sportId + '" data-minstack="' + bookmakerData[i].runners[j].minStack + '" data-maxstack="' + bookmakerData[i].runners[j].maxStack + '" data-maxprofitlimit="' + bookmakerData[i].runners[j].maxProfitLimit + '">' + (data.runners[j].back === undefined ? '0' : data.runners[j].back) + ' <span>00</span></button>';
          bookmakerHtmlData += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '"  data-eindex="' + eIndex + '"  data-eventid="' + bookmakerData[i].event_id + '" data-eventtitle="' + eventTitle + '" data-marketname="' + bookmakerData[i].marketName + '" data-sec="' + bookmakerData[i].runners[j].sec_id + '" data-btype="lay" data-rate="' + data.runners[j].lay + '" data-runnerName="' + bookmakerData[i].runners[j].runner + '" data-mtype="' + bookmakerData[i].runners[j].mType + '"  data-marketId="' + bookmakerData[i].market_id + '" data-slug="' + bookmakerData[i].slug + '" data-sportId="' + bookmakerData[i].sportId + '" data-minstack="' + bookmakerData[i].runners[j].minStack + '" data-maxstack="' + bookmakerData[i].runners[j].maxStack + '" data-maxprofitlimit="' + bookmakerData[i].runners[j].maxProfitLimit + '">' + (data.runners[j].lay === undefined ? '0' : data.runners[j].lay) + ' <span>00</span></button>';
          if (suspended !== '') {
            bookmakerHtmlData += '<div class="suspended"> <span>' + suspended + '</span></div> \n';
          }
          bookmakerHtmlData += '</div></div></div>';
        }
        if (bookmakerData[i].info && bookmakerData[i].info !== null) {
          bookmakerHtmlData += '<p class="alert-text">' + bookmakerData[i].info + '</p>';
        }
        bookmakerHtmlData += '</div>';
      }
      return bookmakerHtmlData;
    } catch (e) {
      console.log('[MyMarket]-Failed to Bind Bookmaker', e);
    }
  }

  bindFancyData(dataOdd, fancy, eventTitle) {
    try {
      if (fancy === undefined || fancy === null) {
        return '';
      }
      if (dataOdd === undefined || dataOdd.length === 0) {
        return '';
      }

      let fancyDataHtml = '';
      fancyDataHtml = '<div class="market-section mt-1">\n' +
        '<div class="row mx-lg-0 align-items-center text-uppercase text-dark market-title-row">\n' +
        '<div class="col-md-7 col-7"><p class="market-title">FANCY</p></div>\n' +
        '<div class="col-md-5 text-center col-5">\n' +
        '<p class="market-back"><span>no</span> <span>yes</span></p>\n' +
        '</div></div>';

      for (let i = 0; i < fancy.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy[i].marketId) {
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancyDataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">\n' +
          '<div class="col-md-6 col-5"><p class="market-name">' + fancy[i].title + '</p></div>';
        fancyDataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail"  ' + bookActiveStatus + ' data-title="' + fancy[i].title + '"  data-marketid="' + data.market_id + '" data-mtype="' + fancy[i].mType + '">book</button></div>';
        fancyDataHtml += '<div class="col-md-5 text-center col-5"><div class="bl-buttons">';
        fancyDataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec"  data-index="' + i + '" data-marketname="' + fancy[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy[i].title + '" data-rate="' + data.no + '" data-mtype="' + fancy[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + fancy[i].slug + '"  data-sportId="' + fancy[i].sportId + '"  data-minstack="' + fancy[i].minStack + '"   data-maxstack="' + fancy[i].maxStack + '"   data-maxprofitlimit="' + fancy[i].maxProfitLimit + '">' + data.no + ' <span>' + data.no_rate + '</span></button>';
        fancyDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy[i].title + '" data-rate="' + data.yes + '" data-mtype="' + fancy[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + fancy[i].slug + '"  data-sportId="' + fancy[i].sportId + '"  data-minstack="' + fancy[i].minStack + '"   data-maxstack="' + fancy[i].maxStack + '"   data-maxprofitlimit="' + fancy[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancyDataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</span></div>';
        }
        fancyDataHtml += '</div></div></div>';
        if (fancy[i].info) {
          fancyDataHtml += '<p class="alert-text">' + fancy[i].info + '</p>';
        }
      }
      fancyDataHtml += '</div>';
      return fancyDataHtml;
    } catch (e) {
      console.log('[MyMarket]-Failed to bind Fancy', e);
    }
  }

  bindFancy2Data(dataOdd, fancy2, eventTitle) {
    try {
      if (fancy2 === null) {
        return '';
      }
      if (dataOdd === undefined || dataOdd.length === 0) {
        return '';
      }
      let fancy2DataHtml = '';
      fancy2DataHtml = '<div class="market-section mt-1">\n' +
        '<div class="row align-items-center text-uppercase text-dark market-title-row">\n' +
        '<div class="col-md-5 col-7"><p class="market-title">Session</p></div>\n' +
        '<div class="col-md-7 text-center col-5">\n' +
        '<p class="market-back"><span>no</span> <span>yes</span></p>\n' +
        '</div></div>';

      for (let i = 0; i < fancy2.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy2[i].marketId) {
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy2[i].marketId) {
              data = dataOdd[ii];
              break;       // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy2[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy2DataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">\n' +
          '<div class="col-md-4 col-5"><p class="market-name">' + fancy2[i].title + '</p></div>';
        fancy2DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail"  ' + bookActiveStatus + ' data-eventid="' + fancy2[i].eventId + '" data-title="' + fancy2[i].title + '"  data-marketid="' + data.market_id + '" data-mtype="' + fancy2[i].mType + '">book</button></div>';
        fancy2DataHtml += '<div class="col-md-7 text-center col-5"><div class="bl-buttons">';
        fancy2DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '"  data-eventid="' + fancy2[i].eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.no + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.no + ' <span>' + data.no_rate + '</span></button>';
        fancy2DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '"  data-eventid="' + fancy2[i].eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.yes + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy2DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</span></div>';
        }
        fancy2DataHtml += '</div></div></div>';
        if (fancy2[i].info) {
          fancy2DataHtml += '<p class="alert-text">' + fancy2[i].info + '</p>';
        }
      }
      fancy2DataHtml += '</div>';
      return fancy2DataHtml;
    } catch (e) {
      console.log('[MyMarket]-Failed to bind Fancy2', e);
    }
  }

  bindFancy3Data(dataOdd, fancy3, eventTitle) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0 || fancy3 === null) {
        return '';
      }

      let fancy3DataHtml = '';
      fancy3DataHtml += '<div class="market-section mt-1"><div class="row align-items-center text-uppercase text-dark market-title-row">';
      fancy3DataHtml += '<div class="col-md-5 col-7"><p class="market-title">other market</p></div>';
      fancy3DataHtml += '<div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < fancy3.length; i++) {
        let data: any = {};
        for (let ii = 0; ii < dataOdd.length; ii++) {
          if (dataOdd[ii].market_id === fancy3[i].marketId) {
            data = dataOdd[ii];
            break;       // <=== breaks out of the loop early
          }
        }

        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy3[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy3DataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        fancy3DataHtml += '<p class="market-name">' + fancy3[i].title + '</p></div>';
        fancy3DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + '  data-eventid="' + fancy3[i].eventId + '" data-title="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-mtype="' + fancy3[i].mType + '">book</button></div>';
        fancy3DataHtml += '<div class="col-md-7 col-5"><div class="bl-buttons">';
        fancy3DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-eventid="' + fancy3[i].eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="back" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.back + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.back + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.back + '<span> 00</span></button>';
        fancy3DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-eventid="' + fancy3[i].eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="lay" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.lay + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.lay + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.lay + ' <span>00</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy3DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        fancy3DataHtml += '</div></div>';
        if (fancy3[i].info) {
          fancy3DataHtml += '<div class="col-12"><p class="alert-text">' + fancy3[i].info + '</p></div>';
        }
        fancy3DataHtml += '</div>';
      }
      fancy3DataHtml += '</div>';
      return fancy3DataHtml;
    } catch (e) {
      console.log('[MyMarket]-Failed to bind Fancy3', e);
    }
  }

  bindKhadoData(dataOdd, khado, eventTitle) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0 || khado === null) {
        return '';
      }
      let khadoDataHtml = '';
      khadoDataHtml = '<div class="market-section mt-1"><div class="row align-items-center text-uppercase text-dark market-title-row">';
      khadoDataHtml += '<div class="col-md-5 col-7"><p class="market-title">Khado market</p></div>';
      khadoDataHtml += ' <div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < khado.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === khado[i].marketId) {
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === khado[i].marketId) {
              data = dataOdd[ii];
              break;       // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (khado[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        khadoDataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        khadoDataHtml += '<p class="market-name">' + khado[i].title + '<span class="khado-number">' + data.difference + '</span></p></div>';
        khadoDataHtml += '<div class="col-md-1 col-1 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-eventid="' + khado[i].eventId + '" data-title="' + khado[i].title + '" data-marketid="' + khado[i].marketId + '" data-mtype="' + khado[i].mType + '">book</button></div>';
        khadoDataHtml += '<div class="col-md-7 col-5"><div class="bl-buttons">';
        khadoDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-eventid="' + khado[i].eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="' + khado[i].title + '" data-marketid="' + khado[i].marketId + '" data-btype="yes" data-runnerName="' + khado[i].title + '" data-rate="' + data.yes + '" data-mtype="' + khado[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + khado[i].slug + '"  data-sportId="' + khado[i].sportId + '" data-minstack="' + khado[i].minStack + '"   data-maxstack="' + khado[i].maxStack + '"   data-maxprofitlimit="' + khado[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        khadoDataHtml += '<button class="bl-btn lay waves-effect waves-light" data-index="' + i + '">- <span>--</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          khadoDataHtml += '<div class="suspended"><span></span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        khadoDataHtml += '</div></div>';
        if (khado[i].info) {
          khadoDataHtml += '<div class="col-12"><p class="alert-text">' + khado[i].info + '</p></div>';
        }
        khadoDataHtml += '</div>';
      }
      khadoDataHtml += '</div>';
      return khadoDataHtml;
    } catch (e) {
      console.log('[MyMarket]-Failed to bind Khado', e);
    }
  }

  bindBallByBallData(dataOdd, ballbyball, eventTitle) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0 || ballbyball === null) {
        return '';
      }
      let bbbDataHtml = '';
      bbbDataHtml = '<div class="market-section mt-1"><div class="row align-items-center text-uppercase text-dark market-title-row">';
      bbbDataHtml += '<div class="col-md-5 border-0 col-5"><p class="market-title">Ball By Ball</p></div>';
      bbbDataHtml += ' <div class="col-md-7 text-center col-7"><p class="market-back"><span>no</span> <span>yes</span></p></div></div>';

      for (let i = 0; i < ballbyball.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === ballbyball[i].marketId) {
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === ballbyball[i].marketId) {
              data = dataOdd[ii];
              break;       // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (ballbyball[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        bbbDataHtml += '<div class="row mx-lg-0 my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
        bbbDataHtml += '<div class="col-md-4 col-4"><p class="market-name"><span class="mn-run">' + ballbyball[i].ball + '</span> ' + ballbyball[i].title + '</p></div>';
        bbbDataHtml += '<div class="col-md-1 col-1 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-eventid="' + ballbyball[i].eventId + '" data-title="' + ballbyball[i].title + '" data-marketid="' + ballbyball[i].marketId + '" data-mtype="' + ballbyball[i].mType + '">book</button></div>';
        bbbDataHtml += '<div class="col-md-7 col-7 text-center"><div class="bl-buttons">';
        bbbDataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-eventid="' + ballbyball[i].eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="' + ballbyball[i].ball + ' ' + ballbyball[i].title + '" data-marketid="' + ballbyball[i].marketId + '" data-btype="no" data-runnerName="' + ballbyball[i].title + '-' + ballbyball[i].ball + '" data-rate="' + data.no + '" data-mtype="' + ballbyball[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + ballbyball[i].slug + '"  data-sportId="' + ballbyball[i].sportId + '"  data-minstack="' + ballbyball[i].minStack + '"   data-maxstack="' + ballbyball[i].maxStack + '"   data-maxprofitlimit="' + ballbyball[i].maxProfitLimit + '">' + data.no + '<span> ' + data.no_rate + '</span></button>';
        bbbDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec"  data-index="' + i + '" data-eventid="' + ballbyball[i].eventId + '" data-eventtitle="' + eventTitle + '" data-marketname="' + ballbyball[i].ball + ' ' + ballbyball[i].title + '" data-marketid="' + ballbyball[i].marketId + '" data-btype="yes" data-runnerName="' + ballbyball[i].title + '-' + ballbyball[i].ball + '" data-rate="' + data.yes + '" data-mtype="' + ballbyball[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + ballbyball[i].slug + '"  data-sportId="' + ballbyball[i].sportId + '"  data-minstack="' + ballbyball[i].minStack + '"   data-maxstack="' + ballbyball[i].maxStack + '"   data-maxprofitlimit="' + ballbyball[i].maxProfitLimit + '">' + data.yes + ' <span>' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          bbbDataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        bbbDataHtml += '</div></div>';
        if (ballbyball[i].info && ballbyball[i].info !== 'null') {
          bbbDataHtml += '<div class="col-12"><p class="alert-text">' + ballbyball[i].info + '</p></div>';
        }
        bbbDataHtml += '</div>';
      }
      bbbDataHtml += '</div>';
      return bbbDataHtml;
    } catch (e) {
      console.log('[MyMarket]-Failed to bind BallByBall', e);
    }
  }

  _placebet(e) {
    this.isActivePlaceBet = true;
    this.isActivePlaceBetIndex = e.index;
    this.isActivePlaceBetSType = e.mtype;
    this.isActivePlaceBetMarketId = e.marketid;

    if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') { // BET SHEET
      if (this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex].market_id == e.marketid) {
        this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
      } else {
        for (let ii = 0; ii < this.oddsData[this.isActivePlaceBetSType].length; ii++) { // CHECK MARKET ID
          if (this.oddsData[this.isActivePlaceBetSType][ii].market_id == e.marketid) {
            this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][ii]);
            break;  // <=== breaks out of the loop early
          }
        }
      }
      // this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
    }

    let secId = e.sec;
    if (e.mtype === 'fancy') {
      secId = e.marketid;
    }

    const initBet = {
      price: e.rate,
      size: '',
      runner: e.runnername,
      bet_type: e.btype,
      sec_id: secId,
      market_id: e.marketid,
      event_id: e.eventid,
      m_type: e.mtype,
      eventname: e.eventtitle,
      market_name: e.marketname,
      slug: e.slug,
      rate: e.rrate ? e.rrate : e.rate,
      sport_id: e.sportid
    };

    const plArr: any = [];
    if (e.mtype === 'match_odd') {
      for (let ii = 0; ii < this.matchData[e.eindex]['matchodd'][e.index].runners.length; ii++) {
        const rData = {
          runner: this.matchData[e.eindex]['matchodd'][e.index].runners[ii].runnerName,
          secId: this.matchData[e.eindex]['matchodd'][e.index].runners[ii].selectionId,
          profit_loss: this.GAME_BOOKDATA[this.matchData[e.eindex]['matchodd'][e.index].marketId + this.matchData[e.eindex]['matchodd'][e.index].runners[ii].selectionId] ? this.GAME_BOOKDATA[this.matchData[e.eindex]['matchodd'][e.index].marketId + this.matchData[e.eindex]['matchodd'][e.index].runners[ii].selectionId] : 0,
        };
        plArr.push(rData);
      }
    }


    if (e.mtype === 'bookmaker') {
      // for (let i = 0; i < this.matchData[e.mtype].length; i++) {
      for (let ii = 0; ii < this.matchData[e.eindex][e.mtype][e.index].runners.length; ii++) {
        const rData = {
          runner: this.matchData[e.eindex][e.mtype][e.index].runners[ii].runner,
          secId: this.matchData[e.eindex][e.mtype][e.index].runners[ii].sec_id,
          profit_loss: this.GAME_BOOKDATA[this.matchData[e.eindex][e.mtype][e.index].market_id + this.matchData[e.eindex][e.mtype][e.index].runners[ii].sec_id] ? this.GAME_BOOKDATA[this.matchData[e.eindex][e.mtype][e.index].market_id + this.matchData[e.eindex][e.mtype][e.index].runners[ii].sec_id] : 0,
        };
        plArr.push(rData);
      }
      // }
    }
    // console.log(this.GAME_BOOKDATA);
    const plLimit = {minStack: e.minstack, maxStack: e.maxstack, maxProfitLimit: e.maxprofitlimit};
    this.placeBetSheet.open(BetplaceComponent, {
      disableClose: true,
      data: {int: initBet, market: plArr, limit: plLimit, index: e.index}
    });
  }

  getProfitLoss() {
    const data = {bookmaker: [], match_odd: []};
    this.marketArr.forEach((item) => {
      if (item.indexOf('-BM') !== -1) {
        data.bookmaker.push(item);
      } else if (item.indexOf('-F3') === -1 && item.indexOf('-F2') === -1 && item.indexOf('-F') === -1 && item.indexOf('-KD') === -1 && item.indexOf('-BB') === -1) {
        data.match_odd.push(item);
      }
    });

    this.service.getPLMatchoddANDBookmaker(data).subscribe((res) => {
        if (res.status === 1 && res.data !== null) {
          res.data.forEach((item) => {
            if (this.GAME_BOOKDATA[item.marketId + item.secId] !== undefined) {
              this.GAME_BOOKDATA[item.marketId + item.secId] = parseInt(item.profit_loss);
            } else {
              this.GAME_BOOKDATA[item.marketId + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
            }
          });
          // res.data.bookmaker.forEach((item) => {
          //   if (this.GAME_BOOKDATA['bookmaker_' + item.secId] !== undefined) {
          //     this.GAME_BOOKDATA['bookmaker_' + item.secId] = parseInt(item.profit_loss);
          //   } else {
          //     this.GAME_BOOKDATA['bookmaker_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          //   }
          // });
        }
      });
  }

  // **Fancy two profit loss******************************/
  getFancyTwoPL(e) {
    this.isLoadFancyPl = false;
    $('#Book-Unbook').modal('show');
    $('#session-title').html('Book <i class="mdi mdi-arrow-right"></i>' + e.title);
    this.isFancy3 = e.mtype === 'fancy3' ? true : false;
    this.fancy2PL = [];
    this.fancyBets = [];
    if (e.marketid) {
      const data = {event_id: e.eventid, market_id: e.marketid, session_type: e.mtype};
      this.service.getBetList(data).subscribe((response: any) => {
          if (response.status === 1) {
            if (!this.isFancy3 && data.session_type !== 'khado' && data.session_type !== 'fancy3') {
              const plData = this.fancyBookCal(response.data.betList);
              plData.forEach(item => {
                this.fancy2PL.push(item);
              });
            } else if (!this.isFancy3 && data.session_type === 'khado') {
              const plData = this.getProfitLossKhado(response.data.betList);
              plData.forEach(item => {
                this.fancy2PL.push(item);
              });
            } else {
              const plData = this.getProfitLossOtherMarket(response.data.betList);
              this.fancy2PL = plData;
            }
            // if (!this.isFancy3) {
            //   response.data.profit_loss.forEach(item => {
            //     this.fancy2PL.push(item);
            //   });
            // } else {
            //   this.fancy2PL = response.data.profit_loss;
            // }
            this.fancyBets = response.data.betList;
          }
          this.isLoadFancyPl = true;
        });
    }
  }

}
