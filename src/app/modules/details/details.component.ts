import {Component, OnInit, Injector, Inject, ElementRef, ViewChild, AfterViewInit, OnDestroy, Renderer2} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {BaseComponent} from '../../share/components/common.component';
import {NavigationEnd} from '@angular/router';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {AlertService, APIService, SharedataService} from '../../core/services';
import {BetplaceComponent} from '../../share/components/bottom-sheet/betplace.component';

declare let $;

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('issectionone', {static: false}) issectionone: ElementRef;
  @ViewChild('issectionsecond', {static: false}) issectionsecond: ElementRef;
  @ViewChild('issectionthird', {static: false}) issectionthird: ElementRef;

  @ViewChild('pRef', {static: false}) pRef: ElementRef;
  @ViewChild('cmRef', {static: false}) cmRef: ElementRef;
  @ViewChild('tmRef', {static: false}) tmRef: ElementRef;
  @ViewChild('wnRef', {static: false}) wnRef: ElementRef;
  @ViewChild('glRef', {static: false}) glRef: ElementRef;
  @ViewChild('setMarketRef', {static: false}) setMarketRef: ElementRef;
  @ViewChild('bookmakerRef', {static: false}) bookmakerRef: ElementRef;
  @ViewChild('virtualCricketRef', {static: false}) virtualCricketRef: ElementRef;
  @ViewChild('fancyRef', {static: false}) fancyRef: ElementRef;
  @ViewChild('fancy2Ref', {static: false}) fancy2Ref: ElementRef;
  @ViewChild('fancy3Ref', {static: false}) fancy3Ref: ElementRef;
  @ViewChild('fancy31Ref', {static: false}) fancy31Ref: ElementRef;
  @ViewChild('meterRef', {static: false}) meterRef: ElementRef;
  @ViewChild('oddevenRef', {static: false}) oddevenRef: ElementRef;
  @ViewChild('khadoRef', {static: false}) khadoRef: ElementRef;
  @ViewChild('ballbyballRef', {static: false}) ballbyballRef: ElementRef;
  @ViewChild('profitLossModal', {static: false}) profitLossModal: ElementRef;

  public oldGdata: any = {};
  public oldCTdata: any = {};
  public oldGdataCnt = 0;
  public primaryColor: string;
  public betlistLoading: boolean;

  // **VARIABLE-DECLARATION************************************

  // **DATA-STORAGE************************************
  public matchData: any;
  public oddsData: any;
  public marketArr: any;
  public currantBets: any = [];
  public fancy2PL: any = [];
  public fancyBets: any = [];

  // **PROFIT-LOSS BOOK-GLOBLE************************************
  public GAME_BOOKDATA: any = [];

  // **SINGLE-LINE-DATA************************************
  public eventId: string;
  public eventTitle: string;
  public marketId: string;
  public bindTVURL: string;
  public bindSCOREURL: string;
  public eventMsg: string;
  public updatedTime: number;
  public annimationClass: string;

  // **FLAGE************************************
  public isScoreBoard: boolean;
  public isTv: boolean;
  public isTabs: string;
  public isFirstLoad: boolean;
  public isDestroy: boolean;
  public isLoadFirst: boolean;
  public isLoadFancyPl: boolean;
  public isFancy3: boolean;
  public loaderShow: boolean;
  public isBetloaderShow: boolean;
  public isActivePlaceBet: boolean;

  // **INTERVAL******************************
  public interval1000: any;
  public interval300: any;
  public isActivePlaceBetIndex: string;
  public isActivePlaceBetSType: string;
  public isActivePlaceBetArrIndx: string;
  public isActivePlaceBetMarket: string;
  private onUpdate: number;
  private intervaleCase: number;

  private navigationSubscription;
  public marketActive: number;

  constructor(inj: Injector,
              @Inject(DOCUMENT) private document: Document,
              private alert: AlertService,
              private commonService: SharedataService,
              private elRef: ElementRef,
              private renderer: Renderer2,
              private placeBetSheet: MatBottomSheet,
              private service: APIService) {
    super(inj);
    this.primaryColor = this.sysConfigDt.themeDefaultSetting(this.sysConfigDt.CURRANT_THEME).primaryColor;
    this.eventId = this.activatedRoute.snapshot.params.id;
    this.commonService.eventId.emit(this.activatedRoute.snapshot.params.id);
    this.marketArr = [];

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        if (this.eventId !== undefined && this.eventId !== this.activatedRoute.snapshot.params.id) {
          this.reloadCurrentRoute();
        }
      }
    });
    this.valInit();
  }

  ngOnInit() {
    this.spinner.show();
    this.getEventDetail();
    this.getProfitLoss();

    this.commonService.betStatus.subscribe((res) => { // Bet Flags Manage
      if (res === 1) {
        this.isActivePlaceBet = true;
      } else if (res === 2) {
        this.isActivePlaceBet = false;
        this.isBetloaderShow = true;
        this.spinner.show();
      } else if (res === 3) {
        this.isBetloaderShow = false;
        this.spinner.hide();
        this.getProfitLoss();
        // this.getBetList();
      }
    });
  }

  ngAfterViewInit(): void {
    this.bindTVURL = this.sysConfigDt.LIVE_TV_URL + this.eventId;
    this.bindSCOREURL = this.sysConfigDt.LIVE_SCORE_URL + this.eventId;

    // if (this.platformName === 'mobile') {
    //   this.testTV1();
    // } else {
    //   this.testTV();
    // }
  }

  ngOnDestroy(): void {
    this.isDestroy = true;
    this.matchData = [];
    this.marketArr = [];
    this.oddsData = [];
    clearTimeout(this.interval1000);
    clearTimeout(this.interval300);
    this.commonService.eventId.emit('');
    this.commonService.betslist.emit('');
    this.commonService.localCommantry.emit('');
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  reloadCurrentRoute() {
    const currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  onoffTVScore(type) {
    if (type === 'tv') {
      this.isTv = !this.isTv;
      this.isScoreBoard = false;
    } else {
      this.isTv = false;
      this.isScoreBoard = !this.isScoreBoard;
    }
    // this.isTv === true ?  this.isScoreBoard = false : this.isScoreBoard = true;
    this.changedRef.detectChanges();
  }

  valInit() {
    this.intervaleCase = 1;
    this.isScoreBoard = false;
    this.isTv = false;
    this.isTabs = 'live-market';
    this.isFirstLoad = false;
    this.isDestroy = false;
    this.isLoadFirst = true;
    this.annimationClass = 'fadeInUp';
    this.isActivePlaceBet = false;
    this.isActivePlaceBetSType = '';
    this.isBetloaderShow = false;
    this.loaderShow = false;
    this.currantBets = [];
    this.betlistLoading = true;

  }

  changeTab(tab: string) {
    this.isTabs = tab;
    this.commonService.setRouterChange('1'); // CALL TO HEADER API
    this.getBetList();
  }


  getEventDetail(): void {
    this.service.geteventDetail(this.eventId).subscribe((res) => {
      if (res.status === 1 && res.data != null) {
        try {
          this.intervaleCase = 1;
          this.matchData = res.data.items;
          this.eventTitle = res.data.items.title; // SET EVENT TITLE

          if (this.marketArr.length === 0) { // INIT MARKET ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
          } else if (this.marketArr.length !== res.data.marketIdsArr.length) { // CHECK DIFF. OF CURRANT AND OLD ARRAY
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = false;
            clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          } else if (this.marketArr.length === res.data.marketIdsArr.length && res.data.items.ballbyball) {
            this.marketArr = res.data.marketIdsArr;
            this.isFirstLoad = true;
            // clearInterval(this.interval300); // API CALLBACK INTERVAL CLEAR
          }
          if (!this.isFirstLoad && this.marketArr.length) { // LOAD FIRST TIME ODDS FUNCTIONS
            this.isFirstLoad = true;
            this.getOdds();
          }

          if (this.onUpdate === undefined && res.updatedOn !== null) {  // SET GAMEOVET TIMES
            this.onUpdate = res.updatedOn;
          } else if (this.onUpdate !== null && this.onUpdate !== res.updatedOn) { // CHECK IF GAMEOVER TIME CHANGE OR NOT
            this.onUpdate = res.updatedOn;
            this.commonService.setRouterChange('3');
            this.getProfitLoss();
          }
          this.clearSesssionBind();
          this.changedRef.detectChanges(); // VIEW UPDATE
        } catch (e) {
          console.log('Failed To Set Event Data', e);
        }
      } else {  // AFTER GAME OVER  TO RECALL OPEN EVENT
        this.intervaleCase = 2;
        this.clearSesssionBind();
        this.eventMsg = res.msg;
        this.eventTitle = '';
        this.matchData = [];
        this.marketArr = [];
        if (!this.isDestroy && this.eventMsg !== '' && this.intervaleCase === 2) { // GAME OVER OR RECALL EVENT LOADED
          this.interval1000 = setTimeout(() => {
            this.getEventDetail();
          }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
        }
      }

      if (!this.loaderShow) {
        this.loaderShow = true;
        this.spinner.hide();
      }

      if (!this.isDestroy && this.intervaleCase === 1) { // API CALL TO GET REAL TIME UPDATES
        this.interval1000 = setTimeout(() => {
          this.getEventDetail();
        }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
      }
    }, (error) => {
      this.clearSesssionBind();
      if (!this.isDestroy) { // API CALL TO GET ERROR OR FAIL API CALL
        this.interval1000 = setTimeout(() => {
          this.getEventDetail();
        }, this.getToken('eventDetailTiming') !== null ? parseInt(this.getToken('eventDetailTiming')) : 5000);
      }
      console.log('Error-detail', error);
      if (!this.loaderShow) {
        this.loaderShow = true;
        this.spinner.hide();
      }
    });
  }

  getOdds() {
    this.service.getOddsData(this.marketArr).subscribe((resData) => {
        try {
          this.oddsData = resData.data.items;
          if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '' && this.isActivePlaceBetSType === 'match_odd') { // BET SHEET
            const tmp = this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetArrIndx];
            if (this.matchData.match_odd !== null && this.isActivePlaceBetMarket === 'match_odd') {
              tmp.suspended = this.matchData.match_odd.runners[this.isActivePlaceBetIndex].suspended;
            } else if (this.matchData.completed_match !== null && this.isActivePlaceBetMarket === 'completed_match') {
              tmp.suspended = this.matchData.completed_match.runners[this.isActivePlaceBetIndex].suspended;
            } else if (this.matchData.tied_match !== null && this.isActivePlaceBetMarket === 'tied_match') {
              tmp.suspended = this.matchData.tied_match.runners[this.isActivePlaceBetIndex].suspended;
            } else if (this.matchData.winner !== null && this.isActivePlaceBetMarket === 'winner') {
              tmp.suspended = this.matchData.winner.runners[this.isActivePlaceBetIndex].suspended;
            } else {
              tmp.suspended = 0;
            }
            this.commonService.setMarketChange(tmp);
          } else if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && this.isActivePlaceBetSType !== '') {
            this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
          }

          if (resData.data.items.match_odd.length && this.matchData.match_odd) { // MATCH ODDS SESSION DATA BINDING
            this.issectionone.nativeElement.style.display = 'block';
            this.bindMatchData(resData.data.items.match_odd, this.matchData.match_odd);
          }

          if (resData.data.items.match_odd.length && this.matchData.completed_match) { // COMPLATED MATCHED SESSION DATA BINDING
            this.issectionone.nativeElement.style.display = 'block';
            this.bindCompletedMatchData(resData.data.items.match_odd, this.matchData.completed_match);
            // this.bindMatchData(resData.data.items.match_odd, this.matchData.completed_match, 'completed match');
          }

          if (resData.data.items.match_odd.length && this.matchData.tied_match) { // TIED MATCH SESSION DATA BINDING
            this.issectionone.nativeElement.style.display = 'block';
            this.bindTiedMatchData(resData.data.items.match_odd, this.matchData.tied_match);
            // this.bindMatchData(resData.data.items.match_odd, this.matchData.tied_match, 'tied match');
          }

          if (resData.data.items.match_odd.length && this.matchData.winner) { // WINNER SESSION DATA BINDING
            this.issectionone.nativeElement.style.display = 'block';
            this.bindWinnerMatchData(resData.data.items.match_odd, this.matchData.winner);
          }

          if (resData.data.items.match_odd.length && this.matchData.goals) { // GOAL SESSION DATA BINDING
            this.issectionone.nativeElement.style.display = 'block';
            this.bindGoalData(resData.data.items.match_odd, this.matchData.goals);
          }

          if (resData.data.items.match_odd.length && this.matchData.set_market) { // SetMarket SESSION DATA BINDING
            this.issectionone.nativeElement.style.display = 'block';
            this.bindSetMarketData(resData.data.items.match_odd, this.matchData.set_market);
          }

          if (resData.data.items.bookmaker && resData.data.items.bookmaker.length) { // BOOKMAKER SESSION DATA BINDING
            this.issectionsecond.nativeElement.style.display = 'block';
            this.bindBookmakerData(resData.data.items.bookmaker, this.matchData.bookmaker, 'bookmaker');
          }

          if (resData.data.items.virtual_cricket && resData.data.items.virtual_cricket.length) { // VIRTUAL CRICKET SESSION DATA BINDING
            this.issectionsecond.nativeElement.style.display = 'block';
            this.changedRef.detectChanges();
            // this.bindVirtualCricketData(resData.data.items.virtual_cricket, this.matchData.virtual_cricket);
            this.bindBookmakerData(resData.data.items.virtual_cricket, this.matchData.virtual_cricket, 'virtual-cricket');
          }

          if (resData.data.items.fancy2 && resData.data.items.fancy2.length && this.matchData.fancy2 !== null) { // FANCY2 SESSION DATA BINDING
            this.issectionsecond.nativeElement.style.display = 'block';
            this.bindFancy2Data(resData.data.items.fancy2, this.matchData.fancy2, 'fancy2');
          }

          if (resData.data.items.fancy && resData.data.items.fancy && this.matchData.fancy) { // FANCY(LIVE) SESSION DATA BINDING
            this.issectionsecond.nativeElement.style.display = 'block';
            // this.bindFancyData(resData.data.items.fancy, this.matchData.fancy);
            this.bindFancy2Data(resData.data.items.fancy, this.matchData.fancy, 'fancy');
          }

          if (resData.data.items.ballbyball && resData.data.items.ballbyball && this.matchData.ballbyball) { // BALLBYBALL SESSION DATA BINDING
            this.issectionsecond.nativeElement.style.display = 'block';
            this.bindBallByBallData(resData.data.items.ballbyball, this.matchData.ballbyball);
          }

          if (resData.data.items.fancy3 && resData.data.items.fancy3 && this.matchData.fancy3) { // FANCY3 SESSION DATA BINDING
            if (this.issectionthird) {
              this.issectionthird.nativeElement.style.display = 'block';
            }
            this.bindFancy3Data(resData.data.items.fancy3, this.matchData.fancy3);
          }

          if (resData.data.items.oddeven && resData.data.items.oddeven && this.matchData.oddeven) { // ODD EVEN SESSION DATA BINDING
            this.issectionthird.nativeElement.style.display = 'block';
            this.bindOddEvenData(resData.data.items.oddeven, this.matchData.oddeven);
          }

          if (resData.data.items.khado && resData.data.items.khado && this.matchData.khado) { // KHADO SESSION DATA BINDING
            this.issectionthird.nativeElement.style.display = 'block';
            this.bindKhadoData(resData.data.items.khado, this.matchData.khado);
          }

          if (resData.data.items.meter && resData.data.items.meter.length && this.matchData.meter !== null) { // METER SESSION DATA BINDING
            this.issectionthird.nativeElement.style.display = 'block';
            this.bindMeterData(resData.data.items.meter, this.matchData.meter);
          }

          // EVENT BINDING
          const tThis = this;
          const clickButtons = this.elRef.nativeElement.querySelectorAll('.bet-sec'); // PLACE BET
          const clickButtonsModelPl = this.elRef.nativeElement.querySelectorAll('.bookModalDetail'); // BOOK
          for (let i = 0; i < clickButtons.length; i++) { // BET PLACE CLICK EVENT HANDEL
            this.renderer.listen(clickButtons[i], 'click', ($event) => {
              tThis._placebet(clickButtons[i].dataset);
            });
          }

          for (let i = 0; i < clickButtonsModelPl.length; i++) { // BOOK MODAL CLICK EVENT HANDEL
            this.renderer.listen(clickButtonsModelPl[i], 'click', ($event) => {
              tThis.getFancyTwoPL(clickButtonsModelPl[i].dataset);
            });
          }

          this.annimationClass = '';
          if (!this.isDestroy) { // && this.isScreenActive CHECK COLSE PAGE TO STOP CALLS
            this.interval300 = setTimeout(() => {
              clearTimeout(this.interval300);
              if (this.marketArr.length) { // CHECK IS MARKET DATA IS VALID
                this.getOdds();
              } else {
                this.isFirstLoad = false;
              }
              // tslint:disable-next-line:radix
            }, this.getToken('oddsTiming') !== null ? parseInt(this.getToken('oddsTiming')) : 400); //
          }
          if (!this.loaderShow) { // INIT LOADER STOP
            this.loaderShow = true;
            this.spinner.hide();
          }
        } catch (e) { // HANDEAL TO ANY KIND OF ERROR ON CODE
          console.log('Failed to set data in Odss Api', e);
        }
      },
      (error) => {
        if (!this.isDestroy) {
          this.interval300 = setTimeout(() => {
            clearTimeout(this.interval300);
            if (this.marketArr.length) {
              this.getOdds();
            } else {
              this.isFirstLoad = false;
            }
          }, this.getToken('oddsTiming') !== null ? parseInt(this.getToken('oddsTiming')) : 400); //
        }
        if (!this.loaderShow) {
          this.loaderShow = true;
          this.spinner.hide();
        }
        console.log('error-Odds', error);
      });
  }

  clearSesssionBind() { // CHECK ANY SESSION COLSE TO CLEAR DATA
    if (this.matchData === undefined) {
      return;
    }
    let checkNoSS = false;
    if (this.matchData && this.matchData.sport_id === undefined && !this.matchData.length) {
      this.pRef.nativeElement.innerHTML = '';
      this.tmRef.nativeElement.innerHTML = '';
      this.cmRef.nativeElement.innerHTML = '';
      this.wnRef.nativeElement.innerHTML = '';
      this.glRef.nativeElement.innerHTML = '';

      if (this.bookmakerRef) {
        this.bookmakerRef.nativeElement.innerHTML = '';
      }
      if (this.virtualCricketRef) {
        this.virtualCricketRef.nativeElement.innerHTML = '';
      }
      if (this.fancyRef) {
        this.fancyRef.nativeElement.innerHTML = '';
      }
      if (this.fancy2Ref) {
        this.fancy2Ref.nativeElement.innerHTML = '';
      }
      if (this.checkUndefinedEle('fancy3Ref')) {
        this.fancy3Ref.nativeElement.innerHTML = '';
      }
      if (this.khadoRef) {
        this.khadoRef.nativeElement.innerHTML = '';
      }
      if (this.ballbyballRef) {
        this.ballbyballRef.nativeElement.innerHTML = '';
      }
      if (this.oddevenRef) {
        this.oddevenRef.nativeElement.innerHTML = '';
      }

      if (this.meterRef) {
        this.meterRef.nativeElement.innerHTML = '';
      }
      this.spinner.hide();
      return;
    }
    if (this.intervaleCase === 1) {
      if (this.matchData.match_odd === null) {
        this.pRef.nativeElement.innerHTML = '';
        checkNoSS = true;
      }
      if (this.matchData.tied_match === null) {
        this.tmRef.nativeElement.innerHTML = '';
        checkNoSS = true;
      }
      if (this.matchData.completed_match === null) {
        this.cmRef.nativeElement.innerHTML = '';
        checkNoSS = true;
      }
      if (this.matchData.winner === null) {
        this.wnRef.nativeElement.innerHTML = '';
        checkNoSS = true;
      }
      if (this.matchData.goal === null) {
        this.glRef.nativeElement.innerHTML = '';
        checkNoSS = true;
      }
      if (this.matchData.bookmaker === null) {
        this.bookmakerRef.nativeElement.innerHTML = '';
        checkNoSS = true;
      }
      if (this.matchData.virtual_cricket === null) {
        this.virtualCricketRef.nativeElement.innerHTML = '';
        checkNoSS = true;
      }
      if (this.matchData.fancy === null) {
        checkNoSS = true;
        this.fancyRef.nativeElement.innerHTML = '';
      }
      if (this.matchData.fancy2 === null && this.fancy2Ref) {
        checkNoSS = true;
        this.fancy2Ref.nativeElement.innerHTML = '';
      }
      if (this.matchData.fancy3 === null && this.fancy3Ref) {
        checkNoSS = true;
        this.fancy3Ref.nativeElement.innerHTML = '';
      }
      if (this.matchData.khado === null && this.khadoRef) {
        checkNoSS = true;
        this.khadoRef.nativeElement.innerHTML = '';
      }
      if (this.matchData.ballbyball === null && this.ballbyballRef) {
        checkNoSS = true;
        this.ballbyballRef.nativeElement.innerHTML = '';
      }
      if (this.matchData.oddeven === null && this.oddevenRef) {
        checkNoSS = true;
        this.oddevenRef.nativeElement.innerHTML = '';
      }
      if (this.matchData.meter === null && this.meterRef) {
        checkNoSS = true;
        this.meterRef.nativeElement.innerHTML = '';
      }

      if (checkNoSS && !this.isBetloaderShow) {
        this.spinner.hide();
      }
    }
  }

  checkUndefinedEle(el: string) {
    if (this[el]) {
      this[el].nativeElement.innerHTML = '';
      return true;
    }
    return false;
  }

  checkMarketOdds(data: any, marketId: any) {  //  CHECK MATCH ODDS DATA || COMPLATED MATCH ODDS DATA || TIED MACTH ODDS
    try {
      if (data.length > 0 && data[0] && data[0].market_id === marketId) {
        return {data: data[0], index: 0};
      }
      if (data.length > 1 && data[1] && data[1].market_id === marketId) {
        return {data: data[1], index: 1};
      }
      if (data.length > 2 && data[2] && data[2].market_id === marketId) {
        return {data: data[2], index: 2};
      }
      return null;
    } catch (e) {
      console.log('Failed to CheckMarket Odds Function', e);
    }
  }

  bindMatchData(data, matchData) { // BIND TO MATCH ODDS SESSION DATA
    try {
      if (matchData === null) { // CHECK MATCHED DATA IS NULL
        return false;
      }
      const tmpdata = this.checkMarketOdds(data, matchData ? matchData.marketId : 0); // CHECK MARKET ID
      if (tmpdata === null) {
        return false;
      }
      data = tmpdata.data;
      let html = '';
      if (this.oldGdataCnt === 0) { // INIT ODDS DATA
        this.oldGdata = data.odds;
        this.oldGdataCnt = 1;
      }

      html += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
        '<div class="col-md-4 col-7"><p class="market-title">Match odds</p></div>' +
        '<div class="col-md-6 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < matchData.runners.length; i++) { // LOOPING TO SET RUNNER DATA
        if (data.odds && data.odds[i].selectionId === undefined) { // CHECK IS VALID RUNNER TO RETURN SELECTION ID
          return false;
        }
        const id = data.odds[i].selectionId.toString();
        let backblink = '';
        let layblink = '';
        if (data.odds[i].backPrice1 !== '0.00') { // CHECK PRICE CHANGE TO BLINK
          if (this.oldGdata[i].backPrice1 !== data.odds[i].backPrice1) {
            backblink = 'blink-back';
          }
        }
        if (data.odds[i].layPrice1 !== '0.0') { // CHECK PRICE CHANGE TO BLINK
          if (this.oldGdata[i].layPrice1 !== data.odds[i].layPrice1) {
            layblink = 'blink-lay';
          }
        }

        this.oldGdata[i].backPrice1 = data.odds[i].backPrice1; // REPLACE TO OLD TO NEW PRICE
        this.oldGdata[i].layPrice1 = data.odds[i].layPrice1;

        let suspended = '';
        let suspendedTitle = 'SUSPENDED';

        if (matchData.runners[i].suspended === 1 || data.status === 'CLOSED' || data.status === 'SUSPENDED') { // CHECK SUSPENDED
          suspended = 'suspended';
          suspendedTitle = data.status;
        }

        let pdata = 0; // PROFITLOSS BOOK SET
        if (this.GAME_BOOKDATA[matchData.runners[i].mType + '_' + data.odds[i].selectionId] !== undefined) {
          pdata = this.GAME_BOOKDATA[matchData.runners[i].mType + '_' + data.odds[i].selectionId];
        }

        let bcolor = ''; // BOOK PLUS OR MINUS COLOR SET
        if (pdata <= -1) {
          bcolor = 'red';
        } else if (pdata >= 1) {
          bcolor = 'green';
        } else {
          bcolor = 'black';
        }

        html += '<div data-title="' + suspendedTitle + '" id="match-odds' + matchData.marketId + '" data-sessiontype="match_odd" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
        html += '<div class="col-md-4 col-7"><p class="market-name">' + matchData.runners[i].runnerName + '<span class="odd-number ' + bcolor + '">' + pdata + '</span></p></div>';
        html += '';
        html += '<div class="col-md-6 col-5"><div class="bl-buttons">';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice3 === '-' ? '0' : data.odds[i].backPrice3) + ' <span>' + (data.odds[i].backPrice3 === '-' ? '00' : data.odds[i].backSize3) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice2 === '-' ? '0' : data.odds[i].backPrice2) + ' <span>' + (data.odds[i].backPrice2 === '-' ? '00' : data.odds[i].backSize2) + '</span></button>';
        html += '<button class="bl-btn back waves-effect waves-light bet-sec ' + backblink + '" data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].backPrice1 + '" data-btype="back" data-mtype="match_odd" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].backPrice1 === '-' ? '0' : data.odds[i].backPrice1) + ' <span>' + (data.odds[i].backPrice1 === '-' ? '00' : data.odds[i].backSize1) + '</span></button>';
        html += '<button class="bl-btn lay waves-effect waves-light bet-sec ' + layblink + '"  data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].layPrice1 + '" data-btype="lay" data-mtype="match_odd" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].layPrice1 === '-' ? '0' : data.odds[i].layPrice1) + ' <span>' + (data.odds[i].layPrice1 === '-' ? '00' : data.odds[i].laySize1) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice2 === '-' ? '0' : data.odds[i].layPrice2) + ' <span>' + (data.odds[i].layPrice2 === '-' ? '00' : data.odds[i].laySize2) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice3 === '-' ? '0' : data.odds[i].layPrice3) + ' <span>' + (data.odds[i].layPrice3 === '-' ? '00' : data.odds[i].laySize3) + '</span></button>';
        if (suspended !== '') {
          html += '<div class="suspended"><span>' + suspended + '</span></div>';
        }
        html += '</div></div>' +
          '<div class="col-md-2 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + matchData.runners[i].minStack + '</b></span><span>max stakes: <b>' + matchData.runners[i].maxStack + '</b></span></p></div></div>';
      }
      if (matchData.info && matchData.info !== null && matchData.info !== 'null' && matchData.info !== '') {
        html += '<p class="alert-text">' + matchData.info + '</p>';
      }
      html += '</div>';
      this.pRef.nativeElement.innerHTML = html; // SESSION FINALY BIND
    } catch (e) {
      console.log('FAILDED TO BIND MATCH ODDS', e);
    }
  }

  bindCompletedMatchData(data, matchData) {
    // debugger;
    try {
      if (matchData === null) {
        return false;
      }
      let html = '';
      const tmpdata = this.checkMarketOdds(data, matchData ? matchData.marketId : 0); // CHECK MARKET ID
      if (tmpdata === null) {
        return false;
      }
      data = tmpdata.data;
      if (!this.oldCTdata[matchData.marketId]) {
        this.oldCTdata[matchData.marketId] = data.odds;
      }

      html += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
        '<div class="col-md-4 col-7"><p class="market-title">' + matchData.marketName + '</p></div>' +
        '<div class="col-md-6 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < matchData.runners.length; i++) {
        if (data.odds && data.odds[i].selectionId === undefined) {
          return false;
        }
        const id = data.odds[i].selectionId.toString();
        let backblink = '';
        let layblink = '';
        if (data.odds[i].backPrice1 !== '0.00') {
          if (this.oldCTdata[matchData.marketId][i].backPrice1 !== data.odds[i].backPrice1) {
            backblink = 'blink-back';
          }
        }
        if (data.odds[i].layPrice1 !== '0.0') {
          if (this.oldCTdata[matchData.marketId][i].layPrice1 !== data.odds[i].layPrice1) {
            layblink = 'blink-lay';
          }
        }

        this.oldCTdata[matchData.marketId][i].backPrice1 = data.odds[i].backPrice1;
        this.oldCTdata[matchData.marketId][i].layPrice1 = data.odds[i].layPrice1;

        let suspended = '';
        let suspendedTitle = 'SUSPENDED';

        if (matchData.runners[i].suspended === 1 || data.status === 'CLOSED' || data.status === 'SUSPENDED') {
          suspended = 'suspended';
          suspendedTitle = data.status;
        }

        let borderClass = '';
        if (i <= 1) {
          borderClass = 'border-bottom';
        }

        let pdata = 0;
        if (this.GAME_BOOKDATA[matchData.runners[i].mType + '_' + data.odds[i].selectionId] !== undefined) {
          pdata = this.GAME_BOOKDATA[matchData.runners[i].mType + '_' + data.odds[i].selectionId];
        }

        let bcolor = '';
        if (pdata <= -1) {
          bcolor = 'red';
        } else if (pdata >= 1) {
          bcolor = 'green';
        } else {
          bcolor = 'black';
        }

        html += '<div data-title="' + suspendedTitle + '" id="match-odds' + matchData.marketId + '" data-sessiontype="complated_match" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
        html += '<div class="col-md-4 col-7"><p class="market-name">' + matchData.runners[i].runnerName + '<span class="odd-number ' + bcolor + '">' + pdata + '</span></p></div>';
        html += '';
        html += '<div class="col-md-6 col-5"><div class="bl-buttons">';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice3 === '-' ? '0' : data.odds[i].backPrice3) + ' <span>' + (data.odds[i].backPrice3 === '-' ? '00' : data.odds[i].backSize3) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice2 === '-' ? '0' : data.odds[i].backPrice2) + ' <span>' + (data.odds[i].backPrice2 === '-' ? '00' : data.odds[i].backSize2) + '</span></button>';
        html += '<button class="bl-btn back waves-effect waves-light bet-sec ' + backblink + '" data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].backPrice1 + '" data-btype="back" data-mtype="' + matchData.mType + '" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].backPrice1 === '-' ? '0' : data.odds[i].backPrice1) + ' <span>' + (data.odds[i].backPrice1 === '-' ? '00' : data.odds[i].backSize1) + '</span></button>';
        html += '<button class="bl-btn lay waves-effect waves-light bet-sec ' + layblink + '"  data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].layPrice1 + '" data-btype="lay" data-mtype="' + matchData.mType + '" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].layPrice1 === '-' ? '0' : data.odds[i].layPrice1) + ' <span>' + (data.odds[i].layPrice1 === '-' ? '00' : data.odds[i].laySize1) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice2 === '-' ? '0' : data.odds[i].layPrice2) + ' <span>' + (data.odds[i].layPrice2 === '-' ? '00' : data.odds[i].laySize2) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice3 === '-' ? '0' : data.odds[i].layPrice3) + ' <span>' + (data.odds[i].layPrice3 === '-' ? '00' : data.odds[i].laySize3) + '</span></button>';
        if (suspended !== '') {
          html += '<div class="suspended"><span>' + suspended + '</span></div>';
        }
        html += '</div></div>' +
          '<div class="col-md-2 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + matchData.runners[i].minStack + '</b></span><span>max stakes: <b>' + matchData.runners[i].maxStack + '</b></span></p></div></div>';
      }

      if (matchData.info && matchData.info !== null && matchData.info !== 'null' && matchData.info !== '') {
        html += '<p class="alert-text">' + matchData.info + '</p>';
      }
      html += '  </div>';
      this.cmRef.nativeElement.innerHTML = html;
    } catch (e) {
      console.log('FAILDED TO BIND COMPLATED MATCHED', e);
    }
  }

  bindTiedMatchData(data, matchData) {
    try {
      if (matchData === null) {
        return false;
      }
      let html = '';
      const tmpdata = this.checkMarketOdds(data, matchData ? matchData.marketId : 0); // CHECK MARKET ID
      if (tmpdata === null) {
        return false;
      }
      data = tmpdata.data;
      if (!this.oldCTdata[matchData.marketId]) {
        this.oldCTdata[matchData.marketId] = data.odds;
      }

      html += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
        '<div class="col-md-4 col-7"><p class="market-title">' + matchData.marketName + '</p></div>' +
        '<div class="col-md-6 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < matchData.runners.length; i++) {
        if (data.odds && data.odds[i].selectionId === undefined) {
          return false;
        }
        const id = data.odds[i].selectionId.toString();
        let backblink = '';
        let layblink = '';
        if (data.odds[i].backPrice1 !== '0.00') {
          if (this.oldCTdata[matchData.marketId][i].backPrice1 !== data.odds[i].backPrice1) {
            backblink = 'blink-back';
          }
        }
        if (data.odds[i].layPrice1 !== '0.0') {
          if (this.oldCTdata[matchData.marketId][i].layPrice1 !== data.odds[i].layPrice1) {
            layblink = 'blink-lay';
          }
        }

        this.oldCTdata[matchData.marketId][i].backPrice1 = data.odds[i].backPrice1;
        this.oldCTdata[matchData.marketId][i].layPrice1 = data.odds[i].layPrice1;

        let suspended = '';
        let suspendedTitle = 'SUSPENDED';

        if (matchData.runners[i].suspended === 1 || data.status === 'CLOSED' || data.status === 'SUSPENDED') {
          suspended = 'suspended';
          suspendedTitle = data.status;
        }

        let borderClass = '';
        if (i <= 1) {
          borderClass = 'border-bottom';
        }

        let pdata = 0;
        if (this.GAME_BOOKDATA[matchData.runners[i].mType + '_' + data.odds[i].selectionId] !== undefined) {
          pdata = this.GAME_BOOKDATA[matchData.runners[i].mType + '_' + data.odds[i].selectionId];
        }

        let bcolor = '';
        if (pdata <= -1) {
          bcolor = 'red';
        } else if (pdata >= 1) {
          bcolor = 'green';
        } else {
          bcolor = 'black';
        }

        html += '<div data-title="' + suspendedTitle + '" id="match-odds' + matchData.marketId + '" data-sessiontype="complated_match" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
        html += '<div class="col-md-4 col-7"><p class="market-name">' + matchData.runners[i].runnerName + '<span class="odd-number ' + bcolor + '">' + pdata + '</span></p></div>';
        html += '';
        html += '<div class="col-md-6 col-5"><div class="bl-buttons">';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice3 === '-' ? '0' : data.odds[i].backPrice3) + ' <span>' + (data.odds[i].backPrice3 === '-' ? '00' : data.odds[i].backSize3) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (data.odds[i].backPrice2 === '-' ? '0' : data.odds[i].backPrice2) + ' <span>' + (data.odds[i].backPrice2 === '-' ? '00' : data.odds[i].backSize2) + '</span></button>';
        html += '<button class="bl-btn back waves-effect waves-light bet-sec ' + backblink + '" data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].backPrice1 + '" data-btype="back" data-mtype="' + matchData.mType + '" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].backPrice1 === '-' ? '0' : data.odds[i].backPrice1) + ' <span>' + (data.odds[i].backPrice1 === '-' ? '00' : data.odds[i].backSize1) + '</span></button>';
        html += '<button class="bl-btn lay waves-effect waves-light bet-sec ' + layblink + '"  data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + data.odds[i].layPrice1 + '" data-btype="lay" data-mtype="' + matchData.mType + '" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (data.odds[i].layPrice1 === '-' ? '0' : data.odds[i].layPrice1) + ' <span>' + (data.odds[i].layPrice1 === '-' ? '00' : data.odds[i].laySize1) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice2 === '-' ? '0' : data.odds[i].layPrice2) + ' <span>' + (data.odds[i].layPrice2 === '-' ? '00' : data.odds[i].laySize2) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (data.odds[i].layPrice3 === '-' ? '0' : data.odds[i].layPrice3) + ' <span>' + (data.odds[i].layPrice3 === '-' ? '00' : data.odds[i].laySize3) + '</span></button>';
        if (suspended !== '') {
          html += '<div class="suspended"><span>' + suspended + '</span></div>';
        }
        html += '</div></div>' +
          '<div class="col-md-2 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + matchData.runners[i].minStack + '</b></span><span>max stakes: <b>' + matchData.runners[i].maxStack + '</b></span></p></div></div>';
      }
      if (matchData.info && matchData.info !== null && matchData.info !== '') {
        html += '<p class="alert-text">' + matchData.info + '</p>';
      }
      html += '  </div>';
      this.tmRef.nativeElement.innerHTML = html;
    } catch (e) {
      console.log('FAILED TO BIND TEID MATCHED', e);
    }
  }

  bindWinnerMatchData(data, matchData) {
    try {
      if (matchData === null) {
        return false;
      }
      let html = '';
      const tmpdata = this.checkMarketOdds(data, matchData ? matchData.marketId : 0); // CHECK MARKET ID
      if (tmpdata === null) {
        return false;
      }
      if (matchData.marketId !== tmpdata.data.market_id) {
        console.log('WINNER-MARKET-ID-NOT-MATCHED');
        return false;
      }
      data = tmpdata.data;
      if (!this.oldCTdata[matchData.marketId]) {
        this.oldCTdata[matchData.marketId] = data.odds;
      }

      html += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
        '<div class="col-md-4 col-7"><p class="market-title">' + matchData.marketName + '</p></div>' +
        '<div class="col-md-6 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';


      for (let i = 0; i < matchData.runners.length; i++) {
        if (data.odds && data.odds[i].selectionId === undefined) {
          return false;
        }

        let datanew: any = {};
        for (let ii = 0; ii < tmpdata.data.odds.length; ii++) { // CHECK MARKET ID
          if (Number(tmpdata.data.odds[ii].selectionId) === Number(matchData.runners[i].selectionId)) {
            datanew = tmpdata.data.odds[ii];
            break;       // <=== breaks out of the loop early
          }
        }

        const sid = tmpdata.data.odds[i].selectionId.toString();
        let backblink = '';
        let layblink = '';
        if (datanew.backPrice1 !== '0.00') {
          if (this.oldCTdata[matchData.marketId][i].backPrice1 !== datanew.backPrice1) {
            backblink = 'blink-back';
          }
        }
        if (data.odds[i].layPrice1 !== '0.0') {
          if (this.oldCTdata[matchData.marketId][i].layPrice1 !== datanew.layPrice1) {
            layblink = 'blink-lay';
          }
        }

        this.oldCTdata[matchData.marketId][i].backPrice1 = datanew.backPrice1;
        this.oldCTdata[matchData.marketId][i].layPrice1 = datanew.layPrice1;

        let suspended = '';
        let suspendedTitle = 'SUSPENDED';

        if (matchData.runners[i].suspended === 1 || data.status === 'CLOSED' || data.status === 'SUSPENDED') {
          suspended = 'suspended';
          suspendedTitle = data.status;
        }

        let borderClass = '';
        if ((matchData.runners.length - 1) > i) {
          borderClass = 'border-bottom';
        }

        let pdata = 0;
        if (this.GAME_BOOKDATA['winner_' + matchData.runners[i].selectionId] !== undefined) {
          pdata = this.GAME_BOOKDATA['winner_' + matchData.runners[i].selectionId];
        }

        let bcolor = '';
        if (pdata <= -1) {
          bcolor = 'red';
        } else if (pdata >= 1) {
          bcolor = 'green';
        } else {
          bcolor = 'black';
        }

        html += '<div data-title="' + suspendedTitle + '" id="match-odds' + matchData.marketId + '" data-sessiontype="match_odd" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
        html += '<div class="col-md-4 col-7"><p class="market-name">' + matchData.runners[i].runnerName + '<span class="odd-number ' + bcolor + '">' + pdata + '</span></p></div>';
        html += '';
        html += '<div class="col-md-6 col-5"><div class="bl-buttons">';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (datanew.backPrice3 === '-' ? '0' : datanew.backPrice3) + ' <span>' + (datanew.backPrice3 === '-' ? '00' : datanew.backSize3) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + backblink + '">' + (datanew.backPrice2 === '-' ? '0' : datanew.backPrice2) + ' <span>' + (datanew.backPrice2 === '-' ? '00' : datanew.backSize2) + '</span></button>';
        html += '<button class="bl-btn back waves-effect waves-light bet-sec ' + backblink + '" data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + datanew.backPrice1 + '" data-btype="back" data-mtype="winner" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (datanew.backPrice1 === '-' ? '0' : datanew.backPrice1) + ' <span>' + (datanew.backPrice1 === '-' ? '00' : datanew.backSize1) + '</span></button>';
        html += '<button class="bl-btn lay waves-effect waves-light bet-sec ' + layblink + '"  data-rindex="' + i + '" data-oindex="' + tmpdata.index + '" data-marketname="match odds" data-sec="' + matchData.runners[i].selectionId + '" data-runnerName="' + matchData.runners[i].runnerName + '" data-rate="' + datanew.layPrice1 + '" data-btype="lay" data-mtype="winner" data-marketId="' + matchData.marketId + '" data-slug="' + matchData.slug + '" data-sportId="' + matchData.sportId + '" data-minstack="' + matchData.runners[i].minStack + '" data-maxstack="' + matchData.runners[i].maxStack + '" data-maxprofitlimit="' + matchData.runners[i].maxProfitLimit + '" data-index="' + i + '">' + (datanew.layPrice1 === '-' ? '0' : datanew.layPrice1) + ' <span>' + (datanew.layPrice1 === '-' ? '00' : datanew.laySize1) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (datanew.layPrice2 === '-' ? '0' : datanew.layPrice2) + ' <span>' + (datanew.layPrice2 === '-' ? '00' : datanew.laySize2) + '</span></button>';
        html += '<button class="bl-btn eon bet-sec ' + layblink + '">' + (datanew.layPrice3 === '-' ? '0' : datanew.layPrice3) + ' <span>' + (datanew.layPrice3 === '-' ? '00' : datanew.laySize3) + '</span></button>';
        if (suspended !== '') {
          html += '<div class="suspended"><span>' + suspended + '</span></div>';
        }
        html += '</div></div>' +
          '<div class="col-md-2 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + matchData.runners[i].minStack + '</b></span><span>max stakes: <b>' + matchData.runners[i].maxStack + '</b></span></p></div></div>';
      }
      if (matchData.info && matchData.info !== null && matchData.info !== 'null' && matchData.info !== '') {
        html += '<p class="alert-text">' + matchData.info + '</p>';
      }
      html += '</div>';

      this.wnRef.nativeElement.innerHTML = html;
    } catch (e) {
      console.log('FAILED TO BIND WINNER MATCHED', e);
    }
  }

  bindGoalData(dataOdd, goalData) {
    try {
      if (goalData === undefined || goalData === null || !goalData.length) {
        return false;
      }
      // fadeInUp
      let goalHtmlData = '';
      for (let i = 0; i < goalData.length; i++) {
        goalHtmlData += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">';
        goalHtmlData += '<div class="col-md-4 col-7"><p class="market-title">' + goalData[i].marketName + '</p></div>';
        goalHtmlData += '<div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

        for (let j = 0; j < goalData[i].runners.length; j++) {

          let data: any = {};
          let tmpOindex = 0;
          if (dataOdd[i].market_id === goalData[i].market_id) { // CHECK MARKET ID
            data = dataOdd[i];
            tmpOindex = i;
          } else {
            for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
              if (dataOdd[ii].market_id === goalData[i].market_id) {
                data = dataOdd[ii];
                tmpOindex = ii;
                break;       // <=== breaks out of the loop early
              }
            }
          }

          let suspended = '';
          if (goalData[i].runners[j].suspended === 1 || data.status === 'CLOSED' || data.status === 'SUSPENDED') { // CHECK SUSPENDED
            suspended = 'suspended';
          }

          let bdata = 0;
          let bcolor = '';
          if (this.GAME_BOOKDATA[goalData[i].runners[j].mType + '_' + goalData[i].market_id + '_' + goalData[i].runners[j].sec_id] !== undefined) {
            bdata = this.GAME_BOOKDATA[goalData[i].runners[j].mType + '_' + goalData[i].market_id + '_' + goalData[i].runners[j].sec_id];
          }

          if (bdata <= -1) {
            bcolor = 'red';
          } else if (bdata >= 1) {
            bcolor = 'green';
          } else {
            bcolor = 'black';
          }

          goalHtmlData += '<div data-title="' + suspended + '"  id="' + goalData[i].runners[j].sec_id + '" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-7">';
          goalHtmlData += '<p class="market-name">' + goalData[i].runners[j].runner + '<span class="' + bcolor + '">' + bdata + '</span></p></div>';
          goalHtmlData += '<div class="col-md-6 col-5"><div class="bl-buttons">';
          goalHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].backPrice3 === undefined ? '0' : data.odds[j].backPrice3) + ' <span>' + (data.odds[j].backSize3 === undefined ? '00' : data.odds[j].backSize3) + '</span></button>';
          goalHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].backPrice2 === undefined ? '0' : data.odds[j].backPrice2) + ' <span>' + (data.odds[j].backSize2 === undefined ? '00' : data.odds[j].backSize2) + '</span></button>';
          goalHtmlData += '<button class="bl-btn back waves-effect waves-light bet-sec" data-rindex="' + j + '" data-index="' + i + '" data-oindex="' + tmpOindex + '" data-marketname="' + goalData[i].marketName + '" data-sec="' + goalData[i].runners[j].sec_id + '" data-btype="back" data-rate="' + data.odds[j].backPrice1 + '" data-runnerName="' + goalData[i].runners[j].runner + '" data-mtype="' + goalData[i].runners[j].mType + '"  data-marketId="' + goalData[i].market_id + '" data-slug="' + goalData[i].slug + '" data-sportId="' + goalData[i].sportId + '" data-minstack="' + goalData[i].runners[j].minStack + '" data-maxstack="' + goalData[i].runners[j].maxStack + '" data-maxprofitlimit="' + goalData[i].runners[j].maxProfitLimit + '">' + (data.odds[j].backPrice1 === undefined ? '0' : data.odds[j].backPrice1) + ' <span>' + (data.odds[j].backSize1 === undefined ? '00' : data.odds[j].backSize1) + '</span></button>';
          goalHtmlData += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-rindex="' + j + '" data-index="' + i + '" data-oindex="' + tmpOindex + '" data-marketname="' + goalData[i].marketName + '" data-sec="' + goalData[i].runners[j].sec_id + '" data-btype="lay" data-rate="' + data.odds[j].layPrice1 + '" data-runnerName="' + goalData[i].runners[j].runner + '" data-mtype="' + goalData[i].runners[j].mType + '"  data-marketId="' + goalData[i].market_id + '" data-slug="' + goalData[i].slug + '" data-sportId="' + goalData[i].sportId + '" data-minstack="' + goalData[i].runners[j].minStack + '" data-maxstack="' + goalData[i].runners[j].maxStack + '" data-maxprofitlimit="' + goalData[i].runners[j].maxProfitLimit + '">' + (data.odds[j].layPrice1 === undefined ? '0' : data.odds[j].layPrice1) + ' <span>' + (data.odds[j].laySize1 === undefined ? '00' : data.odds[j].laySize1) + '</span></button>';
          goalHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].layPrice2 === undefined ? '0' : data.odds[j].layPrice2) + ' <span>' + (data.odds[j].laySize2 === undefined ? '00' : data.odds[j].laySize2) + '</span></button>';
          goalHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].layPrice3 === undefined ? '0' : data.odds[j].layPrice3) + ' <span>' + (data.odds[j].laySize3 === undefined ? '00' : data.odds[j].laySize3) + '</span></button>';
          if (suspended !== '') {
            goalHtmlData += '<div class="suspended"><span>' + suspended + '</span></div>';
          }
          goalHtmlData += '</div></div>' +
            '<div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + goalData[i].runners[j].minStack + '</b></span><span>max stakes: <b>' + goalData[i].runners[j].maxStack + '</b></span></p></div></div>';
        }
        if (goalData[i].info !== null && goalData[i].info !== 'null' && goalData[i].info !== '') {
          goalHtmlData += '<p class="alert-text">' + goalData[i].info + '</p>';
        }
        goalHtmlData += '</div>';
      }
      this.glRef.nativeElement.innerHTML = goalHtmlData;
    } catch (e) {
      console.log('Failed to bind Goal market', e);
    }
  }

  bindSetMarketData(dataOdd, setmarketData) {
    try {
      if (setmarketData === undefined || setmarketData === null || !setmarketData.length) {
        return false;
      }
      // fadeInUp
      let setMarketHtmlData = '';
      for (let i = 0; i < setmarketData.length; i++) {
        setMarketHtmlData += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">';
        setMarketHtmlData += '<div class="col-md-4 col-7"><p class="market-title">' + setmarketData[i].marketName + '</p></div>';
        setMarketHtmlData += '<div class="col-md-7 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

        for (let j = 0; j < setmarketData[i].runners.length; j++) {

          let data: any = {};
          let tmpOindex = 0;
          if (dataOdd[i].market_id === setmarketData[i].market_id) { // CHECK MARKET ID
            data = dataOdd[i];
            tmpOindex = i;
          } else {
            for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
              if (dataOdd[ii].market_id === setmarketData[i].market_id) {
                data = dataOdd[ii];
                tmpOindex = ii;
                break;       // <=== breaks out of the loop early
              }
            }
          }

          let suspended = '';
          if (setmarketData[i].runners[j].suspended === 1 || data.status === 'CLOSED' || data.status === 'SUSPENDED') { // CHECK SUSPENDED
            suspended = 'suspended';
          }

          let bdata = 0;
          let bcolor = '';

          if (this.GAME_BOOKDATA[setmarketData[i].runners[j].mType + '_' + setmarketData[i].market_id + '_' + setmarketData[i].runners[j].sec_id] !== undefined) {
            bdata = this.GAME_BOOKDATA[setmarketData[i].runners[j].mType + '_' + setmarketData[i].market_id + '_' + setmarketData[i].runners[j].sec_id];
          }

          if (bdata <= -1) {
            bcolor = 'red';
          } else if (bdata >= 1) {
            bcolor = 'green';
          } else {
            bcolor = 'black';
          }

          setMarketHtmlData += '';
          setMarketHtmlData += '<div data-title="' + suspended + '"  id="' + setmarketData[i].runners[j].sec_id + '" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-7">';
          setMarketHtmlData += '<p class="market-name">' + setmarketData[i].runners[j].runner + '<span class="' + bcolor + '">' + bdata + '</span></p></div>';
          setMarketHtmlData += '<div class="col-md-6 col-5"><div class="bl-buttons">';
          setMarketHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].backPrice3 === undefined ? '0' : data.odds[j].backPrice3) + ' <span>' + (data.odds[j].backSize3 === undefined ? '00' : data.odds[j].backSize3) + '</span></button>';
          setMarketHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].backPrice2 === undefined ? '0' : data.odds[j].backPrice2) + ' <span>' + (data.odds[j].backSize2 === undefined ? '00' : data.odds[j].backSize2) + '</span></button>';
          setMarketHtmlData += '<button class="bl-btn back waves-effect waves-light bet-sec" data-rindex="' + j + '" data-index="' + i + '" data-oindex="' + tmpOindex + '" data-marketname="' + setmarketData[i].marketName + '" data-sec="' + setmarketData[i].runners[j].sec_id + '" data-btype="back" data-rate="' + data.odds[j].backPrice1 + '" data-runnerName="' + setmarketData[i].runners[j].runner + '" data-mtype="' + setmarketData[i].runners[j].mType + '"  data-marketId="' + setmarketData[i].market_id + '" data-slug="' + setmarketData[i].slug + '" data-sportId="' + setmarketData[i].sportId + '" data-minstack="' + setmarketData[i].runners[j].minStack + '" data-maxstack="' + setmarketData[i].runners[j].maxStack + '" data-maxprofitlimit="' + setmarketData[i].runners[j].maxProfitLimit + '">' + (data.odds[j].backPrice1 === undefined ? '0' : data.odds[j].backPrice1) + ' <span>' + (data.odds[j].backSize1 === undefined ? '00' : data.odds[j].backSize1) + '</span></button>';
          setMarketHtmlData += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-rindex="' + j + '" data-index="' + i + '" data-oindex="' + tmpOindex + '" data-marketname="' + setmarketData[i].marketName + '" data-sec="' + setmarketData[i].runners[j].sec_id + '" data-btype="lay" data-rate="' + data.odds[j].layPrice1 + '" data-runnerName="' + setmarketData[i].runners[j].runner + '" data-mtype="' + setmarketData[i].runners[j].mType + '"  data-marketId="' + setmarketData[i].market_id + '" data-slug="' + setmarketData[i].slug + '" data-sportId="' + setmarketData[i].sportId + '" data-minstack="' + setmarketData[i].runners[j].minStack + '" data-maxstack="' + setmarketData[i].runners[j].maxStack + '" data-maxprofitlimit="' + setmarketData[i].runners[j].maxProfitLimit + '">' + (data.odds[j].layPrice1 === undefined ? '0' : data.odds[j].layPrice1) + ' <span>' + (data.odds[j].laySize1 === undefined ? '00' : data.odds[j].laySize1) + '</span></button>';
          setMarketHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].layPrice2 === undefined ? '0' : data.odds[j].layPrice2) + ' <span>' + (data.odds[j].laySize2 === undefined ? '00' : data.odds[j].laySize2) + '</span></button>';
          setMarketHtmlData += '<button class="bl-btn eon bet-sec">' + (data.odds[j].layPrice3 === undefined ? '0' : data.odds[j].layPrice3) + ' <span>' + (data.odds[j].laySize3 === undefined ? '00' : data.odds[j].laySize3) + '</span></button>';
          if (suspended !== '') {
            setMarketHtmlData += '<div class="suspended"><span>' + suspended + '</span></div>';
          }
          setMarketHtmlData += '</div></div>' +
            '<div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + setmarketData[i].runners[j].minStack + '</b></span><span>max stakes: <b>' + setmarketData[i].runners[j].maxStack + '</b></span></p></div></div>';
        }
        if (setmarketData[i].info !== null && setmarketData[i].info !== 'null' && setmarketData[i].info !== '') {
          setMarketHtmlData += '<p class="alert-text">' + setmarketData[i].info + '</p>';
        }
        setMarketHtmlData += '</div>';
      }
      this.setMarketRef.nativeElement.innerHTML = setMarketHtmlData;
    } catch (e) {
      console.log('Failed to bind Set Market', e);
    }
  }

  bindBookmakerData(dataOdd, bookmakerData, market) {
    try {
      if (bookmakerData === undefined || bookmakerData === null || !bookmakerData.length) {
        return false;
      }
      // fadeInUp
      let bookmakerHtmlData = '';
      for (let i = 0; i < bookmakerData.length; i++) {

        // bookmakerHtmlData += '<div class="card"><div class="card-body market-ui"><div class="row mx-lg-0"><div class="col-md-6 c-pr-2"><div></div>';
        bookmakerHtmlData += '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">' +
          '<div class="col-md-4 col-7"><p class="market-title">' + bookmakerData[i].marketName + '</p></div>' +
          '<div class="col-md-6 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

        for (let j = 0; j < bookmakerData[i].runners.length; j++) {

          let data: any = {};
          if (dataOdd[i].market_id === bookmakerData[i].market_id) { // CHECK MARKET ID
            data = dataOdd[i];
          } else {
            for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
              if (dataOdd[ii].market_id === bookmakerData[i].market_id) {
                data = dataOdd[ii];
                break;       // <=== breaks out of the loop early
              }
            }
          }

          let suspended = '';
          if (data && data.runners[j] && (data.runners[j].suspended !== 0 || data.runners[j].ballrunning !== 0)) {
            suspended = data.runners[j].suspended === 1 ? 'Suspended' : 'Ball Running';
          }
          let bdata = 0;
          let bcolor = '';
          if (this.GAME_BOOKDATA[bookmakerData[i].runners[j].mType + '_' + bookmakerData[i].runners[j].sec_id] !== undefined) {
            bdata = this.GAME_BOOKDATA[bookmakerData[i].runners[j].mType + '_' + bookmakerData[i].runners[j].sec_id];
          }

          if (bdata <= -1) {
            bcolor = 'red';
          } else if (bdata >= 1) {
            bcolor = 'green';
          } else {
            bcolor = 'black';
          }

          bookmakerHtmlData += '<div data-title="' + suspended + '"  id="' + bookmakerData[i].runners[j].sec_id + '" class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
          bookmakerHtmlData += '<div class="col-md-4 col-7"><p class="market-name">' + bookmakerData[i].runners[j].runner + '<span class="odd-number ' + bcolor + '">' + bdata + '</span></p></div>';
          bookmakerHtmlData += '<div class="col-md-6 text-center col-5"> <div class="bl-buttons">';
          bookmakerHtmlData += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + bookmakerData[i].marketName + '" data-sec="' + bookmakerData[i].runners[j].sec_id + '" data-btype="back" data-rate="' + data.runners[j].back + '" data-runnerName="' + bookmakerData[i].runners[j].runner + '" data-mtype="' + bookmakerData[i].runners[j].mType + '"  data-marketId="' + bookmakerData[i].market_id + '" data-slug="' + bookmakerData[i].slug + '" data-sportId="' + bookmakerData[i].sportId + '" data-minstack="' + bookmakerData[i].runners[j].minStack + '" data-maxstack="' + bookmakerData[i].runners[j].maxStack + '" data-maxprofitlimit="' + bookmakerData[i].runners[j].maxProfitLimit + '">' + (data.runners[j].back === undefined ? '0' : data.runners[j].back) + ' <span>00</span></button>';
          bookmakerHtmlData += '<button class="bl-btn lay waves-effect waves-light bet-sec"  data-index="' + i + '" data-marketname="' + bookmakerData[i].marketName + '" data-sec="' + bookmakerData[i].runners[j].sec_id + '" data-btype="lay" data-rate="' + data.runners[j].lay + '" data-runnerName="' + bookmakerData[i].runners[j].runner + '" data-mtype="' + bookmakerData[i].runners[j].mType + '"  data-marketId="' + bookmakerData[i].market_id + '" data-slug="' + bookmakerData[i].slug + '" data-sportId="' + bookmakerData[i].sportId + '" data-minstack="' + bookmakerData[i].runners[j].minStack + '" data-maxstack="' + bookmakerData[i].runners[j].maxStack + '" data-maxprofitlimit="' + bookmakerData[i].runners[j].maxProfitLimit + '">' + (data.runners[j].lay === undefined ? '0' : data.runners[j].lay) + ' <span>00</span></button>';
          if (suspended !== '') {
            bookmakerHtmlData += '<div class="suspended"> <span>' + suspended + '</span></div> \n';
          }
          bookmakerHtmlData += '</div></div>' +
            '<div class="col-md-2 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + bookmakerData[i].runners[j].minStack + '</b></span><span>max stakes: <b>' + bookmakerData[i].runners[j].maxStack + '</b></span></p></div></div>';
        }
        if (bookmakerData[i].info !== null && bookmakerData[i].info !== 'null' && bookmakerData[i].info !== '') {
          bookmakerHtmlData += '<p class="alert-text">' + bookmakerData[i].info + '</p>';
        }
        bookmakerHtmlData += '</div>';
        // bookmakerHtmlData += '</div>';
      }
      if (market === 'bookmaker') {
        this.bookmakerRef.nativeElement.innerHTML = bookmakerHtmlData;
      } else {
        this.virtualCricketRef.nativeElement.innerHTML = bookmakerHtmlData;
      }
    } catch (e) {
      console.log('Failed to bind ' + market, e);
    }
  }

  bindVirtualCricketData(dataOdd, virtualCData) {
    try {
      if (virtualCData === undefined || virtualCData === null || !virtualCData.length) {
        return false;
      }
      // fadeInUp
      let virtualHtmlData = '';
      for (let i = 0; i < virtualCData.length; i++) {
        virtualHtmlData += '<div class="row animated ' + this.annimationClass + '">\n' +
          '<div class="col-12 mt-2">\n' +
          '<div class="row bg-white default-table align-items-center mx-0">\n' +
          '<div class="col-12">\n' +
          '<div class="row bg-black my-row py-1">\n' +
          '<div class="col-7">\n' +
          '<span class="title-sp">' + virtualCData[i].marketName + '</span>\n' +
          '</div>\n' +
          '<div class="col-5 text-right pr-1 pl-0">\n' +
          '<a class="button-title">back</a>\n' +
          '<a class="button-title">lay</a>\n' +
          // '<button type="button" class="info-detail" data-toggle="modal" data-target="#InfoModal">\n' +
          // '<i class="fa fa-info"></i>\n' +
          // '</button>\n' +
          '</div>\n' +
          '</div>';

        for (let j = 0; j < virtualCData[i].runners.length; j++) {

          let data: any = {};
          if (dataOdd[i].market_id === virtualCData[i].market_id) { // CHECK MARKET ID
            data = dataOdd[i];
          } else {
            for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
              if (dataOdd[ii].market_id === virtualCData[i].market_id) {
                data = dataOdd[ii];
                break;       // <=== breaks out of the loop early
              }
            }
          }

          let suspended = '';
          if (data && data.runners[j] && (data.runners[j].suspended !== 0 || data.runners[j].ballrunning !== 0)) {
            suspended = data.runners[j].suspended === 1 ? 'Suspended' : 'Ball Running';
          }
          let bdata = 0;
          let bcolor = '';
          if (this.GAME_BOOKDATA[virtualCData[i].runners[j].mType + '_' + virtualCData[i].runners[j].sec_id] !== undefined) {
            bdata = this.GAME_BOOKDATA[virtualCData[i].runners[j].mType + '_' + virtualCData[i].runners[j].sec_id];
          }

          if (bdata <= -1) {
            bcolor = 'red';
          } else if (bdata >= 1) {
            bcolor = 'green';
          } else {
            bcolor = 'black';
          }

          let borderClass = '';
          if ((virtualCData[i].runners.length === 2 && j === 0) || (virtualCData[i].runners.length === 3 && j < 2)) {
            borderClass = 'border-bottom';
          }
          // else if (bookmakerData[i].info !== null && bookmakerData[i].info !== 'null' && bookmakerData[i].info !== '') {
          //   borderClass = 'border-bottom';
          // }

          virtualHtmlData += '<div class="row py-1 align-items-center ' + borderClass + '"  data-title="' + suspended + '"  id="' + virtualCData[i].runners[j].sec_id + '">\n' +
            '<div class="col-7 text-capitalize">\n' +
            '<span class="odd-text">' + virtualCData[i].runners[j].runner + '</span> <span class="odd-number ' + bcolor + '">' + bdata + '</span>\n' +
            '</div>\n' +
            '<div class="col-5 text-right pr-1 pl-0">\n' +
            '<div class="suspended-div">\n' +
            '<button class="inner-blue-area inner-btn bet-sec" data-index="' + i + '" data-marketname="' + virtualCData[i].marketName + '" data-sec="' + virtualCData[i].runners[j].sec_id + '" data-btype="back" data-rate="' + data.runners[j].back + '" data-runnerName="' + virtualCData[i].runners[j].runner + '" data-mtype="' + virtualCData[i].runners[j].mType + '"  data-marketId="' + virtualCData[i].market_id + '" data-slug="' + virtualCData[i].slug + '" data-sportId="' + virtualCData[i].sportId + '" data-minstack="' + virtualCData[i].runners[j].minStack + '" data-maxstack="' + virtualCData[i].runners[j].maxStack + '" data-maxprofitlimit="' + virtualCData[i].runners[j].maxProfitLimit + '">\n' +
            '<h3>' + (data.runners[j].back === undefined ? '0' : data.runners[j].back) + ' <span>00</span></h3>\n' +
            '</button>\n' +
            '<button class="inner-red-area inner-btn bet-sec" data-index="' + i + '" data-marketname="' + virtualCData[i].marketName + '" data-sec="' + virtualCData[i].runners[j].sec_id + '" data-btype="lay" data-rate="' + data.runners[j].lay + '" data-runnerName="' + virtualCData[i].runners[j].runner + '" data-mtype="' + virtualCData[i].runners[j].mType + '"  data-marketId="' + virtualCData[i].market_id + '" data-slug="' + virtualCData[i].slug + '" data-sportId="' + virtualCData[i].sportId + '" data-minstack="' + virtualCData[i].runners[j].minStack + '" data-maxstack="' + virtualCData[i].runners[j].maxStack + '" data-maxprofitlimit="' + virtualCData[i].runners[j].maxProfitLimit + '">\n' +
            '<h3>' + (data.runners[j].lay === undefined ? '0' : data.runners[j].lay) + ' <span>00</span></h3></button>';
          if (suspended !== '') {
            virtualHtmlData += '<div class="suspended-text">' + suspended + '</div> \n';
          }
          virtualHtmlData += '</div></div></div>';
        }
        if (virtualCData[i].info !== null && virtualCData[i].info !== 'null' && virtualCData[i].info !== '') {
          virtualHtmlData += '<div class="col-12 line-h-0 mt-0 px-0"><p class="urgent-text red">' + virtualCData[i].info + '</p></div>';
        }
        virtualHtmlData += '</div>';
        virtualHtmlData += '</div></div></div></div>';
      }
      this.virtualCricketRef.nativeElement.innerHTML = virtualHtmlData;
    } catch (e) {
      console.log('Failed to bind bookmaker', e);
    }
  }

  bindFancyData(dataOdd, fancy) {
    try {
      if (fancy === null) {
        return false;
      }
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }

      let fancyDataHtml = '';
      fancyDataHtml = '<div class="market-section">\n' +
        '<div class="row align-items-center text-uppercase text-dark market-title-row">\n' +
        '<div class="col-md-4 col-7"><p class="market-title">FANCY</p></div>\n' +
        '<div class="col-md-6 text-center col-5">\n' +
        '<p class="market-back"><span>no</span> <span>yes</span></p>\n' +
        '</div></div>';

      for (let i = 0; i < fancy.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy[i].marketId) {
              data = dataOdd[ii];
              break;       // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-btn';
        let bookActiveStatus = '';
        if (fancy[i].is_book === '0') {
          bkClass = 'unbook-btn';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancyDataHtml += '<div class="row py-1 border-bottom bg-white">\n' +
          '<div class="col-5 text-capitalize align-self-center">\n' +
          '<p class="fancy-name"><span>' + fancy[i].title + '</span></p>\n' +
          '</div>\n' +
          '<div class="col-2 p-0 align-self-center"><button type="button" class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-title="' + fancy[i].title + '"data-template="template" data-marketid="' + data.market_id + '" data-mtype="' + fancy[i].mType + '">Book</button></div>\n' +
          '<div class="col-5 text-right align-self-center pr-1 pl-0">\n' +
          '<div class="suspended-div">\n' +
          '<button class="inner-red-area inner-btn bet-sec"  data-index="' + i + '"  data-marketname="' + fancy[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy[i].title + '" data-rate="' + data.data.no + '" data-mtype="' + fancy[i].mType + '" data-rrate="' + data.data.no_rate + '" data-slug="' + fancy[i].slug + '"  data-sportId="' + fancy[i].sportId + '" data-minstack="' + fancy[i].minStack + '"   data-maxstack="' + fancy[i].maxStack + '"   data-maxprofitlimit="' + fancy[i].maxProfitLimit + '">\n' +
          '<h3>' + data.data.no + ' <span>' + data.data.no_rate + '</span></h3>\n' +
          '</button>\n' +
          '<button class="inner-blue-area inner-btn bet-sec" data-index="' + i + '"   data-marketname="' + fancy[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy[i].title + '" data-rate="' + data.data.yes + '" data-mtype="' + fancy[i].mType + '" data-rrate="' + data.data.yes_rate + '" data-slug="' + fancy[i].slug + '"  data-sportId="' + fancy[i].sportId + '" data-minstack="' + fancy[i].minStack + '"   data-maxstack="' + fancy[i].maxStack + '"   data-maxprofitlimit="' + fancy[i].maxProfitLimit + '">\n' +
          '<h3>' + data.data.yes + '<span> ' + data.data.yes_rate + '</span></h3>\n' +
          '</button>';
        if (data.suspended === 1 || data.ballRunning === 1) {
          fancyDataHtml += '<div class="suspended-text">' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</div>';
        }
        fancyDataHtml += '</div></div>';
        if (fancy[i].info) {
          fancyDataHtml += '<div class="col-12 line-h-0 mt-0">';
          fancyDataHtml += '<p class="urgent-text red">' + fancy[i].info + '</p>';
          fancyDataHtml += '</div>';
        }
        fancyDataHtml += '</div>';

        // fancyDataHtml += '</div></div></div>';
      }
      fancyDataHtml += '</div></div></div></div>';
      this.fancyRef.nativeElement.innerHTML = fancyDataHtml;
    } catch (e) {
      console.log('Failed to bind Fancy', e);
    }
  }

  bindFancy2Data(dataOdd, fancy2, market) {
    try {
      if (fancy2 === undefined || fancy2 === null) {
        return false;
      }
      if (dataOdd === undefined || dataOdd.length === 0 || fancy2 === null) {
        return false;
      }
      const title = market === 'fancy' ? 'fancy' : 'session';
      let fancy2DataHtml = '';
      fancy2DataHtml = '<div class="market-section">\n' +
        '<div class="row align-items-center text-uppercase text-dark market-title-row">\n' +
        '<div class="col-md-4 col-7"><p class="market-title">' + title + '</p></div>\n' +
        '<div class="col-md-6 text-center col-5">\n' +
        '<p class="market-back"><span>no</span> <span>yes</span></p>\n' +
        '</div></div>';

      for (let i = 0; i < fancy2.length; i++) {
        let data: any = {};
        if (dataOdd && dataOdd[i] && dataOdd[i].market_id === fancy2[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          let ii = 0;
          for (const oddsDataItem of dataOdd) { // CHECK MARKET ID
            if (oddsDataItem.market_id === fancy2[i].marketId) {
              data = oddsDataItem;
              break; // <=== breaks out of the loop early
            }
            ii++;
          }
        }

        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy2[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy2DataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">\n' +
          '<div class="col-md-4 col-5"><p class="market-name">' + fancy2[i].title + '</p></div>';
        fancy2DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail"  ' + bookActiveStatus + ' data-title="' + fancy2[i].title + '"  data-marketid="' + data.market_id + '" data-mtype="' + fancy2[i].mType + '">book</button></div>';
        fancy2DataHtml += '<div class="col-md-4 text-center col-5"><div class="bl-buttons">';
        fancy2DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec"  data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.no + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.no + ' <span>' + data.no_rate + '</span></button>';
        fancy2DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.yes + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy2DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</span></div>';
        }
        fancy2DataHtml += '</div></div>' +
          '<div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + fancy2[i].minStack + '</b></span><span>max stakes: <b>' + fancy2[i].maxStack + '</b></span></p></div></div>';
        if (fancy2[i].info !== null && fancy2[i].info !== 'null' && fancy2[i].info !== '' && fancy2[i].info !== ' ') {
          fancy2DataHtml += '<p class="alert-text">' + fancy2[i].info + '</p>';
        }
      }
      fancy2DataHtml += '</div>';
      if (market === 'fancy') {
        this.fancyRef.nativeElement.innerHTML = fancy2DataHtml;
      } else {
        this.fancy2Ref.nativeElement.innerHTML = fancy2DataHtml;
      }
    } catch (e) {
      console.log('Failed to bind ' + market, e);
    }
  }

  bindFancy3Data(dataOdd, fancy3) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let fancy3DataHtml = '';
      fancy3DataHtml += '<div class="market-section"><div class="row mx-lg-0 align-items-center text-uppercase text-dark market-title-row">';
      fancy3DataHtml += '<div class="col-md-5 col-7"><p class="market-title">other market</p></div>';
      fancy3DataHtml += '<div class="col-md-4 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < fancy3.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy3[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy3[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy3[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        fancy3DataHtml += '<div class="row mx-lg-0 my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        fancy3DataHtml += '<p class="market-name">' + fancy3[i].title + '</p></div>';
        fancy3DataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-title="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-mtype="' + fancy3[i].mType + '">book</button></div>';
        fancy3DataHtml += '<div class="col-md-4 col-5"><div class="bl-buttons">';
        fancy3DataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="back" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.back + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.back + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.back + '<span> 00</span></button>';
        fancy3DataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy3[i].title + '" data-marketid="' + data.market_id + '" data-btype="lay" data-runnerName="' + fancy3[i].title + '" data-rate="' + data.lay + '" data-mtype="' + fancy3[i].mType + '" data-rrate="' + data.lay + '" data-slug="' + fancy3[i].slug + '"  data-sportId="' + fancy3[i].sportId + '" data-minstack="' + fancy3[i].minStack + '"   data-maxstack="' + fancy3[i].maxStack + '"   data-maxprofitlimit="' + fancy3[i].maxProfitLimit + '">' + data.lay + ' <span>00</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          fancy3DataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        fancy3DataHtml += '</div>' +
          '</div><div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + fancy3[i].minStack + '</b></span><span>max stakes: <b>' + fancy3[i].maxStack + '</b></span></p></div>';
        if (fancy3[i].info) {
          fancy3DataHtml += '<div class="col-12"><p class="alert-text">' + fancy3[i].info + '</p></div>';
        }
        fancy3DataHtml += '</div>';
      }
      fancy3DataHtml += '</div>';
      if (this.matchData.sport_id === 4) {
        this.fancy3Ref.nativeElement.innerHTML = fancy3DataHtml;
      } else {
        this.fancy31Ref.nativeElement.innerHTML = fancy3DataHtml;
      }
    } catch (e) {
      console.log('Failed to bind Fancy3', e);
    }
  }

  bindMeterData(dataOdd, fancy2) {
    try {
      if (fancy2 === undefined || fancy2 === null) {
        return false;
      }
      if (dataOdd === undefined || dataOdd.length === 0 || fancy2 === null) {
        return false;
      }

      let meterDataHtml = '';
      meterDataHtml = '<div class="market-section"><div class="row mx-lg-0 align-items-center text-uppercase text-dark market-title-row">';
      meterDataHtml += '<div class="col-md-5 col-7"><p class="market-title">Meter</p></div>';
      meterDataHtml += '<div class="col-md-4 col-5 text-center"><p class="market-back"><span>No</span> <span>Yes</span></p></div></div>';

      for (let i = 0; i < fancy2.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === fancy2[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === fancy2[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }

        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (fancy2[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }
        meterDataHtml += '<div class="row mx-lg-0 my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        meterDataHtml += '<p class="market-name">' + fancy2[i].title + '</p></div>';
        meterDataHtml += '<div class="col-md-1 col-2 text-center px-0"></div>';
        meterDataHtml += '<div class="col-md-4 col-5"><div class="bl-buttons">';
        meterDataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.no + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.no + ' <span>' + data.no_rate + '</span></button>';
        meterDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + fancy2[i].title + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + fancy2[i].title + '" data-rate="' + data.yes + '" data-mtype="' + fancy2[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + fancy2[i].slug + '"  data-sportId="' + fancy2[i].sportId + '"  data-minstack="' + fancy2[i].minStack + '"   data-maxstack="' + fancy2[i].maxStack + '"   data-maxprofitlimit="' + fancy2[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          meterDataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'Suspended' : 'Ball Running') + '</span></div>';
        }
        meterDataHtml += '</div>' +
          '</div><div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + fancy2[i].minStack + '</b></span><span>max stakes: <b>' + fancy2[i].maxStack + '</b></span></p></div>';
        if (fancy2[i].info) {
          meterDataHtml += '<div class="col-12"><p class="alert-text">' + fancy2[i].info + '</p></div>';
        }
        meterDataHtml += '</div>';

      }
      meterDataHtml += '</div>';
      this.meterRef.nativeElement.innerHTML = meterDataHtml;
    } catch (e) {
      console.log('Failed to bind METER', e);
    }
  }

  bindOddEvenData(dataOdd, oddeven) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let oddevenDataHtml = '';
      oddevenDataHtml += ' <div class="market-section"><div class="row mx-lg-0 align-items-center text-uppercase text-dark market-title-row">';
      oddevenDataHtml += '<div class="col-md-5 col-7"><p class="market-title">ODD EVEN market</p></div>';
      oddevenDataHtml += '<div class="col-md-4 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';


      for (let i = 0; i < oddeven.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === oddeven[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === oddeven[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (oddeven[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        oddevenDataHtml += '<div class="row mx-lg-0 my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        oddevenDataHtml += '<p class="market-name">' + oddeven[i].title + '</p></div>';
        oddevenDataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-title="' + oddeven[i].title + '" data-marketid="' + data.market_id + '" data-mtype="' + oddeven[i].mType + '">book</button></div>';
        oddevenDataHtml += '<div class="col-md-4 col-5"><div class="bl-buttons">';
        oddevenDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + oddeven[i].title + '" data-marketid="' + data.market_id + '" data-btype="back" data-runnerName="' + oddeven[i].title + '" data-rate="' + data.odd + '" data-mtype="' + oddeven[i].mType + '" data-rrate="' + data.odd + '" data-slug="' + oddeven[i].slug + '"  data-sportId="' + oddeven[i].sportId + '" data-minstack="' + oddeven[i].minStack + '"   data-maxstack="' + oddeven[i].maxStack + '"   data-maxprofitlimit="' + oddeven[i].maxProfitLimit + '">' + data.odd + '<span> 00</span></button>';
        oddevenDataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + oddeven[i].title + '" data-marketid="' + data.market_id + '" data-btype="lay" data-runnerName="' + oddeven[i].title + '" data-rate="' + data.even + '" data-mtype="' + oddeven[i].mType + '" data-rrate="' + data.even + '" data-slug="' + oddeven[i].slug + '"  data-sportId="' + oddeven[i].sportId + '" data-minstack="' + oddeven[i].minStack + '"   data-maxstack="' + oddeven[i].maxStack + '"   data-maxprofitlimit="' + oddeven[i].maxProfitLimit + '">' + data.even + ' <span>00</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          oddevenDataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        oddevenDataHtml += '</div></div>' +
          '<div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + oddeven[i].minStack + '</b></span><span>max stakes: <b>' + oddeven[i].maxStack + '</b></span></p></div>';
        if (oddeven[i].info) {
          oddevenDataHtml += '<div class="col-12"><p class="alert-text">' + oddeven[i].info + '</p></div>';
        }
        oddevenDataHtml += '</div>';
      }
      oddevenDataHtml += '</div>';
      this.oddevenRef.nativeElement.innerHTML = oddevenDataHtml;
    } catch (e) {
      console.log('Failed to bind odd event', e);
    }
  }

  bindKhadoData(dataOdd, khado) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let khadoDataHtml = '';
      khadoDataHtml = '<div class="market-section"><div class="row mx-lg-0 align-items-center text-uppercase text-dark market-title-row">';
      khadoDataHtml += '<div class="col-md-5 col-7"><p class="market-title">Khado market</p></div>';
      khadoDataHtml += ' <div class="col-md-4 col-5 text-center"><p class="market-back"><span>back</span> <span>lay</span></p></div></div>';

      for (let i = 0; i < khado.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === khado[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === khado[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (khado[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        khadoDataHtml += '<div class="row mx-lg-0 my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row"><div class="col-md-4 col-5">';
        khadoDataHtml += '<p class="market-name">' + khado[i].title + '<span class="khado-number">' + data.difference + '</span></p></div>';
        khadoDataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + '  data-title="' + khado[i].title + '-' + data.difference + '" data-marketid="' + data.market_id + '" data-mtype="' + khado[i].mType + '">book</button></div>';
        khadoDataHtml += '<div class="col-md-4 col-5"><div class="bl-buttons">';
        khadoDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + khado[i].title + '-' + data.difference + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + khado[i].title + '-' + data.difference + '" data-rate="' + data.yes + '" data-mtype="' + khado[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + khado[i].slug + '"  data-sportId="' + khado[i].sportId + '" data-minstack="' + khado[i].minStack + '"   data-maxstack="' + khado[i].maxStack + '"   data-maxprofitlimit="' + khado[i].maxProfitLimit + '">' + data.yes + '<span> ' + data.yes_rate + '</span></button>';
        khadoDataHtml += '<button class="bl-btn lay waves-effect waves-light" data-index="' + i + '">- <span>--</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          khadoDataHtml += '<div class="suspended"><span></span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        khadoDataHtml += '</div></div>' +
          '<div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b>' + khado[i].minStack + '</b></span><span>max stakes: <b>' + khado[i].maxStack + '</b></span></p></div>';
        if (khado[i].info) {
          khadoDataHtml += '<div class="col-12"><p class="alert-text">' + khado[i].info + '</p></div>';
        }
        khadoDataHtml += '</div>';
      }
      khadoDataHtml += '</div>';
      this.khadoRef.nativeElement.innerHTML = khadoDataHtml;
    } catch (e) {
      console.log('Failed to bind Khado session', e);
    }
  }

  bindBallByBallData(dataOdd, ballbyball) {
    try {
      if (dataOdd === undefined || dataOdd.length === 0) {
        return false;
      }
      let bbbDataHtml = '';
      bbbDataHtml = '<div class="market-section"><div class="row align-items-center text-uppercase text-dark market-title-row">';
      bbbDataHtml += '<div class="col-md-4 border-0 col-7"><p class="market-title">Ball By Ball</p></div>';
      bbbDataHtml += ' <div class="col-md-6 text-center col-5"><p class="market-back"><span>no</span> <span>yes</span></p></div></div>';

      for (let i = 0; i < ballbyball.length; i++) {
        let data: any = {};
        if (dataOdd[i].market_id === ballbyball[i].marketId) { // CHECK MARKET ID
          data = dataOdd[i];
        } else {
          for (let ii = 0; ii < dataOdd.length; ii++) { // CHECK MARKET ID
            if (dataOdd[ii].market_id === ballbyball[i].marketId) {
              data = dataOdd[ii];
              break; // <=== breaks out of the loop early
            }
          }
        }
        let bkClass = 'book-fill';
        let bookActiveStatus = '';
        if (ballbyball[i].is_book === '0') {
          bkClass = 'book';
          bookActiveStatus = 'style="pointer-events: none;cursor: wait;"';
        }

        bbbDataHtml += '<div class="row my-1 py-1 align-items-center text-capitalize text-dark bg-theme market-data-row">';
        bbbDataHtml += '<div class="col-md-4 col-5"><p class="market-name"><span class="mn-run">' + ballbyball[i].ball + '</span> ' + ballbyball[i].title + '</p></div>';
        bbbDataHtml += '<div class="col-md-1 col-2 text-center px-0"><button class="' + bkClass + ' bookModalDetail" ' + bookActiveStatus + ' data-title="' + ballbyball[i].title + '" data-marketid="' + data.market_id + '" data-mtype="' + ballbyball[i].mType + '">book</button></div>';
        bbbDataHtml += '<div class="col-md-4 col-5 text-center"><div class="bl-buttons">';
        bbbDataHtml += '<button class="bl-btn lay waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + ballbyball[i].title + '-' + ballbyball[i].ball + '" data-marketid="' + data.market_id + '" data-btype="no" data-runnerName="' + ballbyball[i].title + '-' + ballbyball[i].ball + '" data-rate="' + data.no + '" data-mtype="' + ballbyball[i].mType + '" data-rrate="' + data.no_rate + '" data-slug="' + ballbyball[i].slug + '"  data-sportId="' + ballbyball[i].sportId + '"  data-minstack="' + ballbyball[i].minStack + '"   data-maxstack="' + ballbyball[i].maxStack + '"   data-maxprofitlimit="' + ballbyball[i].maxProfitLimit + '">' + data.no + '<span> ' + data.no_rate + '</span></button>';
        bbbDataHtml += '<button class="bl-btn back waves-effect waves-light bet-sec" data-index="' + i + '" data-marketname="' + ballbyball[i].title + '-' + ballbyball[i].ball + '" data-marketid="' + data.market_id + '" data-btype="yes" data-runnerName="' + ballbyball[i].title + '-' + ballbyball[i].ball + '" data-rate="' + data.yes + '" data-mtype="' + ballbyball[i].mType + '" data-rrate="' + data.yes_rate + '" data-slug="' + ballbyball[i].slug + '"  data-sportId="' + ballbyball[i].sportId + '"  data-minstack="' + ballbyball[i].minStack + '"   data-maxstack="' + ballbyball[i].maxStack + '"   data-maxprofitlimit="' + ballbyball[i].maxProfitLimit + '">' + data.yes + ' <span>' + data.yes_rate + '</span></button>';
        if (data.suspended === 1 || data.ball_running === 1) {
          bbbDataHtml += '<div class="suspended"><span>' + (data.suspended === 1 ? 'suspended' : 'Ball Running') + '</span></div>';
        }
        bbbDataHtml += '</div></div> <div class="col-md-3 d-none d-sm-block"><p class="min-max-detail"><span>min stakes: <b> ' + ballbyball[i].minStack + ' </b></span><span>max stakes: <b>' + ballbyball[i].maxStack + '</b></span></p></div>';
        if (ballbyball[i].info && ballbyball[i].info !== 'null') {
          bbbDataHtml += '<div class="col-12"><p class="alert-text">' + ballbyball[i].info + '</p></div>';
        }
        bbbDataHtml += '</div>';
      }
      bbbDataHtml += '</div>';
      this.ballbyballRef.nativeElement.innerHTML = bbbDataHtml;
    } catch (e) {
      console.log('Failed to bind BallByBallData session', e);
    }
  }

  _placebet(e) {
    if (e.rate !== undefined && e.rate !== '0' && e.rate !== '-') {
      this.isActivePlaceBet = true;
      this.isActivePlaceBetIndex = e.index;
      this.isActivePlaceBetArrIndx = e.oindex;

      if (this.isActivePlaceBet && this.isActivePlaceBetSType !== undefined && (e.mtype === 'completed_match' || e.mtype === 'tied_match' || e.mtype === 'winner' || e.mtype === 'goals' || e.mtype === 'set_market' || e.mtype === 'match_odd')) { // BET SHEET
        this.isActivePlaceBetSType = (e.mtype === 'completed_match' || e.mtype === 'tied_match' || e.mtype === 'winner' || e.mtype === 'goals' || e.mtype === 'set_market' || e.mtype === 'match_odd') ? 'match_odd' : e.mtype;
        this.isActivePlaceBetMarket = e.mtype;
        const tmp = this.oddsData[this.isActivePlaceBetSType][e.oindex];
        if (e.mtype === 'winner') {
          tmp.suspended = this.matchData.winner.runners[e.index].suspended;
        } else if (e.mtype === 'goals' || e.mtype === 'set_market') {
          // debugger;
          tmp.suspended = this.matchData[e.mtype][e.index].runners[e.rindex].suspended;
        } else {
          tmp.suspended = this.matchData[e.mtype].runners[e.rindex].suspended;
        }
        this.commonService.setMarketChange(tmp);
      } else {
        this.isActivePlaceBetSType = e.mtype;
        // this.commonService.setMarketChange(this.oddsData[this.isActivePlaceBetSType][this.isActivePlaceBetIndex]);
      }

      let secId = e.sec;
      if (e.mtype === 'fancy' || e.mtype === 'fancy2' || e.mtype === 'fancy3' || e.mtype === 'meter' || e.mtype === 'oddeven' || e.mtype === 'khado' || e.mtype === 'ballbyball') {
        secId = e.marketid;
      }

      const initBet = {
        price: e.rate,
        size: '',
        runner: e.runnername,
        bet_type: e.btype,
        sec_id: secId,
        market_id: e.marketid,
        event_id: this.eventId,
        m_type: e.mtype,
        eventname: this.eventTitle,
        market_name: e.marketname,
        slug: e.slug,
        rate: e.rrate ? e.rrate : e.rate,
        sport_id: e.sportid
      };
      const plArr: any = [];
      if (e.mtype === 'match_odd' || e.mtype === 'completed_match' || e.mtype === 'tied_match' || e.mtype === 'winner') {
        for (let ii = 0; ii < this.matchData[e.mtype].runners.length; ii++) {
          const rData = {
            runner: this.matchData[e.mtype].runners[ii].runnerName,
            secId: this.matchData[e.mtype].runners[ii].selectionId,
            profit_loss: this.GAME_BOOKDATA[e.mtype + '_' + this.matchData[e.mtype].runners[ii].selectionId] ? this.GAME_BOOKDATA[e.mtype + '_' + this.matchData[e.mtype].runners[ii].selectionId] : 0,
          };
          plArr.push(rData);
        }
      }
      if (e.mtype === 'bookmaker' || e.mtype === 'virtual_cricket' || e.mtype === 'goals' || e.mtype === 'set_market') {
        // for (let i = 0; i < this.matchData[e.mtype].length; i++) {

        for (let ii = 0; ii < this.matchData[e.mtype][e.index].runners.length; ii++) {
          let pl = this.GAME_BOOKDATA[e.mtype + '_' + this.matchData[e.mtype][e.index].runners[ii].sec_id] ? this.GAME_BOOKDATA[e.mtype + '_' + this.matchData[e.mtype][e.index].runners[ii].sec_id] : 0;
          if (e.mtype === 'goals' || e.mtype === 'set_market') {
            pl = this.GAME_BOOKDATA[e.mtype + '_' + e.marketid + '_' + this.matchData[e.mtype][e.index].runners[ii].sec_id] ? this.GAME_BOOKDATA[e.mtype + '_' + e.marketid + '_' + this.matchData[e.mtype][e.index].runners[ii].sec_id] : 0;
            // pl = this.GAME_BOOKDATA[e.mtype + '_' + e.marketid + '_' + secId] ? this.GAME_BOOKDATA[e.mtype + '_' + e.marketid + '_' + secId] : 0;
          }
          const rData = {
            runner: this.matchData[e.mtype][e.index].runners[ii].runner,
            secId: this.matchData[e.mtype][e.index].runners[ii].sec_id,
            profit_loss: pl,
          };
          plArr.push(rData);
        }
        // }
      }
      const plLimit = {minStack: e.minstack, maxStack: e.maxstack, maxProfitLimit: e.maxprofitlimit};
      const sheetRef = this.placeBetSheet.open(BetplaceComponent, {
        disableClose: true,
        data: {int: initBet, market: plArr, limit: plLimit, index: e.index}
      });
      //
      // sheetRef.afterDismissed().subscribe(data => {
      //   if (data && data.message === 'Cancel') {
      //     alert('Cancel was clicked in bottomsheet');
      //   }
      //   if (data && data.message === 'Status') {
      //     alert('Change Status was clicked in bottomsheet');
      //   }
      // });
    } else {
      console.log('Rate zero bet can not place..');
    }
  }

  getProfitLoss() {
    const data = {
      event_id: this.eventId
    };
    this.service.getPLMatchoddANDBookmaker(data).subscribe((res) => {
      if (res.status === 1 && res.data !== null) {
        res.data.match_odd.forEach((item) => {
          if (this.GAME_BOOKDATA['match_odd_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['match_odd_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['match_odd_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
        });
        res.data.completed_match.forEach((item) => {
          if (this.GAME_BOOKDATA['completed_match_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['completed_match_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['completed_match_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
        });
        res.data.tied_match.forEach((item) => {
          if (this.GAME_BOOKDATA['tied_match_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['tied_match_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['tied_match_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
        });
        res.data.bookmaker.forEach((item) => {
          if (this.GAME_BOOKDATA['bookmaker_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['bookmaker_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['bookmaker_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
        });
        res.data.virtual_cricket.forEach((item) => {
          if (this.GAME_BOOKDATA['virtual_cricket_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['virtual_cricket_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['virtual_cricket_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
        });
        res.data.winner.forEach((item) => {
          if (this.GAME_BOOKDATA['winner_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['winner_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['winner_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
          // console.log(this.GAME_BOOKDATA);
        });
        res.data.goals.forEach((item) => {
          if (this.GAME_BOOKDATA['goals_' + item.marketId + '_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['goals_' + item.marketId + '_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['goals_' + item.marketId + '_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
        });

        res.data.set_market.forEach((item) => {
          if (this.GAME_BOOKDATA['set_market_' + item.marketId + '_' + item.secId] !== undefined) {
            this.GAME_BOOKDATA['set_market_' + item.marketId + '_' + item.secId] = parseInt(item.profit_loss);
          } else {
            this.GAME_BOOKDATA['set_market_' + item.marketId + '_' + item.secId] = item.profit_loss === null ? 0 : parseInt(item.profit_loss);
          }
        });
        this.changedRef.detectChanges();
      }
    });
  }

  /**
   * DELETE BET FROM UN-MATCH LIST
   * @param id bet id
   */
  deleteUnmatchBet(ids: number) {
    const data = {id: ids};
    this.service.deleteUnmatchedBet(data).subscribe((response: any) => {
      if (response.status === 1) {
        this.alert.success(response.success.message);
        this.commonService.CallBalance.emit(1);
      }
    });
  }

  // **Fancy two profit loss******************************/
  getFancyTwoPL(e) {
    this.isLoadFancyPl = false;
    $('#Book-Unbook').modal('show');
    // if (e.is_book.toLocaleString() === '1') {
    $('#session-title').html('Book <i class="mdi mdi-arrow-right"></i>' + e.title);
    this.isFancy3 = (e.mtype === 'fancy3' || e.mtype === 'oddeven') ? true : false;
    this.fancy2PL = [];
    this.fancyBets = [];
    if (e.marketid) {
      const data = {event_id: this.eventId, market_id: e.marketid, session_type: e.mtype};
      this.service.getBetList(data).subscribe((response: any) => {
        if (response.status === 1) {
          if (!this.isFancy3 && data.session_type !== 'khado' && data.session_type !== 'fancy3' && data.session_type !== 'oddeven') {
            const plData = this.fancyBookCal(response.data.betList);
            plData.forEach(item => {
              this.fancy2PL.push(item);
            });
          } else if (!this.isFancy3 && data.session_type === 'khado') {
            const plData = this.getProfitLossKhado(response.data.betList);
            plData.forEach(item => {
              this.fancy2PL.push(item);
            });
          } else {
            const plData = this.getProfitLossOtherMarket(response.data.betList);
            console.log(plData);
            this.fancy2PL = plData;
          }
          // if (!this.isFancy3) {
          //     response.data.profit_loss.forEach(item => {
          //         this.fancy2PL.push(item);
          //     });
          // } else {
          //     this.fancy2PL = response.data.profit_loss;
          // }
          this.fancyBets = response.data.betList;
        }
        this.isLoadFancyPl = true;
      });
      // }
    }
  }

  // **BETLIST**************************/
  getBetList() {
    this.service.getMatchedUnmatchedBet(this.eventId).subscribe((res) => {
      if (res.status === 1 && res.data !== null && res.data.matched && res.data.unmatched) {
        this.currantBets = res.data;
        this.betlistLoading = true;
        $('#Matched-Unmatched').modal('show');
      } else {
        this.currantBets = [];
        this.alert.error('No Data found');
      }
      this.betlistLoading = false;
      this.changedRef.detectChanges();
    });
  }

}
