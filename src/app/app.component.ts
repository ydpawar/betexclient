import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Renderer2,
  ViewChild,
  ElementRef,
  HostListener
} from '@angular/core';
import {AuthService, EnvService} from './core/services';
import {Router} from '@angular/router';
import { fromEvent, Observable, Subscription } from 'rxjs';

declare var window, InstallTrigger;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'MPCEXCH';
  public isDevActive: boolean = false;
  public isMobile: boolean = false;
  public isActiveSYS: number = 1;
  public systemInActiveMsg: string;
  public onlineEvent: Observable<Event>;
  public offlineEvent: Observable<Event>;
  public subscriptions: Subscription[] = [];
  public noInternet: boolean;
  public connectionStatus: string;
  @ViewChild('bodyEle', {static: true}) myCkechName: ElementRef;

  constructor(
    private renderer: Renderer2,
    private env: EnvService,
    private auth: AuthService,
    private router: Router,
    private elRef: ElementRef,
    private detectChange: ChangeDetectorRef
  ) {
    this.noInternet = false;
    this.onResize();
    this.setupSYS();
    this.getSiteMantanance();
  }

  ngOnInit() {
    this.renderer.setAttribute(document.body, 'data-sidebar-color', 'dark');
    this.detectMobil();
    this.checkInternet();
  }

  ngAfterViewInit() {
    // this.versionCheckService.clearCatch();
    if (this.env.ISENV === 2 && !this.isMobile) {
      const isFirefox = typeof InstallTrigger !== 'undefined';
      console.log(isFirefox);
      if (isFirefox) {
        this.nwDevTools();
        this.devBlockScript();
      } else {
        this.testDev();
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  checkInternet() {
    this.onlineEvent = fromEvent(window, 'online');
    this.offlineEvent = fromEvent(window, 'offline');
    this.subscriptions.push(this.onlineEvent.subscribe(event => {
      this.connectionStatus = 'online';
      console.log('online');
      location.reload();
    }));
    this.subscriptions.push(this.offlineEvent.subscribe(e => {
      this.noInternet = true;
      this.connectionStatus = 'offline';
      console.log('offline');
      this.detectChange.detectChanges();
    }));
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.setPlatfrom();
  }

  setPlatfrom() {
    if (window.innerWidth < 992) {
      this.renderer.setAttribute(document.body, 'data-sidebar-size', 'default');
      this.renderer.setAttribute(document.body, 'data-platform', 'mobile');
    } else {
      this.renderer.setAttribute(document.body, 'data-platform', 'desktop');
    }
  }

  setupSYS() {
    const sys = this.env.themeDefaultSetting(this.env.CURRANT_THEME);
    document.getElementsByTagName('title')[0].text = sys.systemname;
    document.getElementsByTagName('link')[0].href = sys.favIcon;
    const logoClass = this.elRef.nativeElement.querySelectorAll('.syslogo'); // LOGO
    for (let i = 0; i < logoClass.length; i++) { // BET PLACE CLICK EVENT HANDEL
      this.renderer.setAttribute(logoClass[i], 'src', sys.logo);
    }
  }

  logout() {
    this.auth.logout().subscribe((response: any) => {
      if (response.status === 1) {
        localStorage.clear();
        sessionStorage.clear();
        document.cookie.split(';').forEach((c) => {
          document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
        });
        this.router.navigate(['/auth']);
      }
    });
  }

  getSiteMantanance() {
    this.auth.getSiteMode().subscribe((res) => {
      if (res.status === 1) {
        this.isActiveSYS = Number(res.data.siteMode);
        this.systemInActiveMsg = res.data.message;
      }
      // this.cdr.detectChanges();
    });
  }

  fnT(e1, t1) {
    window.devtools = {isOpen: e1, orientation: t1};
    window.dispatchEvent(new CustomEvent('devtoolschange', {detail: {isOpen: e1, orientation: t1}}));
  }

  nwDevTools() {
    const e: any = {isOpen: !1, orientation: void 0};
    setInterval(() => {
      const n = window.outerWidth - window.innerWidth > 160, i = window.outerHeight - window.innerHeight > 160,
        r = n ? 'vertical' : 'horizontal';
      i && n || !(window.Firebug && window.Firebug.chrome && window.Firebug.chrome.isInitialized || n || i) ?
        (e.isOpen && this.fnT(!1, void 0), e.isOpen = !1, e.orientation = void 0) :
        (e.isOpen && e.orientation === r || this.fnT(!0, r), e.isOpen = !0, e.orientation = r);

    }, 500);
    window.devtools = e;
  }

  testDev() {
    document.oncontextmenu = (e) => {
      return !1;
    };
    const element: any = new Image();
    this.isDevActive = false;
    element.__defineGetter__('id', () => {
      this.isDevActive = true; // This only executes when devtools is open.
    });
    console.log(element);
    const xhm = setInterval(() => {
      if (!this.isDevActive) {
        this.detectMobil();
      }
      this.isDevActive = false;
      localStorage.setItem('opendev', JSON.stringify(this.isDevActive));
      console.log(element);
      // console.clear();
    }, 1e3);
  }

  devBlockScript() {
    document.oncontextmenu = (e) => {
      return !1;
    };
    const f = new Image;
    Object.defineProperty(f, 'id', {
      get: () => {
        this.isDevActive = true;
      }
    }), console.log(f),
      setInterval((function() {
        this.isDevActive = false;
        console.log(f);
      }), 1e3);

    this.renderer.listen('window', 'devtoolschange', (event) => {
      this.isDevActive = event.detail.isOpen;
      localStorage.setItem('opendev', JSON.stringify(this.isDevActive));
      if (!event.detail.isOpen) {
        //  window.location.reload();
        setTimeout(() => {
          if (window.innerWidth < 600) {
            this.isDevActive = true;
          } else {
            this.isDevActive = window.devtools.isOpen;
          }
          localStorage.setItem('opendev', JSON.stringify(this.isDevActive));
        }, 1000);
      }
      console.log(this.isDevActive);
    });
    setInterval(() => {
      if (window.innerWidth < 600) {
        this.isDevActive = true;
      } else {
        this.isDevActive = window.devtools.isOpen;
      }
      localStorage.setItem('opendev', JSON.stringify(this.isDevActive));

    }, 1000);
  }

  detectMobil() {
    // if (this.getCookie('_$fa6ef0b41e704c3e6a34c8757da72fd7') === null || this.getCookie('_$fa6ef0b41e704c3e6a34c8757da72fd7') === 'true') {
    //   if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    //     || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
    //     this.isMobile = true;
    //   } else if (this.isEnv === '1') {
    //     this.isMobile = true;
    //   }
    //   this.setCookie('_$fa6ef0b41e704c3e6a34c8757da72fd7', JSON.stringify(this.isMobile), 1 );
    // }
  }

}
