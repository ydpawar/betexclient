import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BasicAuthInterceptor, ErrorInterceptor} from './share/interceptor/index';

/** PLUGINS************************************* */

import {AppMaterialModule} from './app.material.module';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AuthService, SharedataService, ConstantsService, ExcelService, EnvService, APIService} from './core/services/index';
import {AuthGuard} from './share/guard/auth.guard';

/** COMMON***************************************** */
import {BaseComponent} from './share/components/common.component';
import {BetplaceComponent} from './share/components/bottom-sheet/betplace.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {AuthStateGuard} from './share/guard/authstate.guard';


@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    BetplaceComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    APIService,
    SharedataService,
    ConstantsService,
    AuthGuard,
    AuthStateGuard,
    ExcelService,
    EnvService,
    {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ],
  entryComponents: [
    BetplaceComponent
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
