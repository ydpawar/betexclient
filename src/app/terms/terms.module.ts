import { TermsComponent } from './terms.component';
import { NgModule } from '@angular/core';
import { TermsRoutingModule } from './terms-routing.module';

@NgModule({
  imports: [TermsRoutingModule],
  declarations: [
    TermsComponent
  ]
})
export class TermsModule {
}
