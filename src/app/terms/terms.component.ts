import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-terms',
  template: `
    <div class="terms-ui">

      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="scrollableModalTitle">TERMS</h5>
          </div>
          <div class="modal-body">
            <ul>
              <li>If you not accept this agreement do not place any bet.</li>
              <li>Cheating bets deleted automatically or manually, No Claim.</li>
              <li>Admin decision is final and no claim on it.</li>
              <li>Batsman Runs (In-Play) Over/Under (back/lay) runs bets will stand after batsman has faced one ball or is given out before
                first ball is faced. Score counts if batsman is Not-Out including if innings is declared. In case of rain, match abandoned
                etc. settled bets will be valid.
              </li>
              <li>Current/Next Innings Runs Odd/Even Extras and Penalty runs will be included for settlement purposes. Runs at Fall of 1st
                Wicket At least one delivery must be bowled, if no wickets fall bets will be void unless settlement is already determined.
              </li>
              <li>Runs at Fall of Next Wicket The total innings runs scored by a team before the fall of the specified wicket determines the
                result of the market. If a team declares or reaches their target then the total amassed will be the result of the market.
              </li>
              <li>Bets will be void should no more play take place following the intervention of rain, or any other delay, as the
              <li>ability to reach previous quotes offered will have been removed .</li>
              <li>In case of rain, match abandoned etc. settled bets will be valid. We do not accept manual bet.</li>
              <li>In case of cheating and betting in unfair rates we will cancel the bet even after settling. Local fancy are based on Haar
                - Jeet. Incomplete session will be cancelled but complete session will be settled.
              </li>
              <li>In case of match abandoned, cancelled, no result etc. completed sessions will be settled.</li>
              <li>In case of a Tie match, only match odds will be cancelled , completed sessions will be continue.</li>
            </ul>
          </div>
          <div class="modal-footer agree-dis">
            <a href="javascript:void(0);" (click)="isAgree()" class="btn agree-btn">agree</a>
            <a href="javascript:void(0);" (click)="isDisagree()" class="btn disagree-btn">disagree</a>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->

    </div>
  `,
  styles: [`a.agree.ripple {
    color: #ffffff;
  }

  a.disagree.ripple {
    color: #ffffff;
  }

  html {
    z-index: 0;
  }`]
})
export class TermsComponent {

  constructor(private router: Router) {
  }

  isAgree() {
    this.router.navigate(['/']);
  }

  isDisagree() {
    this.router.navigate(['/auth']);
    localStorage.clear();
  }
}
