import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';
import {AuthGuard} from './share/guard/auth.guard';
import { AuthStateGuard } from './share/guard/authstate.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () =>
      import('./core/authentication/authentication.module').then(m => m.AuthenticationModule),
      canActivate: [AuthStateGuard]
  },
  {
    path: 'terms',
    loadChildren: () =>
      import('./terms/terms.module').then(m => m.TermsModule),
    canActivate: [AuthGuard]
  },
  {
    path: '',
    loadChildren: () =>
      import('./modules/modules.module').then(m => m.ModulesModule),
      canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
