import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {APIService} from '../../services/api.service';
import {Sidemenu, SidemenuObj} from './../../../share/models/sidemenu';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {

  public mList: any[];

  constructor(private api: APIService, private cdr: ChangeDetectorRef) {
    this.mList = [];
  }

  ngOnInit(): void {
    this.getMenuData();
  }

  getMenuData() {
    this.api.getMenu().subscribe((res) => {
      if (res.status === 1) {
        for (const data of res.data) {
          const cData = new SidemenuObj(data);
          this.mList.push(cData);
        }
      }
      this.cdr.detectChanges();
      // console.log(this.mList);
    });
  }

}
