import { Component, OnInit } from '@angular/core';
import { RouterModule, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public hideElement: boolean = true;
  public username: string;

  constructor(private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        const tmp = event.urlAfterRedirects.split('/');
        if (tmp.length > 2 && tmp[1] === 'ac') {
          this.hideElement = false;
        }  else {
          this.hideElement = true;
        }
        console.log(this.hideElement);
      }
    });
    this.username = JSON.parse(localStorage.getItem('currentUser')).username;
   }

  ngOnInit() {
  }

}
