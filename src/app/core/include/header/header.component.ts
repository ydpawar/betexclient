import {Component, OnInit, Injector, ChangeDetectorRef, Renderer2} from '@angular/core';
import {Location} from '@angular/common';
import {AuthService, EnvService, SharedataService} from '../../services/index';
import {Router, NavigationEnd, NavigationStart} from '@angular/router';
import {BaseComponent} from '../../../share/components/common.component';
import Swal from 'sweetalert2';
import swal from 'sweetalert2';

export let browserRefresh = false;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends BaseComponent implements OnInit {

  public balance: string;
  public expose: string;
  public username: string;
  public isMenu: boolean;
  public isMobileMenu: boolean;

  public queryString: string = '';
  public isTeenpattiOpen: boolean = false;
  public isRefreshCall: boolean = false;
  public isTimeChange: number;
  public interval2000: any;
  public interval5000: any;

  constructor(inj: Injector, private renderer: Renderer2,
              private service: AuthService,
              private shareData: SharedataService,
              private env: EnvService,
              private location: Location,
              private cdr: ChangeDetectorRef) {
    super(inj);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        browserRefresh = !this.router.navigated;
      }
      if (event instanceof NavigationEnd) {
        this.getBalance();
      }
    });
    this.isMenu = false;
    this.isMobileMenu = false;
  }

  ngOnInit() {
    this.getBalance();
    this.shareData.getCall().subscribe(res => {
      if (res) {
        this.getBalance();
      }
    });

    this.shareData.teenPattiOpen.subscribe((res) => {
      if (res !== undefined) {
        this.isTeenpattiOpen = res;
      }
    });

    this.shareData.itemsRouteChange.subscribe((res) => { // ROUTER CHANGES
      if (res === '1') { // Router change
        clearTimeout(this.interval2000);
        clearTimeout(this.interval5000);
        this.env.IS_CONTINUES_CALL = false;
        this.getBalance();
      } else if (res === '2') { // TEENPATTI
        this.queryString = '?type=teenpatti';
        this.env.IS_CONTINUES_CALL = true;
        this.getBalance();
      } else if (res === '3') { // GAMEOVER & RECALL
        this.env.IS_CONTINUES_CALL = false;
        this.getBalance();
      } else if (res === '4') { // TEENPATTI CALL STOPS
        this.queryString = '';
        this.env.IS_CONTINUES_CALL = false;
      } else { // PLACEBET
        this.env.IS_CONTINUES_CALL = false;
        // this.updateBalance1(res);
      }
    });

    this.shareData.itemsBalanceChange.subscribe((res) => { // BALANCE CHANGES
      this.balance = res;
      this.cdr.detectChanges();
    });

    this.shareData.itemsExposeChange.subscribe((res) => { // EXPOSE CHANGES
      this.expose = res;
      this.cdr.detectChanges();
    });
  }

  getBalance(): void {
    this.service.headerCall().subscribe((res) => {
      if (res.data) {
        this.username = res.data.userName;
        this.balance = res.data.balance.balance;
        this.expose = res.data.balance.expose;
        this.username = res.data.userName;
        this.setToken('myExpose', this.expose);
        this.setToken('betstack', this.optionSplit(res.data.betOption));
        this.setToken('oprater3PattiId', res.data.teenpatti_operatorId);
        this.setToken('eventDetailTiming', res.data.eventDetailTiming.web);
        this.setToken('oddsTiming', res.data.oddsTiming.web);
        this.setToken('eventListTiming', res.data.eventListTiming === undefined ? 0 : res.data.eventListTiming.web);
        this.setToken('balanceRefreshTiming', res.data.balanceRefreshTiming === undefined ? 0 : res.data.balanceRefreshTiming.web);
        this.setToken('userstay', res.data.userInactiveTime === undefined ? 600000 : res.data.userInactiveTime);
        this.shareData.cExpose.emit({
          expose: res.data.balance.expose,
          detail: res.data.teenPattiData,
          status: res.data.teenPattiStatus
        });
        this.shareData.betsoptions.emit(res.data.betOption);
        if (this.env.IS_CONTINUES_CALL) {
          this.shareData.cOpraterId3Patti.emit(res.data.teenpatti_operatorId);
        }


        if (this.isTimeChange === undefined) {
          this.isTimeChange = res.data.balance.updated_time;
        } else if (res.data.balance.updated_time !== this.isTimeChange) {
          this.isTimeChange = res.data.balance.updated_time;
          clearTimeout(this.interval5000);
        }

        if (this.env.IS_CONTINUES_CALL) {
          this.interval2000 = setTimeout(() => {
            // this.eventID = this.eventID === undefined ? '' : this.eventID;
            // this.eventID = this.eventID === false ? '' : this.eventID;
            this.getBalance();
          }, this.env.GET_BALANCE_TIME);
        } // after response call second time
        this.isRefreshCall = false;

        this.cdr.detectChanges();
      }
    });
  }

  optionSplit(data) {
    const opData = data.split(',');
    let strOption = '';
    opData.forEach((item, index) => {
      if (index < 5) {
        if (strOption === '') {
          strOption += item;
        } else {
          strOption += ',' + item;
        }

      }
    });
    return strOption;
  }

  logout(): void {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You Want to be Log Out!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, logout it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.logout().subscribe((res) => {
          localStorage.clear();
          sessionStorage.clear();
          this.router.navigate(['/auth']);
        });
      }
    });
  }

  openMobileMenu() {
    const vSide: any = document.getElementsByTagName('body')[0];
    if (vSide.dataset.platform === 'mobile') {
      this.isMobileMenu = !this.isMobileMenu;
      if (this.isMobileMenu) {
        this.renderer.addClass(document.body, 'sidebar-enable');
      } else {
        this.renderer.removeClass(document.body, 'sidebar-enable');
      }
    } else {
      if (vSide.dataset.sidebarSize === 'default') {
        this.renderer.setAttribute(document.body, 'data-sidebar-size', 'condensed');
      } else {
        this.renderer.setAttribute(document.body, 'data-sidebar-size', 'default');
      }
    }
  }

  back() {
    this.router.navigate(['/main']);
    // if (this.routeUrl === 'teenpatti') {
    //   this.router.navigate(['/main']);
    // } else {
    //   this.location.back();
    // }
  }

}
