import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {ChangeComponent} from './change/change.component';
import {ModalComponent} from './../../share/components/modal.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'change',
    component: ChangeComponent
  }
];

@NgModule({
  declarations: [
    LoginComponent,
    ChangeComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    ModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AuthenticationModule {
}
