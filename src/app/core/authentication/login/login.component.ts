import {Component, OnInit, OnDestroy, Renderer2, ElementRef, ViewChild, ChangeDetectorRef} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService, SharedataService, ModalService, EnvService} from '../../services/index';
import {MatBottomSheet} from '@angular/material/bottom-sheet';

declare var $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css', './login1.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  @ViewChild('captchaElem') captchaElem: ElementRef;
  @ViewChild('indicaters') indicaters: ElementRef;
  @ViewChild('carouselInner') carouselInner: ElementRef;
  public logo: string;
  public form: FormGroup;
  public errorMsg: string;
  public systemInActiveMsg: string;
  public passwordType: string = 'password';
  public isSubmit: boolean = false;
  public downloadUrl: any = window.location.origin !== undefined ? window.location.origin : window.location.protocol + '://' + window.location.hostname;
  public isActiveSYS: number = 0;
  public downloadTag: string;
  public landingData: any;
  public landingSlider: any;
  public whatsappLink: string;

  constructor(
    private renderer: Renderer2,
    public router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private env: EnvService,
    private share: SharedataService,
    private bottomSheet: MatBottomSheet,
    private modalService: ModalService,
    private cdr: ChangeDetectorRef
  ) {
    this.getLandingDetail();
    this.createForm();
    /*******Back button disable***********/
    window.location.hash = 'login';
    window.location.hash = 'Again-No-back-button'; // again because google chrome don't insert first hash into history
    window.onhashchange = () => {
      window.location.hash = 'login';
    };

    this.downloadTag = 'MpcExch';
    this.downloadUrl += '/APK/MpcExch.apk';
    this.bindSliderHTML();
    $('#carouselExampleIndicators').carousel({
      interval: 3000,
      cycle: true
    });
  }

  ngOnInit() {
    this.getCaptchaCode();
  }

  ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'login-bg');
  }

  createForm() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      platform: ['web'],
      systemId: ['1'],
      recaptcha: ['']
    });
  }

  submitLogin() {
    this.errorMsg = '';
    this.isSubmit = true;
    const data = this.form.value;
    if (this.form.valid) {
      this.authService.login(data).subscribe(
        res => {
          this.isSubmit = false;
          if (res.code === 401 && res.status === 0) {
            this.errorMsg = res.message;
            this.cdr.detectChanges();
            this.getCaptchaCode();
          } else if (res.code === 200 && res.status === 1 && res.data === null) {
            this.errorMsg = res.message;
            this.cdr.detectChanges();
            this.getCaptchaCode();
          } else {
            if (res.data.is_password_updated === 1) {
              this.router.navigate(['/terms']);
            } else {
              this.router.navigate(['/auth/change']);
            }
          }
        },
        error => {
          this.isSubmit = false;
        }
      );
    } else if (!this.form.valid) {
      this.errorMsg = 'Please ENTER data.. Try again..';
    } else if (!this.isActiveSYS) {
      this.errorMsg = this.systemInActiveMsg;
    }
  }

  getCaptchaCode() {
    this.authService.recaptchaCode().subscribe((res) => {
      this.form.patchValue({
        recaptcha: res.data.captcha
      });
    });
  }

  getLandingDetail() {
    const platfrorm = document.getElementsByTagName('body')[0];
    this.authService.getLandingDetail().subscribe((res) => {
      if (res.status === 1) {
        this.landingData = res.data;
        this.landingSlider = res.slider;
        sessionStorage.setItem('slider', JSON.stringify(res.slider));
        if (res.data.whatsapp && res.data.whatsapp.NUMBER) {
          this.whatsappLink = 'https://api.whatsapp.com/send?phone=' + res.data.whatsapp.NUMBER + '&text=' + res.data.whatsapp.MESSAGE;
          if (platfrorm.dataset.platform === 'desktop') {
            this.whatsappLink = 'https://web.whatsapp.com/send?phone=' + res.data.whatsapp.NUMBER + '&text=' + res.data.whatsapp.MESSAGE;
          }
        }
        // else {
        //   this.whatsappLink = 'https://api.whatsapp.com/send?phone=1224112&text=test?v=1610457952749';
        // }
      }
      this.cdr.detectChanges();
    });
  }

  scrollToQuestionNode(id) {
    const element = document.getElementById(id);
    element.scrollIntoView({block: 'end', behavior: 'smooth'});
  }

  get frmuUsername() {
    return this.form.get('username');
  }

  get frmPassword() {
    return this.form.get('password');
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('event');
    // this._router.navigate(['/auth/']);
  }

  closeMsg() {
    this.errorMsg = '';
  }

  viewPassword(type) {
    if (type === 'password') {
      this.passwordType = 'text';
    }
    if (type === 'text') {
      this.passwordType = 'password';
    }
  }

  openModal() {
    this.modalService.open('login-model', 'custom-modal-popup');
  }

  closeModal() {
    this.modalService.close('login-model');
  }

  bindSliderHTML() {
    const sliderImages = JSON.parse(sessionStorage.getItem('slider'));
    if (sliderImages !== null) {
      let indicaterHTML = '';
      let carouselBodyHTML = '';
      let i = 0;
      const images = [];
      for (const item of sliderImages) {
        images[i] = new Image();
        images[i].src = item.image; // + '?v=' + this.timestamp
        let className = '';
        if (i === 0) {
          className = 'active';
        }
        indicaterHTML += '<li data-target="#carouselExampleIndicators" data-slide-to="' + i + '" class="' + className + '"></li>';
        carouselBodyHTML += '<div class="carousel-item ' + className + '">';
        carouselBodyHTML += '<img class="d-block w-100" loading="lazy" rel="prefetch" src="' + item.image + '" alt="">';
        carouselBodyHTML += '</div>';
        i++;
      }
      if (this.indicaters && this.carouselInner) {
        this.indicaters.nativeElement.innerHTML = indicaterHTML;
        this.carouselInner.nativeElement.innerHTML = carouselBodyHTML;
      } else {
        const xhm = setTimeout(() => {
          clearTimeout(xhm);
          this.bindSliderHTML();
        }, 100);
      }

      // ?v=' + this.timestamp + '
    } else {
      const xhm = setTimeout(() => {
        clearTimeout(xhm);
        this.bindSliderHTML();
      }, 100);
    }
  }
}

