import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {tap, retry, shareReplay} from 'rxjs/operators';
import {Observable, BehaviorSubject} from 'rxjs';
import {EnvService} from './env.service';
import {User} from '../../share/models/user';

@Injectable({
  providedIn: 'root'
})

export class BetplaceService {
  isLoggedIn = false;
  token: any;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private route: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  betPlace(data) {
    return this.http.post<any>(this.env.API_URL + 'bet-place', data) // , {headers: headers}
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getBetToken() {
    return this.http.get<any>(this.env.API_URL + 'request-generate') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

}
