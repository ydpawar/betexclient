import {Injectable} from '@angular/core';
import {Router, NavigationStart} from '@angular/router';
import * as toastr from 'toastr';

@Injectable({providedIn: 'root'})
export class AlertService {
  private keepAfterRouteChange = false;

  constructor(private router: Router) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterRouteChange) {
          this.keepAfterRouteChange = false;
        } else {
          this.clear();
        }
      }
    });

    toastr.options = {
      closeButton: true,
      debug: false,
      newestOnTop: false,
      progressBar: false,
      positionClass: 'toast-bottom-right',
      preventDuplicates: false,
      onclick: null,
      showDuration: '2000',
      hideDuration: '2000',
      timeOut: '3000',
      extendedTimeOut: '2000',
      showEasing: 'swing',
      hideEasing: 'linear',
      showMethod: 'fadeIn',
      hideMethod: 'fadeOut'
    };
  }

  // convenience methods
  success(message: string) {
    toastr.success(message, '');
  }

  error(message: string) {
    toastr.error(message, '');
  }

  info(message: string, alertId?: string) {
    toastr.info(message, '');
  }

  warn(message: string, alertId?: string) {
    toastr.warning(message, '');
  }

  immClear() {
    toastr.remove();
  }

  // clear alerts
  clear() {
    toastr.clear();
  }
}
