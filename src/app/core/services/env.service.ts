import {Injectable} from '@angular/core';

export let sysconfig;

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  ISENV = 1;
  CURRANT_THEME = 1;
  API_URL = 'https://api.mpcexch.com/api/';
  // API_ODDS_URL = 'https://odds.mpcexch.com:8443/app/';

  // API_URL = 'https://devapi.betex567.com/api/';
  API_ODDS_URL = 'http://54.246.31.65:3000/app/';
  LIVE_GAME_1_D_URL = 'https://d2.fawk.app/#/splash-screen/';
  LIVE_GAME_1_M_URL = 'https://m2.fawk.app/#/splash-screen/';
  LIVE_GAME_2_URL = 'https://livegame.marutix.com/#/fs?'; // https://faas.sports999.in/#/fs
  LIVE_TV_URL = 'https://dreamexch9.co.in/new-channel-list.php?mid=';
  LIVE_SCORE_URL = 'https://scoreboard.dbm9.com/fscoreboard.php?v=';
  WHATSAPP_LINK = '';

  INACTIVE_USER_TIME_THRESHOLD = 60000;
  USER_ACTIVITY_THROTTLER_TIME = 1000;
  IS_UPDATED_PASSWORD = 0;
  IS_CONTINUES_CALL = false;
  GET_BALANCE_TIME: number = 1000;
  SYS_CONFIG: any;
  CC_BET_INDEX = 11;
  JFANCY_BET_INDEX = 6;
  JOTHERCC_BET_INDEX = 7;
  JKHADO_BET_INDEX = 8;

  constructor() {
    sysconfig = this.themeDefaultSetting(this.CURRANT_THEME);
  }

  themeDefaultSetting(theme) {
    switch (theme) {
      case 1:
        return {
          assetsUrl: 'assets/images/',
          uploadsUrl: '',
          operatorId: '1',
          system_key: 'De456Ex99Gf9',
          logo: 'assets/img/logo.png?V=1242',
          favIcon: 'assets/img/fav.png?V=1242',
          theme_id: '1',
          systemname: 'MPCExch',
          isActiveSYS: -1,
          systemInActiveMsg: '',
          primaryColor: '#7ED979',
          themeClass: 'operator-id-1',
          apkdownloadLink: '/download/MPCExch.apk',
          downloadTag: 'MPCEXCH'
        };
        break;
    }
  }
}
