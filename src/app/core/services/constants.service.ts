import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {

  readonly BASE_APP_URL: string = 'http://localhost:10001/';
  readonly DIST_LOCATION: string = 'dist/marketDesktop/';
  readonly DATE_FORMAT: string = 'YYYY-MM-DD';
  readonly SET_INPUT_DATE_TIMEOUT: number = 500;

  constructor() { }
}
