export const config = {
  // Todo update by guest
  port: '4006',
  assetsUrl: '',
  uploadsUrl: '',
  apiTimeOut: 60000,
  env: '1', // 1 -> devlopment, 2 -> production
  isActiveTab: false,
  isContinuesCall: false,
  envProdOId: '9093',
  envDevOId: '90931',
  getBalanceTime: 2000,
  refreshBalanceTime: 60000,
  appListTime: 5000,
  inplayTime: 5000,
  detailTime: 2000,
  oddsTime: 500,
  currantTheme: '1',
  start_date: localStorage.getItem('sdata') !== null ? localStorage.getItem('sdata') : '',
  end_date: localStorage.getItem('edata') !== null ? localStorage.getItem('sdata') : '',
  isActiveLoginBtn: false
};

export const INACTIVE_USER_TIME_THRESHOLD = 60000;
export const USER_ACTIVITY_THROTTLER_TIME = 1000;

export class SystemConfig {
  systems(theme) {
    switch (theme) {
      case '1':
        return {
          assetsUrl: 'assets/images/',
          uploadsUrl: '',
          operatorId: '1',
          system_key: 'De456Ex99Gf9',
          logo: 'assets/img/logo.png',
          favIcon: 'assets/img/icon.ico',
          theme_id: '1',
          systemname: 'Betex567',
          isActiveSYS: -1,
          systemInActiveMsg: '',
          primaryColor: '#c5221b',
          themeClass: 'operator-id-1',
          apkdownloadLink: '/download/Betex567.apk',
          downloadTag: 'Betex567'
        };
        break;
    }
  }
}

const objSystemConfig = new SystemConfig();
export const sysconfig = objSystemConfig.systems(config.currantTheme);
