import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {tap, retry, shareReplay} from 'rxjs/operators';
import {Observable, BehaviorSubject} from 'rxjs';
import {EnvService} from './env.service';
import {User} from '../../share/models/user';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  isLoggedIn = false;
  token: any;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private route: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  isLoggedIn1() {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
    return false;
  }

  login(data) {
    return this.http.post(this.env.API_URL + 'login', data).pipe(
      tap((token: any) => {
        if (token.status === 1 && token.data !== null) {
          localStorage.setItem('currentUser', JSON.stringify(token.data));
          localStorage.setItem('isBannershow', '0');
          this.token = token.data;
          this.isLoggedIn = true;
          this.currentUserSubject.next(this.token);
        }
        return token;
      }),
    );
  }

  recaptchaCode() {
    return this.http.get(this.env.API_URL + 'get-captcha').pipe(
      tap((code: any) => {
        return code;
      }),
    );
  }

  getLandingDetail() {
    return this.http.get(this.env.API_URL + 'landing-detail').pipe(
      tap((code: any) => {
        return code;
      }),
    );
  }

  getSiteMode() {
    return this.http.get(this.env.API_URL + 'site-mode').pipe(
      tap((code: any) => {
        return code;
      }),
    );
  }

  changePassword(data) {
    return this.http.post(this.env.API_URL + 'change-password', data).pipe(
      tap((token: any) => {
        return token;
      }),
    );
  }

  logout() {
    return this.http.get(this.env.API_URL + 'logout')
      .pipe(
        tap(data => {
          localStorage.clear();
          sessionStorage.clear();
          this.currentUserSubject.next(null);
          this.isLoggedIn = false;
          delete this.token;
          return data;
        })
      );
  }

  updateBetOptions(data) {
    return this.http.post(this.env.API_URL + 'set-bet-options', data).pipe(
      tap((code: any) => {
        return code;
      }),
    );
  }

  getUserProfile() {
    return this.http.get(this.env.API_URL + 'user-profile').pipe(
      tap((code: any) => {
        return code;
      }),
    );
  }

  accessDenide() {
    localStorage.clear();
    sessionStorage.clear();
    this.currentUserSubject.next(null);
    this.isLoggedIn = false;
    delete this.token;
    this.route.navigate(['/auth']);
  }

  headerCall(type = '') {
    // const headers = new HttpHeaders({
    //     'Authorization': 'Bearer ' + this.token['token'],
    //     'content-type': 'application/json'
    // });

    return this.http.post<any>(this.env.API_URL + 'user-data' + type, {})
      .pipe(
        retry(4),
        tap(user => {
          // localStorage.setItem('userData', JSON.stringify({
          //   username: user.data.userName,
          //   balance: user.data.balance.balance,
          //   expose: user.data.balance.expose
          // }));
          // localStorage.setItem('betOption', JSON.stringify({betOption: user.data.betOption}));
          // if (user.status === 0 && user.code === 444) {
          //   this.accessDenied();
          // }
          if ([444, 403].indexOf(user.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            this.accessDenide();
            // location.reload(true);
          }
          return user;
        }, err => {
          if (err.status === 0 && err.code === 444) {
            this.accessDenide();
          }
        }),
        shareReplay()
      );
  }

  getToken() {
    this.token = JSON.parse(localStorage.getItem('currentUser'));
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  accessDenied() {
    localStorage.clear();
    sessionStorage.clear();
    this.currentUserSubject.next(null);
    this.isLoggedIn = false;
    delete this.token;
    return false;
  }

}
