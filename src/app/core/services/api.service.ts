import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {tap, retry, shareReplay} from 'rxjs/operators';
import {Observable, BehaviorSubject} from 'rxjs';
import {EnvService} from './env.service';
import {User} from '../../share/models/user';

@Injectable({
  providedIn: 'root'
})

export class APIService {
  isLoggedIn = false;
  token: any;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private route: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  getMenu() {
    return this.http.get<any>(this.env.API_URL + 'dashboard-front-menu-list') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getDashboard() {
    return this.http.get<any>(this.env.API_URL + 'dashboard-list') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getEventlist() {
    return this.http.get<any>(this.env.API_URL + 'event-app-list?platform=desktop') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getInplaylist() {
    return this.http.get<any>(this.env.API_URL + 'event-inplay-today-tomorrow?platform=desktop') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  geteventDetail(id) {
    return this.http.get<any>(this.env.API_URL + 'dashboard-detail/' + id) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getOddsData(data) {
    return this.http.post<any>(this.env.API_ODDS_URL + 'get_odds' , data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getPLMatchoddANDBookmaker(data) {
    return this.http.post<any>(this.env.API_URL + 'profit-loss-matchodd-bookmaker' , data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  deleteUnmatchedBet(data) {
    return this.http.post<any>(this.env.API_URL + 'delete-bet' , data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getMatchedUnmatchedBet(id) {
    return this.http.get<any>(this.env.API_URL + 'user-match-unmatch-data/' + id) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getBetList(data) {
    return this.http.post<any>(this.env.API_URL + 'get-betlist', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getRules() {
    return this.http.get<any>(this.env.API_URL + 'rules') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getCCDetail(id) {
    return this.http.get<any>(this.env.API_URL + 'event-lottery/' + id) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getMyMarketDetail() {
    return this.http.get<any>(this.env.API_URL + 'market-list') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getBinaryDetail(id) {
    return this.http.get<any>(this.env.API_URL + 'event-binary/' + id) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getElectionDetail(id) {
    return this.http.get<any>(this.env.API_URL + 'event-election/' + id) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getCasionList() {
    return this.http.get<any>(this.env.API_URL + 'casino/game-list') // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getCasionUrl(data) {
    return this.http.post<any>(this.env.API_URL + 'casino/game-url', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  fundTransfer(data) {
    return this.http.post<any>(this.env.API_URL + 'casino/transaction', data) // , {headers: headers}
      .pipe(
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getJackportGrid(id) {
    return this.http.get<any>(this.env.API_URL + 'event-jackpot-dynamic-list/' + id) // , {headers: headers}
      .pipe(
       retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
           // location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }


  getJackportList(data) {
    return this.http.post<any>(this.env.API_URL + 'event-jackpot-list', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getJackpotFanyKhado(id, slug: string) {
    return this.http.get<any>(this.env.API_URL + 'event-jackpot-' + slug + '/' + id) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getJackpotDetail(data) {
    return this.http.post<any>(this.env.API_URL + 'event-jackpot-detail', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }
}
