import {PLATFORM_ID, Injectable, Inject, EventEmitter} from '@angular/core';
import {BehaviorSubject, Subject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedataService {

  public platformId;
  private data = {};
  private Setting = [];

  betStatus = new BehaviorSubject<any>(''); // OPEN BET STATUS
  itemsBetStatus = this.betStatus.asObservable();
  marketChange = new BehaviorSubject<any>(''); // OPEN BET MARKET
  itemsMarketChange = this.marketChange.asObservable();

  routeChange = new BehaviorSubject<any>('');
  itemsRouteChange = this.routeChange.asObservable();
  isTabActive = new BehaviorSubject<any>('');
  tabActiveStatus = this.isTabActive.asObservable();
  balanceChange = new BehaviorSubject<any>(''); // BALANCE UPATES
  itemsBalanceChange = this.balanceChange.asObservable();
  exposeChange = new BehaviorSubject<any>(''); // EXPOSE UPATES
  itemsExposeChange = this.exposeChange.asObservable();
  inplayChange = new BehaviorSubject<any>(''); // Inplay UPATES
  itemsInplayChange = this.inplayChange.asObservable();
  betslist: EventEmitter<any> = new EventEmitter();
  eventId: EventEmitter<any> = new EventEmitter();
  localCommantry: EventEmitter<any> = new EventEmitter();
  betsoptions: EventEmitter<any> = new EventEmitter();
  activeRoute: EventEmitter<any> = new EventEmitter();
  CallBalance: EventEmitter<any> = new EventEmitter();
  teenPattiOpen: EventEmitter<any> = new EventEmitter();
  cExpose: EventEmitter<any> = new EventEmitter();
  cOpraterId3Patti: EventEmitter<any> = new EventEmitter();

  headerCall: EventEmitter<any> = new EventEmitter();

  globleCommantryChange = new BehaviorSubject<any>(''); // GLOBLE COMMANTRY
  itemsglobleCommantryChange = this.globleCommantryChange.asObservable();

  itemsbalanceChangeChange = this.balanceChange.asObservable();

  private subject = new Subject<any>();

  constructor(@Inject(PLATFORM_ID) platformId: Object) {
    this.platformId = platformId;
  }

  setOption(option, value) {
    this.data[option] = value;
  }

  setGlobleCommantryChange(data: any) {
    this.globleCommantryChange.next(data);
  }

  getCall(): Observable<any> {
    return this.subject.asObservable();
  }


  getSingleSettingOption(option) {
    return this.Setting[option];
  }

  setRouterChange(data: any) {
    this.routeChange.next(data);
  }

  setIsActiveTab(data: any) {
    this.isTabActive.next(data);
  }

  setBalanceChange(data: any) {
    this.balanceChange.next(data);
  }

  setExposeChange(data: any) {
    this.exposeChange.next(data);
  }

  setInplayChange(data: any) {
    this.inplayChange.next(data);
  }

  setMarketChange(data: any) {
    this.marketChange.next(data);
  }

  setBetStatus(data: any) {
    this.betStatus.next(data);
  }

}
