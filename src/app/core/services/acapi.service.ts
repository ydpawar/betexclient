import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {tap, retry, shareReplay} from 'rxjs/operators';
import {Observable, BehaviorSubject} from 'rxjs';
import {EnvService} from './env.service';
import {User} from '../../share/models/user';

@Injectable({
  providedIn: 'root'
})

export class ACAPIService {
  isLoggedIn = false;
  token: any;
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private env: EnvService,
    private route: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  getLiveBets(data) {
    return this.http.post<any>(this.env.API_URL + 'current-bets', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getAccountStatment(data) {
    return this.http.post<any>(this.env.API_URL + 'transaction-account-statement', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getAccountStatmentBet(data) {
    return this.http.post<any>(this.env.API_URL + 'account-bet-list', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getProfitLoss(data) {
    return this.http.post<any>(this.env.API_URL + 'market-sports-list', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getProfitLossMarket(data) {
    return this.http.post<any>(this.env.API_URL + 'market-profit-loss', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getProfitLossBets(data) {
    return this.http.post<any>(this.env.API_URL + 'market-bet-list', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getProfitlossEvent(data) {
    return this.http.post<any>(this.env.API_URL + 'market-get-event-list', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }


  getBetHistory(data, endpoint) {
    return this.http.post<any>(this.env.API_URL + endpoint, data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getTeenpattiBetHistroy(data) {
    return this.http.post<any>(this.env.API_URL + 'teenpatti-bet-history', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }

  getresults(data) {
    return this.http.post<any>(this.env.API_URL + 'market-result', data) // , {headers: headers}
      .pipe(
        retry(4),
        tap(res => {
          if ([444, 403].indexOf(res.code) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            location.reload(true);
          }
          return res;
        }),
        shareReplay()
      );
  }
}
