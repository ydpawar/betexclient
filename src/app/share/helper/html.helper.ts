import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {Router} from '@angular/router';
// import {config} from '../_shared/config';
import * as CryptoJS from 'crypto-js';

declare var $;

export var noFilterThresold = 100000000;

@Injectable()
export class cHtmlHelper {
  public callCenterInterval: any;


  formatTime(time) {
    let a = moment.unix(parseInt(time));
    return a.format('DD MMM YYYY , hh:mm A');
  }

  formatTimeYMD(time) {
    let a = moment(time);
    return a.format('DD MMM YYYY , hh:mm A');
  }


  formatCurrency(amount) {
    if (typeof amount == 'string') {
      amount = amount.replace('$', '');
    }
    return '$' + parseFloat(amount).toFixed(2);
  }


  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  convertToSlug(Text) {
    return Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
  }

  convertToTitle(Text) {
    const words = Text.split('_');
    return words.map(function (word) {
      return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase();
    }).join(' ');
  }

  parseFloat2Decimals(value, decimalPlaces) {
    return parseFloat(parseFloat(value).toFixed(decimalPlaces));
  }

  checkTabOpenOne() {
    // Do not allow multiple call center tabs
    if (~window.location.origin.indexOf(window.location.origin)) {
      $(window).on('beforeunload onbeforeunload', function () {
        document.cookie = 'ic_window_id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
      });
      this.callCenterInterval = setInterval(this.validateCallCenterTab, 3000);
    }
  }

  validateCallCenterTab() {
    var win_id_cookie_duration = 10; // in seconds
    if (!window.name) {
      window.name = Math.random().toString();
    }
    if (!this.getCookie('ic_window_id') || window.name === this.getCookie('ic_window_id')) {
      // This means they are using just one tab. Set/clobber the cookie to prolong the tab's validity.
      this.setCookie('ic_window_id', window.name, win_id_cookie_duration);
    } else if (this.getCookie('ic_window_id') !== window.name) {
      // config.isTabs = false;
      // this means another browser tab is open, alert them to close the tabs until there is only one remaining
      var message = 'You cannot have this website open in multiple tabs. ' +
        'Please close them until there is only one remaining. Thanks!';
      $('html').html(message);
      clearInterval(this.callCenterInterval);
      throw 'Multiple call center tabs error. Program terminating.';
    }
  }

  // helper function to set cookies
  setCookie(cname, cvalue, seconds) {
    var d = new Date();
    d.setTime(d.getTime() + (seconds * 1000));
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  }

  // helper function to get a cookie
  getCookie(cname) {
    var name = cname + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  convertUrl(textToConvert, encryptMode) {
    const password = 'maerdexhc';
    if (textToConvert) {
      if (textToConvert.toString().trim() === '' || password.toString().trim() === '') {
        console.log('Please fill the textboxes.');
        return;
      } else {
        if (encryptMode) {
          return CryptoJS.AES.encrypt(textToConvert.toString().trim(), password.trim()).toString();
        } else {
          return CryptoJS.AES.decrypt(textToConvert.toString().trim(), password.trim()).toString(CryptoJS.enc.Utf8);
        }
      }
    }
  }
}
