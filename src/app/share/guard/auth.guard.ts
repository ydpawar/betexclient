import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../../core/services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthService) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('currentUser') && JSON.parse(localStorage.getItem('currentUser')).is_password_updated) {
      // logged in so return true
      return true;
    }

    // update pass in so return false
    if (localStorage.getItem('currentUser') && !JSON.parse(localStorage.getItem('currentUser')).is_password_updated) {
      this.router.navigate(['/auth/change']);
      return false;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/auth']);
    return false;
  }


}
