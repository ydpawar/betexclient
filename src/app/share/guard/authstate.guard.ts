import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';

import {AuthService} from '../../core/services';

@Injectable({ providedIn: 'root' })
export class AuthStateGuard implements CanActivate {
  constructor(private router: Router, private auth: AuthService) { }

  async canActivate(): Promise<boolean | UrlTree>  {

    console.log('AuthStateGuard has run');

    if (! await this.auth.isLoggedIn1()) { return true; }

    return this.router.parseUrl('main');
  }

}
