import {Component, OnInit, OnChanges, Renderer2, Input, ChangeDetectorRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventList} from './../models/dashboard';
import {APIService} from './../../core/services';

declare var $: any;
@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html'
})
export class EventListComponent implements OnInit, OnChanges {
  @Input() sportname: string;
  public aList: EventList[];
  public aListTMP: EventList[];
  public cList: any[];

  constructor(private renderer: Renderer2,
              private apiservice: APIService,
              private cdRef: ChangeDetectorRef,
              private router: Router,
              private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.getEventList();
  }

  ngOnChanges() {
    if (this.sportname === 'casino') {
      const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
      body.classList.remove('sidebar-enable');
      this.getCasinoGames();
    }
    if (this.sportname === 'live-games' || this.sportname === 'live-games2') {
      const qstr = this.sportname === 'live-games' ? 'teenpatti' : 'teenpatti/new';
      this.router.navigate([qstr]);
    }
    this.initSport();
    this.getEventList();
  }

  initSport() {
    if (this.sportname && this.aListTMP && this.sportname !== 'casino') {
      this.aList = this.aListTMP[this.sportname];
      this.cdRef.detectChanges();
    } else if (this.sportname === 'casino') {
      this.getCasinoGames();
    } else {
      this.getEventList();
    }
  }

  getEventList() {
    this.apiservice.getEventlist().subscribe((res) => {
      if (res.status === 1) {
        this.aListTMP = res.data.items;
        if (this.sportname) {
          this.aList = this.aListTMP[this.sportname];
        }
        this.cdRef.detectChanges();
      }
      $('#DashSlider').carousel({
        interval: 3000,
        cycle: true
      });
    });
  }

  getCasinoGames() {
    this.apiservice.getCasionList().subscribe((res) => {
      if (res.status === 1) {
        this.cList = [];
        for (const item of res.data) {
          if (item.thumb !== null) {
            this.cList.push(item);
          }
        }
        this.cdRef.detectChanges();
      }
    });
  }

  getCasinoGamesUrl(id) {
    this.router.navigate(['/main/casino/detail/' + id]);
    // const data = {gameId: id, platform: 'GPL_DESKTOP'};
  }
}
