import {Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, Input, AfterViewInit, Injector} from '@angular/core';
import * as moment from 'moment'; // Momentjs
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-filter',
  template: `
    <div class="collapse" id="filter" style="">
      <div class="row justify-content-between">
        <div class="col-md-12">
          <div class="form-row align-items-center justify-content-center">
            <div class="col-md-2 col-6">
              <input type="date" (paste)="false" class="form-control" [(ngModel)]="filter.start_date" [ngModelOptions]="{standalone: true}" #startdate
                     placeholder="From">
            </div>
            <div class="col-md-2 col-6">
              <input type="date" (paste)="false" class="form-control" [(ngModel)]="filter.end_date" [ngModelOptions]="{standalone: true}" #enddate
                     placeholder="To">
            </div>
            <div class="col-md-2 col-6">
              <button class="btn-get waves-effect waves-light" (click)="applyFilter()">get statement</button>
            </div>
            <div *ngIf="title === 'Bet History' || title === 'Teenpatti Bet History'" class="col-md-2 col-6">
              <button type="submit" class="btn-get waves-effect waves-light" [ngClass]="{'bt-active': selCancel === 0}"
                      (click)="getCancel()">{{selCancel ? 'All Bet' : 'Cancel Bet'}}
              </button>
            </div>

            <div class="col-md-2 col-6">

                <div class="input-group">
                    <select class="form-control" (change)="filters($event.target.value)">
                        <option value="" disabled selected hidden> Select</option>
                        <option (click)="filters('today')" value="today">today</option>
                        <option (click)="filters('yesterday')" value="yesterday">Yesterday</option>
                        <option (click)="filters('7_day')" value="7_day">last 7 days</option>
                        <option (click)="filters('30_day')" value="30_day">last 30 days</option>
                    </select>
                    <div class="input-group-append p-0" *ngIf="isDownload">
                        <span class="input-group-text bg-primary ml-1 p-0 border-0">
                            <button class="btn-download" (click)="exportToExcel()" #epltable><i class="mdi mdi-download"></i></button>
                        </span>
                    </div>
                </div>
            </div>
              
          </div>
            
        </div>
      </div>
    </div>`
})

export class FilterComponent implements OnInit, AfterViewInit {

  @ViewChild('epltable') epltable: ElementRef;
  @ViewChild('startdate', {static: false}) startdate: ElementRef;
  @ViewChild('enddate', {static: false}) enddate: ElementRef;
  @Output() filterChanged: EventEmitter<any> = new EventEmitter();
  @Output() isExports = new EventEmitter();
  @Output() isCancel = new EventEmitter();
  @Input() title: string;
  @Input() isDownload: number;
  @Input() isOpen: string;
  public loading = false;
  public filterOpen: boolean;
  public filter: any = {};
  public filterBtn: string = 'today';
  public class: string = 'col-6';
  public slug: string;
  public selCancel: number;
  public acState: number = 1;
  public isexcel: number = 0;
  public spinner: NgxSpinnerService;

  constructor(injector: Injector) {
    this.filterOpen = false;
    this.class = 'col-6';
    this.selCancel = 0;
    this.spinner = injector.get(NgxSpinnerService);
  }

  ngOnInit(): void {
    // this.spinner.show();
    // this.isCancel.subscribe(res => {
    //   this.selCancel = res;
    // });
    console.log(this.title);
  }

  ngAfterViewInit() {
    // if (this.isOpen === undefined) {
    // debugger;
    setTimeout(() => {
      if (this.generateTitle(this.title) === 'Bet_History' || this.generateTitle(this.title) === 'Account_Statement' || this.generateTitle(this.title) === 'Result') {
        this.class = 'col-4';
      }
    }, 500);

    this.slug = this.generateTitle(this.title);
    const now = moment();
    const today = now.format('YYYY-MM-DD');
    setTimeout(() => {
      this.startdate.nativeElement.value = today;
      this.enddate.nativeElement.value = today;
    }, 500);
    this.filter.start_date = today, this.filter.end_date = today;
    const data = {start_date: today, end_date: today, isFirst: 1, isCancel: this.selCancel};
    if (this.title !== 'Profit-Loss' ) {
      this.filterChanged.emit(data);
    }

    // }
  }

  onChange($event) {
    this.filter.type = $event.target.options[$event.target.options.selectedIndex].value;
  }

  getCancel() {
    this.selCancel = this.selCancel ? 0 : 1;
    this.applyFilter();
  }

  applyFilter() {
    console.log(this.title);
    this.filter.isFirst = 0;
    if (this.title === 'Bet History' || this.title === 'Teenpatti Bet History' || this.title === 'Casino Bet History') {
      this.filter.isCancel = this.selCancel;
    }
    if (this.filter.start_date && this.filter.end_date) {
      this.filterOpen = false;
      this.filterChanged.emit(this.filter);
    }
  }

  clearFilter() {
    this.filters('today');
    this.filterChanged.emit(this.filter);
  }


  filters(search: string) {
    this.filterBtn = search;
    this.filter.isFirst = 0;
    if (this.title === 'Bet History' || this.title === 'Teenpatti Bet History' || this.title === 'Casino Bet History') {
      this.filter.isCancel = this.selCancel;
    }
    switch (search) {
      case 'today' :
        this.filter.start_date = moment().format('YYYY-MM-DD');
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case 'yesterday' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(1, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '7_day' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(7, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '30_day' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(30, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
      case '3_month' :
        this.filter.end_date = moment().format('YYYY-MM-DD');
        this.filter.start_date = moment().subtract(90, 'd').format('YYYY-MM-DD');
        this.setInputDate(this.filter.start_date, this.filter.end_date);
        break;
    }
    this.filterChanged.emit(this.filter);
  }

  setInputDate(sDate: Date, eDate: Date) {
    setTimeout(() => {
      this.startdate.nativeElement.value = moment(sDate).format('YYYY-MM-DD');
      this.enddate.nativeElement.value = moment(eDate).format('YYYY-MM-DD');
    }, 500);
  }

  generateTitle(slug: string) {
    const words = slug.split(' ');
    for (let i = 0; i < words.length; i++) {
      const word = words[i];
      words[i] = word.charAt(0).toUpperCase() + word.slice(1);
    }
    return words.join('_');
  }

  exportToExcel() {
    this.loading = true;
    // this.spinner.show();
    this.isexcel = 1;
    // if(this.loading===true){
    //   this.spinner.hide();
    //   this.loading = false;
    //   this.spinner.hide();
    // }
    this.filterChanged.emit(this.isexcel);
    // const now = moment();
    // const today = now.format('YYYY-MM-DD hh:mm:ss');
    // const ws: xlsx.WorkSheet =
    //     xlsx.utils.table_to_sheet(this.epltable.nativeElement);
    // const wb: xlsx.WorkBook = xlsx.utils.book_new();
    // xlsx.utils.book_append_sheet(wb, ws, 'Sheet1');
    // xlsx.writeFile(wb, this.title + today + '.xlsx');
  }
}

