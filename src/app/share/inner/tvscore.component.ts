import {Component, Input, OnInit, AfterViewInit, OnChanges, ChangeDetectorRef, ViewChild, ElementRef} from '@angular/core';

declare var $;

@Component({
  selector: 'app-live-tv-score',
  template: `
      <div [ngClass]="{'popout-video': platform === 'mobile', 'video': platform === 'desktop'}" #bindFrame>Loading frame...</div>`,
  styles: [`
      #fp_embed_player {
          height: 30vh;
      }
  `]
})
export class LiveTvScoreComponent implements OnInit, OnChanges, AfterViewInit {

  @ViewChild('bindFrame', {static: false}) bindFrameHTML: ElementRef;
  @Input() bindUrl: string;
  @Input() isTvOn: boolean;
  @Input() isScoreOn: boolean;
  platform: string;

  constructor(private changedRef: ChangeDetectorRef) {
    const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
    this.platform = body.dataset.platform;
  }

  ngOnInit() {
    document.onreadystatechange = () => {
      const state = document.readyState;
      console.log(state);
      if (state === 'interactive') {
      } else if (state === 'complete') {
        setTimeout(() => {
          // document.getElementById('load').style.visibility = 'hidden';
        }, 1000);
      }
    };
  }

  ngAfterViewInit(): void {
    if (this.platform === 'mobile') {
      this.setMobileTV();
    } else {
      this.setDesktopTV();
    }
  }

  ngOnChanges(changes: any) {
    if (this.isTvOn) {
      this.openTV(this.bindUrl);
    } else if (this.isScoreOn) {
      this.openTV(this.bindUrl);
    }
  }

  public openTV(iurl: string) {
    // iurl = 'https://cheeryexch.com:8888/embed_player?urlServer=wss://cheeryexch.com:8443&streamName=ch_4&mediaProviders=WebRTC,Flash,MSE,WSPlayer';
    this.bindFrameHTML.nativeElement.innerHTML = '<div class="close-video"></div><iframe id="fp_embed_player" src="' + iurl + '" frameborder="0" width = "100%" height="100%" scrolling="no"> </iframe>';
    const frame = document.getElementById('fp_embed_player');
    frame.style.height = '30vh';
    frame.style.border = '4px solid #131924';
    this.setCloseTVEvent();
    this.changedRef.detectChanges();
  }

  setMobileTV() {
    // console.clear();
    const $window = $(window);
    const introContainer: any = document.querySelector('.intro');
    const videoContainer: any = document.querySelector('.popout-video');
    // const video = videoContainer.querySelector('video');
    const videoHeight = videoContainer.offsetHeight;

    const closeVideoBtn = document.querySelector('.close-video');
    let popOut = true;
    introContainer.style.height = `${videoHeight}px`;

    $window.on('scroll', () => {
      if (window.scrollY > videoHeight) {
        // only pop out the video if it wasnt closed before
        if (popOut) {
          videoContainer.classList.add('popout-video--popout');
          // set video container off the screen for the slide in animation
          videoContainer.style.top = `-${videoHeight}px`;
        }
      } else {
        videoContainer.classList.remove('popout-video--popout');
        videoContainer.style.top = `0px`;
        popOut = true;
      }
    });

    // // close the video and prevent from opening again on scrolling + pause the video
    // closeVideoBtn.addEventListener('click', () => {
    //   videoContainer.classList.remove('popout-video--popout');
    //   video.pause();
    //   popOut = false;
    // });
  }

  setDesktopTV() {
    const $window = $(window);
    const $videoWrap = $('.video-wrap');
    const $video = $('.video');
    const videoHeight = $video.outerHeight();

    $window.on('scroll', () => {
      const windowScrollTop = $window.scrollTop();
      const videoBottom = videoHeight + $videoWrap.offset().top;

      if (windowScrollTop > 290 && windowScrollTop > videoBottom) {
        $videoWrap.height(videoHeight);
        $video.addClass('stuck');
      } else {
        $videoWrap.height('auto');
        $video.removeClass('stuck');
      }
    });
  }

  setCloseTVEvent() {
    let videoContainer: any;  let video: any; let popOut = true;
    if (this.platform === 'mobile') {
      videoContainer = document.querySelector('.popout-video');
      video = videoContainer.querySelector('video');
    }
    const closeVideoBtn = document.querySelector('.close-video');

    // close the video and prevent from opening again on scrolling + pause the video
    closeVideoBtn.addEventListener('click', () => {
      if (this.platform === 'mobile') {
        videoContainer.classList.remove('popout-video--popout');
        if (video) {
          video.pause();
        }
        popOut = false;
      } else {
        const $video = $('.video');
        $video.removeClass('stuck');
      }
      this.bindFrameHTML.nativeElement.innerHTML = '';
    });
  }

}
