export interface Sidemenu {
  sportId: number;
  name: string;
  slug: string;
  status: number;
  icon: string;
  icon_class: string;
  list: any[];
}

export class SidemenuObj implements Sidemenu {
  sportId: number;
  name: string;
  slug: string;
  status: number;
  icon: string;
  icon_class: string;
  list: any[];

  constructor(item?: Sidemenu) {
    this.sportId = item.sportId;
    this.name = item.name;
    this.slug = item.slug;
    this.status = item.status;
    this.icon = item.icon;
    this.icon_class = item.icon_class;
    this.list = item.list;
  }
}
