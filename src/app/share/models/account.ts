export interface AccountStatementList {
  id: number;
  sid: number;
  description: string;
  type: string;
  amount: string;
  balance: string;
  marketId: string;
  status: number;
  createdOn: string;
}

export class AccountStatementObj implements AccountStatementList {
  id: number;
  sid: number;
  description: string;
  type: string;
  amount: string;
  balance: string;
  marketId: string;
  status: number;
  createdOn: string;

  constructor(item?: AccountStatementList) {
    this.id = item.id;
    this.sid = item.sid;
    this.description = item.description;
    this.type = item.type;
    this.amount = item.amount;
    this.balance = item.balance;
    this.marketId = item.marketId;
    this.status = item.status;
    this.createdOn = item.createdOn;
  }
}


/**********************************************
 * ACCOUNT-BET ********************************
 **********************************************/

export interface AccountStatementBetList {
  betId: number;
  description: string;
  bType: string;
  price: string;
  size: number;
  date: string;
  result: string;
  win: number;
  loss: number;
  rate: string;
  mType: string;
}

export class AccountStatementBetObj implements AccountStatementBetList {
  betId: number;
  bType: string;
  description: string;
  price: string;
  size: number;
  date: string;
  result: string;
  win: number;
  loss: number;
  rate: string;
  mType: string;

  constructor(item?: AccountStatementBetList) {
    this.betId = item.betId;
    this.bType = item.bType;
    this.description = item.description;
    this.price = item.price;
    this.size = item.size;
    this.date = item.date;
    this.result = item.result;
    this.win = item.win;
    this.loss = item.loss;
    this.rate = item.rate;
    this.mType = item.mType;
  }
}

/***PROFIT-LOSS*******************************************************************************************/

/**********************************************
 * SPORT-LIST-PL ********************************
 **********************************************/

export interface ProfitLossSportList {
  sportId: number;
  sportName: string;
  profitLoss: number;
}

export class ProfitLossSportObj implements ProfitLossSportList {
  sportId: number;
  sportName: string;
  profitLoss: number;

  constructor(item?: ProfitLossSportList) {
    this.sportId = item.sportId;
    this.sportName = item.sportName;
    this.profitLoss = item.profitLoss;
  }
}


/**********************************************
 * EVENT-LIST-PL ********************************
 **********************************************/

export interface ProfitLossEventList {
  eventId: number;
  eventName: string;
  sportName: string;
  slug: string;
  mType: string;
  profitLoss: number;
  settledDate: string;
}

export class ProfitLossEventObj implements ProfitLossEventList {
  eventId: number;
  eventName: string;
  sportName: string;
  slug: string;
  mType: string;
  profitLoss: number;
  settledDate: string;

  constructor(item?: ProfitLossEventList) {
    this.eventId = item.eventId;
    this.eventName = item.eventName;
    this.sportName = item.sportName;
    this.slug = item.slug;
    this.mType = item.mType;
    this.settledDate = item.settledDate;
    this.profitLoss = item.profitLoss;
  }
}

/**********************************************
 * MARKET-LIST-PL ********************************
 **********************************************/

export interface ProfitLossMarketList {
  eventId: number;
  marketId: string;
  mType: string;
  winner: string;
  description: string;
  profitLoss: number;
  settledDate: string;
}

export class ProfitLossMarketObj implements ProfitLossMarketList {
  eventId: number;
  marketId: string;
  mType: string;
  winner: string;
  description: string;
  profitLoss: number;
  settledDate: string;

  constructor(item?: ProfitLossMarketList) {
    this.eventId = item.eventId;
    this.marketId = item.marketId;
    this.mType = item.mType;
    this.winner = item.winner;
    this.description = item.description;
    this.settledDate = item.settledDate;
    this.profitLoss = item.profitLoss;
  }
}

/**********************************************
 * MARKET-PL-BET ********************************
 **********************************************/

export interface ProfitLossBetList {
  betId: number;
  description: string;
  bType: string;
  price: string;
  rate: number;
  size: number;
  date: string;
  result: string;
  win: number;
  loss: number;
  createdOn: string;
  mType: string;
}

export class ProfitLossBetObj implements ProfitLossBetList {
  betId: number;
  bType: string;
  description: string;
  price: string;
  rate: number;
  size: number;
  date: string;
  result: string;
  win: number;
  loss: number;
  createdOn: string;
  mType: string;

  constructor(item?: ProfitLossBetList) {
    this.betId = item.betId;
    this.bType = item.bType;
    this.description = item.description;
    this.price = item.price;
    this.rate = item.rate;
    this.size = item.size;
    this.date = item.date;
    this.result = item.result;
    this.win = item.win;
    this.loss = item.loss;
    this.createdOn = item.createdOn;
    this.mType = item.mType;
  }
}

/**********************************************
 * CASINO-BET ********************************
 **********************************************/

export interface CasinoBetList {
  betId: number;
  description: string;
  type: string;
  bType: string;
  amount: number;
  date: string;
  result: string;
  win: number;
  loss: number;
}

export class CasinoBetObj implements CasinoBetList {
  betId: number;
  description: string;
  type: string;
  bType: string;
  amount: number;
  date: string;
  result: string;
  win: number;
  loss: number;

  constructor(item?: CasinoBetList) {
    this.betId = item.betId;
    this.bType = item.bType;
    this.description = item.description;
    this.type = item.type;
    this.amount = item.amount;
    this.date = item.date;
    this.result = item.result;
    this.win = item.win;
    this.loss = item.loss;
  }
}


