export interface SportList {
  sport_id: number;
  sport_name: string;
  sport_slug: string;
  img: string;
  icon: string;
  icon_class: string;
  is_new: number;
}

export class SportListObj implements SportList {
  sport_id: number;
  sport_name: string;
  sport_slug: string;
  img: string;
  icon: string;
  icon_class: string;
  is_new: number;

  constructor(item?: SportListObj) {
    this.sport_id = item.sport_id;
    this.sport_name = item.sport_name;
    this.sport_slug = item.sport_slug;
    this.img = item.img;
    this.icon = item.icon;
    this.icon_class = item.icon_class;
    this.is_new = item.is_new;
  }
}

export interface EventList {
  slug: string;
  type: string;
  sportId: string;
  eventId: string;
  marketId: string;
  title: string;
  league: string;
  time: string;
  runner1: string;
  runner2: string;
  is_fancy: string;
  is_favorite: string;
  isFancy: number;
  isJackpot: number;
  isGhoda: number;
  is_tv: string;
  closeTime: string;
}

