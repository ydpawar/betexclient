import {
  Component, OnInit, Inject, Injector, ViewRef, ChangeDetectionStrategy,
  ViewEncapsulation
} from '@angular/core';
import {MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';
import {BaseComponent} from './../common.component';
import {BehaviorSubject} from 'rxjs';
import {Location} from '@angular/common';
import {matBottomSheetAnimations} from './bottom-sheet-animations';
import {AlertService, BetplaceService, SharedataService} from '../../../core/services';

@Component({
  selector: 'app-bottom-sheet',
  templateUrl: './betplace.component.html',
  styleUrls: ['./betplace.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  animations: [matBottomSheetAnimations.bottomSheetState]
})
export class BetplaceComponent extends BaseComponent implements OnInit {

  public index: any;
  public isBetPlace: boolean = true;
  public lastClickTime: number = 0;
  public betOptionsList: any;
  public keyFocue: string = '1';
  public activeOdd1Amount: any;
  public matchOdd2price: any = 0;
  public matchOdd2size: any = 0;
  public matchOdd2profit = 0;
  public profitLoss1: any = null;
  public profitLoss2: any = null;
  public profitLoss3: any = null;

  private csrfToken: any;
  public placeBet: any = {};
  public classN: string = 'LayBetting';
  public market: any;
  public globleColor: boolean = false;
  public inx0: number = -1;
  public inx1: number = -1;
  public inx2: number = -1;
  public marketsOdds: any = [];
  public fancy1price: any;
  public fancy1rate: any;
  public matchOddsSize: string;
  backSize1 = new BehaviorSubject<any>('');
  tabActiveStatus = this.backSize1.asObservable();
  public plLimits: any = {};
  public plArrTmp: any = [];

  constructor(inj: Injector, private commonService: SharedataService,
              private alert: AlertService, private betServie: BetplaceService,
              private bottomSheetRef: MatBottomSheetRef<BetplaceComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
              private location: Location
  ) {
    super(inj);
    this.commonService.setBetStatus(1);
    this.plLimits = data.limit;
    this.index = data.index;
  }

  ngOnInit() {
    // const doc: any = document.getElementsByTagName('html')[0];
    // doc.style.top = 0;
    this.betOptionsList = this.setStackShortcut(this.getToken('betstack'));
    this.inilization();
    document.getElementById('odd2size').focus();
  }

  inilization() {
    this.tokenGenerate();
    this.placeBet = this.data.int;
    console.log(this.placeBet);
    this.plArrTmp = this.data.market;
    this.classN = (this.data.int.bet_type === 'back' || this.data.int.bet_type === 'yes') ? '1' : '0';
    this.matchOdd2price = (this.placeBet.m_type === 'match_odd'
      || this.placeBet.m_type === 'completed_match' ||
      this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' ||
      this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market'
      || this.placeBet.m_type === 'bookmaker' || this.placeBet.m_type === 'virtual_cricket'
      || this.placeBet.m_type === 'fancy3' || this.placeBet.m_type === 'oddeven')
      // tslint:disable-next-line:radix
      ? parseFloat(this.placeBet.price) : parseInt(this.placeBet.price);
    if (this.placeBet.m_type === 'winner') {
      this.matchOdd2price = parseFloat(this.parseCurrency(this.placeBet.price));
    }
    this.matchOdd2size = 0;
    this.matchOdd2profit = 0;
    this.fancy1rate = (this.placeBet.m_type !== 'match_odd' || this.placeBet.m_type !== 'completed_match' || this.placeBet.m_type !== 'tied_match' || this.placeBet.m_type !== 'winner' || this.placeBet.m_type !== 'goals' || this.placeBet.m_type !== 'set_market' || this.placeBet.m_type !== 'bookmaker' || this.placeBet.m_type !== 'virtual_cricket' || this.placeBet.m_type === 'fancy3' || this.placeBet.m_type === 'oddeven') ? parseFloat(this.placeBet.rate) : 0;
    this.globleColor = (this.placeBet.bet_type === 'back' || this.placeBet.bet_type === 'yes') ? false : true;
    this.fancy1rate = (this.placeBet.slug === 'binary' || this.placeBet.m_type === 'fancy' || this.placeBet.m_type === 'fancy2' || this.placeBet.m_type === 'fancy3' || this.placeBet.m_type === 'meter' || this.placeBet.m_type === 'oddeven' || this.placeBet.m_type === 'khado' || this.placeBet.m_type === 'ballbyball' || this.placeBet.m_type === 'cricket_casino') ? this.placeBet.rate : 0;

    if (this.placeBet.m_type === 'match_odd' || this.placeBet.m_type === 'completed_match' || this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' || this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market' || this.placeBet.m_type === 'bookmaker' || this.placeBet.m_type === 'virtual_cricket') {
      let i = 0;
      this.plArrTmp.forEach((itemrunner) => {
        // tslint:disable-next-line:radix
        const secId = parseInt(this.placeBet.sec_id);
        let keepGoing = true;
        // tslint:disable-next-line:radix
        if (itemrunner.secId !== undefined && parseInt(itemrunner.secId) === secId && this.inx0 === -1 && keepGoing) {
          this.inx0 = i;
          keepGoing = false;
        }
        // tslint:disable-next-line:radix
        if (itemrunner.secId !== undefined && parseInt(itemrunner.secId) !== secId && this.inx1 === -1 && keepGoing) {
          this.inx1 = i;
          keepGoing = false;
        }
        // tslint:disable-next-line:radix
        if (this.plArrTmp.length === 3 && itemrunner.secId !== undefined && parseInt(itemrunner.secId) !== secId && this.inx2 === -1 && keepGoing) {
          this.inx2 = i;
          keepGoing = false;
        }
        i++;
      });
    }
    this.commonService.marketChange.subscribe((res) => {
      if (res) {
        if (!(this.changedRef as ViewRef).destroyed) {
          this.marketsOdds = res;
          //     console.log(this.marketsOdds);
          this.changedRef.detectChanges();
        }
      }
    });

    this.location.subscribe(x => {
      if (x) {
        this.bottomSheetRef.dismiss();
      }
    }); // CLOSE BOTTOM SHEET AFTER BACK TO BROWSER
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  parseCurrency(e) {
    if (typeof (e) === 'number') {
      return e;
    }
    if (typeof (e) === 'string') {
      const str = e.trim();
      const value = e.replace(/[^0-9.]+/g, '');
      return str.startsWith('(') && str.endsWith(')') ? -value : value;
    }
    return e;
  }

  closeBetSlip(event: MouseEvent): void {
    this.globleColor = false;
    this.profitLoss1 = '';
    this.profitLoss2 = '';
    this.profitLoss3 = '';
    this.matchOdd2profit = 0;
    this.activeOdd1Amount = '';
    this.commonService.setBetStatus(3);
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  changeBet(betType: string, price, rate = 0) {
    if (price === 0) {
      this.alert.error('Zero rate bet can`t place');
      return;
    }
    if (!(this.changedRef as ViewRef).destroyed) {
      // debugger;
      this.globleColor = (betType === 'back' || betType === 'yes') ? false : true;
      this.classN = (betType === 'back' || betType === 'yes') ? '1' : '0';
      this.profitLoss1 = '';
      this.profitLoss2 = '';
      this.profitLoss3 = '';
      this.matchOdd2profit = 0;
      this.matchOdd2size = 0;
      this.matchOdd2price = (this.placeBet.m_type === 'match_odd' || this.placeBet.m_type === 'completed_match' || this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' || this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market' || this.placeBet.m_type === 'bookmaker' || this.placeBet.m_type === 'virtual_cricket' || this.placeBet.m_type === 'fancy3' || this.placeBet.m_type === 'oddeven') ? parseFloat(price) : parseInt(price);
      if (this.placeBet.m_type === 'winner') {
        this.matchOdd2price = parseFloat(this.parseCurrency(price));
      }
      this.fancy1rate = (this.placeBet.m_type !== 'match_odd' || this.placeBet.m_type !== 'completed_match' || this.placeBet.m_type !== 'tied_match' || this.placeBet.m_type !== 'winner' || this.placeBet.m_type !== 'goals' || this.placeBet.m_type !== 'set_market' || this.placeBet.m_type !== 'bookmaker' || this.placeBet.m_type !== 'virtual_cricket' || this.placeBet.m_type !== 'fancy3' || this.placeBet.m_type !== 'oddeven') ? rate : rate;
      this.activeOdd1Amount = '';
      this.placeBet.bet_type = betType;
      this.placeBet.price = price;
      this.changedRef.detectChanges();
    }
  }

  minusData(id) {
    const elemnt: any = document.getElementById(id) as HTMLElement;
    // elemnt.stepDown(1);
    let tmp1: any = parseFloat(elemnt.value);  // old
    tmp1 -= 0.01;
    tmp1 = tmp1.toFixed(2);

    let tmptt: any = parseFloat(this.matchOdd2price); // new
    tmptt -= 0.01;
    this.matchOdd2price = tmptt.toFixed(2);
    let tmp = 0;
    let action = this.placeBet.m_type;
    if (this.placeBet.m_type === 'set_market') {
      action = 'match_odd';
    }
    switch (action) {
      case 'match_odd':
        tmp = tmptt - 1;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'completed_match':
        tmp = tmptt - 1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'tied_match':
        tmp = tmptt - 1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'winner':
        tmp = tmptt - 1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'goals':
        tmp = tmptt - 1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'bookmaker':
        tmp = tmptt / 100;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'virtual_cricket':
        tmp = tmptt - 1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'khado':
        // tslint:disable-next-line:no-shadowed-variable
        let tmp1: any = parseFloat(elemnt.value);
        tmp1 -= 1;
        // tslint:disable-next-line:radix
        tmp1 = parseInt(tmp1);
        this.matchOdd2price = tmp1;
        tmp = this.fancy1rate * this.matchOdd2size;
        const profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
    }
  }

  plusData(id) {
    const elemnt: any = document.getElementById(id) as HTMLElement;
    // elemnt.stepUp(1);
    let tmp1: any = parseFloat(elemnt.value);
    tmp1 += 0.01;
    tmp1 = tmp1.toFixed(2);

    let tmptt: any = parseFloat(this.matchOdd2price);
    tmptt += 0.01;
    this.matchOdd2price = tmptt.toFixed(2);
    let tmp = 0;
    let action = this.placeBet.m_type;
    if (this.placeBet.m_type === 'set_market') {
      action = 'match_odd';
    }
    switch (action) {
      case 'match_odd':
        tmp = tmptt - 1;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'completed_match':
        tmp = tmptt - 1;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'tied_match':
        tmp = tmptt - 1;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'winner':
        tmp = tmptt - 1;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'goles':
        tmp = tmptt - 1;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'bookmaker':
        tmp = tmptt / 100;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'virtual_cricket':
        tmp = tmptt - 1;
        // this.matchOdd2price = tmp1;
        if (this.matchOdd2size > 0) {
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'khado':
        // tslint:disable-next-line:no-shadowed-variable
        let tmp1: any = parseFloat(elemnt.value);
        tmp1 += 1;
        // tslint:disable-next-line:radix
        tmp1 = parseInt(tmp1);
        this.matchOdd2price = tmp1;
        tmp = this.fancy1rate * this.matchOdd2size;
        const profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
    }
  }

  minusSize() {
    let action = this.placeBet.m_type;
    let tmp = 0;
    let profit = 0;
    if (this.placeBet.slug === 'binary' || this.placeBet.m_type === 'khado' || this.placeBet.m_type === 'ballbyball' || this.placeBet.m_type === 'meter') {
      action = 'fancy';
    }

    if (this.placeBet.m_type === 'oddeven') {
      action = 'fancy3';
    }

    if (this.placeBet.m_type === 'completed_match' || this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' || this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market' || this.placeBet.m_type === 'virtual_cricket') {
      action = 'match_odd';
    }

    switch (action) {
      case 'match_odd':
        this.matchOdd2size = Number(this.matchOdd2size) - Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'bookmaker':
        this.matchOdd2size = Number(this.matchOdd2size) - Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price / 100;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'fancy':
        this.matchOdd2size = Number(this.matchOdd2size) - Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy2':
        this.matchOdd2size = Number(this.matchOdd2size) - Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy3':
        this.matchOdd2size = Number(this.matchOdd2size) - Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'jackpot':
        this.matchOdd2size = Number(this.matchOdd2size) - Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        this.initProfitLoss();
        break;
      case 'cricket_casino':
        this.matchOdd2size = Number(this.matchOdd2size) - Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.fancy1rate - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        // this.initProfitLoss();
        break;

    }
  }

  plusSize() {
    let action = this.placeBet.m_type;
    let tmp = 0;
    let profit = 0;
    if (this.placeBet.slug === 'binary' || this.placeBet.m_type === 'khado' || this.placeBet.m_type === 'ballbyball' || this.placeBet.m_type === 'meter') {
      action = 'fancy';
    }
    if (this.placeBet.m_type === 'oddeven') {
      action = 'fancy3';
    }
    if (this.placeBet.m_type === 'completed_match' || this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' || this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market' || this.placeBet.m_type === 'virtual_cricket') {
      action = 'match_odd';
    }
    switch (action) {
      case 'match_odd':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'bookmaker':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price / 100;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'fancy':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy2':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy3':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'jackpot':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        this.initProfitLoss();
        break;
      case 'cricket_casino':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(5000);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.fancy1rate - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        // this.initProfitLoss();
        break;

    }
  }

  keyPressSize() {
    let action = this.placeBet.m_type, tmp = 0, profit = 0;
    if (this.placeBet.slug === 'binary' || this.placeBet.m_type === 'khado' || this.placeBet.m_type === 'ballbyball' || this.placeBet.m_type === 'meter') {
      action = 'fancy';
    }
    if (this.placeBet.m_type === 'oddeven') {
      action = 'fancy3';
    }
    if (this.placeBet.m_type === 'completed_match' || this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' || this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market' || this.placeBet.m_type === 'virtual_cricket') {
      action = 'match_odd';
    }
    switch (action) {
      case 'match_odd':
        this.matchOdd2size = Number(this.matchOdd2size);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'bookmaker':
        this.matchOdd2size = Number(this.matchOdd2size);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price / 100;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'fancy':
        this.matchOdd2size = Number(this.matchOdd2size);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy2':
        this.matchOdd2size = Number(this.matchOdd2size);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy3':
        this.matchOdd2size = Number(this.matchOdd2size);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'jackpot':
        this.matchOdd2size = Number(this.matchOdd2size);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        this.initProfitLoss();
        break;
      case 'cricket_casino':
        this.matchOdd2size = Number(this.matchOdd2size);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.fancy1rate - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        // this.initProfitLoss();
        break;

    }
  }

  /**
   *
   * @param type MATCH-ODD|MATCH-ODD2|FANCY|FANCY2|LOTTERY
   * @param stack STACK TYPE ACTIVE
   * @param price STACK VALUE
   */
  addStack(stack, price) {
    this.activeOdd1Amount = stack;
    let action = this.placeBet.m_type;
    if (this.placeBet.slug === 'binary' || this.placeBet.m_type === 'khado' || this.placeBet.m_type === 'ballbyball' || action === 'meter') { // CALCULATION SAME AS FANCY
      action = 'fancy';
    }
    if (this.placeBet.m_type === 'oddeven') {
      action = 'fancy3';
    }
    if (this.placeBet.m_type === 'completed_match' || this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' || this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market' || this.placeBet.m_type === 'virtual_cricket') {
      action = 'match_odd';
    }

    let tmp = 0;
    let profit = 0;
    switch (action) {
      case 'match_odd':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'bookmaker':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price / 100;
        this.matchOdd2profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(this.matchOdd2profit);
        this.initProfitLoss();
        break;
      case 'fancy':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy2':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy3':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'ballbyball':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'jackpot':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        // this.initProfitLoss();
        break;
      case 'cricket_casino':
        this.matchOdd2size = Number(this.matchOdd2size) + Number(price);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.fancy1rate - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        // this.initProfitLoss();
        break;

    }
  }

  /**
   *
   * @param action 1 TO 9 | 00 | 0 | . | Back
   */
  keyboard(action: string, type: string) {
    let saction = this.placeBet.m_type;
    if (this.placeBet.slug === 'binary' || this.placeBet.m_type === 'khado' || this.placeBet.m_type === 'ballbyball' || saction === 'meter') {
      saction = 'fancy';
    }
    if (this.placeBet.m_type === 'oddeven') {
      saction = 'fancy3';
    }
    if (this.placeBet.m_type === 'completed_match' || this.placeBet.m_type === 'tied_match' || this.placeBet.m_type === 'winner' || this.placeBet.m_type === 'goals' || this.placeBet.m_type === 'set_market' || this.placeBet.m_type === 'virtual_cricket') {
      saction = 'match_odd';
    }

    if (this.matchOdd2size === 0) {
      this.matchOdd2size = '';
    }
    let tmp = 0;
    let profit = 0;
    switch (saction) {
      case 'match_odd':
        if (action === '0' || action === '00' || action === '.') {
          if (this.keyFocue === '1' && action !== '.') { // stack and not dot
            this.matchOdd2size = action === '0' ? this.matchOdd2size * 10 : this.matchOdd2size * 100;
          }

          if (this.keyFocue === '0' && action !== '.') { // rate and 0 | 00
            this.matchOdd2price += action;
          }

          if (this.keyFocue === '0' && action === '.') { // rate and .
            this.matchOdd2price += action;
          }

        } else if (action === 'back') { // Back
          if (this.keyFocue === '1') { // stack back
            this.matchOdd2size = Math.floor(this.matchOdd2size / 10);
          } else { // rate back

            this.matchOdd2price = this.matchOdd2price.toString();
            if (!isNaN(this.matchOdd2price)) { // string
              this.matchOdd2price = this.matchOdd2price.substr(0, this.matchOdd2price.length - 1);
            }

            if (this.matchOdd2price === '') {
              this.matchOdd2price = '0';
            }
          }
        } else { // zero to nine
          if (this.keyFocue === '0') {
            if (this.matchOdd2price === '0') {
              this.matchOdd2price = '';
            }
            this.matchOdd2price += action;
          }
          if (this.keyFocue === '1') {
            // let tmp = this.matchOdd2size;
            if (this.matchOdd2size === '0') {
              this.matchOdd2size = '';
            }
            this.matchOdd2size += action;
          }
        }

        if (this.keyFocue === '0') {
          this.limit('matchOdd2price', 5, 'rate');
        }

        if (this.keyFocue === '1') {
          this.limit('matchOdd2size', 9, 'stack');
        }

        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        if (this.matchOdd2size !== '0') {
          tmp = this.matchOdd2price - 1;
          this.matchOdd2profit = tmp * this.matchOdd2size;
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'bookmaker':
        if (action === '0' || action === '00' || action === '.') {
          if (this.keyFocue === '1' && action !== '.') { // stack and not dot
            this.matchOdd2size = action === '0' ? this.matchOdd2size * 10 : this.matchOdd2size * 100;
          }
        } else if (action === 'back') { // Back
          if (this.keyFocue === '1') { // stack back
            this.matchOdd2size = Math.floor(this.matchOdd2size / 10);
          }
        } else { // zero to nine

          if (this.keyFocue === '1') {
            if (this.matchOdd2size === '0') {
              this.matchOdd2size = '';
            }
            this.matchOdd2size += action;
          }
        }
        if (this.keyFocue === '1') {
          this.limit('matchOdd2size', 9, 'stack');
        }

        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        if (this.matchOdd2size !== '0') {
          tmp = this.matchOdd2price / 100;
          this.matchOdd2profit = tmp * Number(this.matchOdd2size);
          this.matchOdd2profit = Math.round(this.matchOdd2profit);
          this.initProfitLoss();
        }
        break;
      case 'fancy':
        if (action === '0' || action === '00' || action === '.') {
          if (this.keyFocue === '1' && action !== '.') { // stack and not dot
            this.matchOdd2size = action === '0' ? this.matchOdd2size * 10 : this.matchOdd2size * 100;
          }

        } else if (action === 'back') { // Back
          if (this.keyFocue === '1') { // stack back
            this.matchOdd2size = Math.floor(this.matchOdd2size / 10);
          }
        } else { // zero to nine

          if (this.keyFocue === '1') {
            // let tmp = this.matchOdd2size;
            if (this.matchOdd2size === '0') {
              this.matchOdd2size = '';
            }
            this.matchOdd2size += action;
          }
        }

        if (this.keyFocue === '1') {
          this.limit('matchOdd2size', 9, 'stack');
        }

        // this.matchOdd2size = Number(action);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * Number(this.matchOdd2size);
        tmp = this.fancy1rate * Number(this.matchOdd2size);
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy2':
        if (action === '0' || action === '00' || action === '.') {
          if (this.keyFocue === '1' && action !== '.') { // stack and not dot
            this.matchOdd2size = action === '0' ? this.matchOdd2size * 10 : this.matchOdd2size * 100;
          }

        } else if (action === 'back') { // Back
          if (this.keyFocue === '1') { // stack back
            this.matchOdd2size = Math.floor(this.matchOdd2size / 10);
          }
        } else { // zero to nine

          if (this.keyFocue === '1') {
            // let tmp = this.matchOdd2size;
            if (this.matchOdd2size === '0') {
              this.matchOdd2size = '';
            }
            this.matchOdd2size += action;
          }
        }

        if (this.keyFocue === '1') {
          this.limit('matchOdd2size', 9, 'stack');
        }

        // this.matchOdd2size = Number(action);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        // tmp = this.matchOdd2price * this.matchOdd2size;
        tmp = this.fancy1rate * this.matchOdd2size;
        profit = tmp / 100;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'fancy3':
        if (action === '0' || action === '00' || action === '.') {
          if (this.keyFocue === '1' && action !== '.') { // stack and not dot
            this.matchOdd2size = action === '0' ? this.matchOdd2size * 10 : this.matchOdd2size * 100;
          }

        } else if (action === 'back') { // Back
          if (this.keyFocue === '1') { // stack back
            this.matchOdd2size = Math.floor(this.matchOdd2size / 10);
          }
        } else { // zero to nine

          if (this.keyFocue === '1') {
            // let tmp = this.matchOdd2size;
            if (this.matchOdd2size === '0') {
              this.matchOdd2size = '';
            }
            this.matchOdd2size += action;
          }
        }

        if (this.keyFocue === '1') {
          this.limit('matchOdd2size', 9, 'stack');
        }

        // this.matchOdd2size = Number(action);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        break;
      case 'jackpot':
        if (action === '0' || action === '00' || action === '.') {
          if (this.keyFocue === '1' && action !== '.') { // stack and not dot
            this.matchOdd2size = action === '0' ? this.matchOdd2size * 10 : this.matchOdd2size * 100;
          }

        } else if (action === 'back') { // Back
          if (this.keyFocue === '1') { // stack back
            this.matchOdd2size = Math.floor(this.matchOdd2size / 10);
          }
        } else { // zero to nine

          if (this.keyFocue === '1') {
            // let tmp = this.matchOdd2size;
            if (this.matchOdd2size === '0') {
              this.matchOdd2size = '';
            }
            this.matchOdd2size += action;
          }
        }

        if (this.keyFocue === '1') {
          this.limit('matchOdd2size', 9, 'stack');
        }
        // this.matchOdd2size = Number(action);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.matchOdd2price - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        // this.initProfitLoss();
        break;
      case 'cricket_casino':
        if (action === '0' || action === '00' || action === '.') {
          if (this.keyFocue === '1' && action !== '.') { // stack and not dot
            this.matchOdd2size = action === '0' ? this.matchOdd2size * 10 : this.matchOdd2size * 100;
          }

        } else if (action === 'back') {// Back
          if (this.keyFocue === '1') {// stack back
            this.matchOdd2size = Math.floor(this.matchOdd2size / 10);
          }
        } else { // zero to nine

          if (this.keyFocue === '1') {
            // let tmp = this.matchOdd2size;
            if (this.matchOdd2size === '0') {
              this.matchOdd2size = '';
            }
            this.matchOdd2size += action;
          }
        }

        if (this.keyFocue === '1') {
          this.limit('matchOdd2size', 9, 'stack');
        }
        // this.matchOdd2size = Number(action);
        this.matchOdd2size < 0 ? this.matchOdd2size = 0 : this.matchOdd2size;
        tmp = this.fancy1rate - 1;
        profit = tmp * this.matchOdd2size;
        this.matchOdd2profit = Math.round(profit);
        // this.initProfitLoss();
        break;

      default:
        this.initProfitLoss();
    }
  }

  initProfitLoss() {
    this.profitLoss1 = 0;
    this.profitLoss2 = 0;
    this.profitLoss3 = 0;

    if (!this.globleColor) {
      this.profitLoss1 += Number(this.matchOdd2profit);
      this.profitLoss2 -= Number(this.matchOdd2size);
      this.profitLoss3 -= Number(this.matchOdd2size);
    }
    if (this.globleColor) {
      this.profitLoss1 -= Number(this.matchOdd2profit);
      this.profitLoss2 += Number(this.matchOdd2size);
      this.profitLoss3 += Number(this.matchOdd2size);
    }

    this.profitLoss1 = Number(this.plArrTmp[this.inx0].profit_loss) + Number(this.profitLoss1);
    this.profitLoss2 = Number(this.plArrTmp[this.inx1].profit_loss) + Number(this.profitLoss2);
    if (this.plArrTmp[2] !== undefined && this.inx2 !== -1) {
      this.profitLoss3 = Number(this.plArrTmp[this.inx2].profit_loss) + Number(this.profitLoss3);
    }
  }

  limit(element, maxChars, type) {
    this[element].toString();
    if (type === 'rate' && !this.validate(this[element])) {
      this[element] = this[element].substr(0, this[element].length - 1);
    }

    // if(type == 'stack' && !this.validateStack(this[element])) {
    //   this[element] =  this[element].substr(0, this[element].length - 1);
    // }

    this[element].toString();
    if (this[element].length > maxChars) {
      // if(this.validate(this[element].value)) {
      this[element] = this[element].substr(0, maxChars);
      // }
    }
  }

  validate(s) {
    const rgx = /^[0-9]*\.?[0-9]*$/;
    const tt = s.match(rgx);
    return s.match(rgx);
  }

  /**
   * PLACE BET
   * @param sessionType MATCH-ODD|MATCH-ODD2|FANCY|FANCY2|LOTTERY
   */

  submitNew() {
    const systemClock = new Date();
    if (systemClock.getTime() - this.lastClickTime < 1000) {
      // this.alert.error('System time-' + systemClock.getTime() + 'Last time-' + this.lastClickTime);
      return;
    }
    this.lastClickTime = systemClock.getTime();
    if (!this.isBetPlace) {
      this.alert.error('Your bet can be on process.. Plz wait or refresh page and try again..');
      return;
    }
    this.activeOdd1Amount = '';
    const data = this.placeBet;
    data.csrf_token = this.csrfToken;
    this.placeBet = data;
    const mType = this.placeBet.slug === 'binary' ? 'binary' : this.placeBet.m_type;
    const newIndexType = this.typeOfBetMarket(mType);
    if (newIndexType && this.matchOdd2size) {
      this.isBetPlace = false;
      this.commonService.setBetStatus(2);
      this.bottomSheetRef.dismiss();
      this.initPlSizeStack(); // UPDATE LATEST STACK AND PRICE AND PROFIT ON BET PLACE OBJECT.
      const arrObj = this.placeBet;
      this.placeBet = {};
      if (newIndexType === 1 || newIndexType === 2) {
        if (newIndexType === 1 && (arrObj.rate === 1.00 || arrObj.rate === 0)) { // ALL BETFAIR MARKET
          this.commonService.setBetStatus(3); // CLOASE LOADER ON SCREEN
          this.alert.error('Bet cancelled can`t place, rate is like 0 or 1.00');
          return;
        }

        /***sorry**/
        if (newIndexType === 2 && (this.marketsOdds.runners[this.inx0].ballrunning === 1 || this.marketsOdds.runners[this.inx0].suspended === 1)) { // BOOKMAKER
          this.commonService.setBetStatus(3);  // CLOASE LOADER ON SCREEN
          let msg = 'Ball Running';
          if (this.marketsOdds.runners[this.inx0].suspended) {
            msg = 'Suspended';
          }
          this.alert.error('Bet cancelled can`t place, ' + msg);
          return;
        }
      } else if (newIndexType === 3) { // FANCY
        arrObj.size = this.matchOdd2size;
        if (this.marketsOdds.ball_running === 1 || this.marketsOdds.suspended === 1) {
          this.commonService.setBetStatus(3);  // CLOASE LOADER ON SCREEN
          let msg = 'Ball Running';
          if (this.marketsOdds.suspended) {
            msg = 'Suspended';
          }
          this.alert.error('Bet cancelled can`t place, ' + msg);
          return;
        }
      } else if (newIndexType === 4) { // KHADO AND BALLBYBALL
        if (this.marketsOdds.ball_running === 1 || this.marketsOdds.suspended === 1) {
          this.commonService.setBetStatus(3);  // CLOASE LOADER ON SCREEN
          let msg = 'Ball Running';
          if (this.marketsOdds.suspended) {
            msg = 'Suspended';
          }
          this.alert.error('Bet cancelled can`t place, ' + msg);
          return;10.50
        }
        arrObj.size = this.matchOdd2size;
        arrObj.rate = this.fancy1rate;
        let rateData = 0;
        let runData = 0;
        rateData = arrObj.bet_type === 'no' ? this.marketsOdds.no_rate : this.marketsOdds.yes_rate;
        runData = arrObj.bet_type === 'no' ? this.marketsOdds.no : this.marketsOdds.yes;
        if (arrObj.m_type === 'khado') {
          runData = this.matchOdd2price;
          arrObj.price = this.matchOdd2price;
        }
        if (arrObj.m_type === 'ballbyball' && (Number(this.fancy1rate) !== Number(rateData) || Number(this.matchOdd2price) !== Number(runData))) {
          this.commonService.setBetStatus(3);  // CLOASE LOADER ON SCREEN
          this.alert.error('Bet cancelled can`t place due to rate or run changed.');
          return;
        }
      } else if (newIndexType === 5) {
        if (this.marketsOdds.ball_running === 1 || this.marketsOdds.suspended === 1) {
          this.commonService.setBetStatus(3);  // CLOASE LOADER ON SCREEN
          let msg = 'Ball Running';
          if (this.marketsOdds.suspended) {
            msg = 'Suspended';
          }
          this.alert.error('Bet cancelled can`t place, ' + msg);
          return;
        }
        let rateData = 0;
        rateData = arrObj.bet_type === 'back' ? this.marketsOdds.back : this.marketsOdds.lay;
        if (arrObj.m_type === 'oddeven') {
          rateData = arrObj.bet_type === 'back' ? this.marketsOdds.odd : this.marketsOdds.even;
        }
        this.fancy1rate = this.matchOdd2price;
        arrObj.size = this.matchOdd2size;
        arrObj.rate = this.fancy1rate;

        if (Number(this.fancy1rate) !== Number(rateData)) {
          this.commonService.setBetStatus(3);  // CLOASE LOADER ON SCREEN
          this.alert.error('Bet cancelled can`t place due to rate changed.');
          return;
        }

      } else if (newIndexType === 6) {
        if (this.marketsOdds.ball_running === 1 || this.marketsOdds.suspended === 1) {
          this.commonService.setBetStatus(3);  // CLOASE LOADER ON SCREEN
          let msg = 'Ball Running';
          if (this.marketsOdds.suspended) {
            msg = 'Suspended';
          }
          this.alert.error('Bet cancelled can`t place, ' + msg);
          return;
        }
        arrObj.size = this.matchOdd2size;

      } else if (newIndexType === 7) {
        data.size = this.matchOdd2size;
        if (arrObj === 'jackpot') {
          data.rate = this.matchOdd2price;
          data.profit = this.matchOdd2profit;
        }
      }
      this.callApi(arrObj);
    } else {
      this.alert.error('Please enter stack.. try again.');
    }
  }

  callApi(data) {
    this.betServie.betPlace(data).subscribe((response: any) => {
      this.isBetPlace = true; // BET SUBMIT BUTTON ON
      this.commonService.setBetStatus(3); // CLOSE LOADER
      if (response.status === 1) {
        if (response.data) {
          this.alert.success(response.message);
          this.commonService.setBalanceChange(response.data.balance.balance);
          this.commonService.setExposeChange(response.data.balance.expose);
        } else {
          this.alert.error(response.message);
        }
      }
      if (response.status === 0) {
        this.alert.error(response.message);
        if (response.data) {
          this.commonService.setBalanceChange(response.data.balance.balance);
          this.commonService.setExposeChange(response.data.balance.expose);
        }
      }

      this.afterBetComplatedToClearVar(); // CLEAR VARIABLES RELATED ON BETS
    }, (error) => {
      this.commonService.setBetStatus(3); // ON ERROR CATCH TO BE CLOSE LOADER
      this.alert.error('Some thing wrong..!!');
      this.isBetPlace = true; // BET SUBMIT BUTTON ON
    });
  }

  initPlSizeStack() {
    this.placeBet.price = this.matchOdd2price;
    this.placeBet.size = this.matchOdd2size;
    this.placeBet.profit = this.matchOdd2profit;
  }

  afterBetComplatedToClearVar() {
    this.globleColor = false;
    this.profitLoss1 = '';
    this.profitLoss2 = '';
    this.profitLoss3 = '';
    this.matchOdd2price = '';
    this.matchOdd2size = '';
    this.matchOdd2profit = -1;
    this.classN = '';
  }

  typeOfBetMarket(type) {
    if (type === 'match_odd' || type === 'completed_match'
      || type === 'tied_match' || type === 'winner'
      || type === 'goals' || type === 'set_market'
      || type === 'virtual_cricket') {
      this.setRate(1);
      return 1;
    } else if (type === 'bookmaker') {
      this.setRate(2);
      return 2;
    } else if (type === 'fancy') {
      return 3;
    } else if (type === 'khado' || type === 'ballbyball') {
      return 4;
    } else if (type === 'fancy3' || type === 'oddeven') {
      return 5;
    } else if (type === 'fancy2' || type === 'meter' || type === 'binary') {
      return 6;
    } else if (type === 'jackpot' || type === 'cricket_casino') {
      return 7;
    }
  }

  intToString(item) {
    const ii = Number(item);
    if (ii > 999 && ii < 99999) {
      if (ii > 9999) {
        return item.substring(0, 2) + 'K';
      } else {
        return item.substring(0, 1) + 'K';
      }
    }

    if (ii > 99999 && ii < 9999999) {
      if (ii > 999999) {
        return item.substring(0, 2) + 'Lac';
      } else {
        return item.substring(0, 1) + 'Lac';
      }
    }

    if (ii > 9999999) {
      if (ii > 99999999) {
        return item.substring(0, 2) + 'Cr';
      } else {
        return item.substring(0, 1) + 'Cr';
      }
    }
    if (ii < 999) {
      return item;
    }
  }

  tokenGenerate() {
    this.betServie.getBetToken().subscribe((response: any) => {
      if (response.status === 1) {
        this.csrfToken = response.token;
      } else {
        this.csrfToken = '0';
      }
    }, (error: any) => {
      this.alert.error('Some thing wrong..!!');
    });
  }

  setRate(type) {
    if (type === 1 && this.marketsOdds && this.marketsOdds.odds[this.inx0] && this.placeBet.rate) {
      if (this.placeBet.bet_type === 'back') {
        this.placeBet.rate = this.marketsOdds.odds[this.inx0].backPrice1;
        return true;
      }
      this.placeBet.rate = this.marketsOdds.odds[this.inx0].layPrice1;
      return  true;
    }
    if (type === 2 && this.marketsOdds && this.marketsOdds.runners[this.inx0] && this.placeBet.rate) {
      if (this.placeBet.bet_type === 'back') {
        this.placeBet.rate = this.marketsOdds.runners[this.inx0].back;
        return true;
      }
      this.placeBet.rate = this.marketsOdds.runners[this.inx0].lay;
      return  true;
    }
  }
}
