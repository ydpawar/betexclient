import {Component, OnInit, PLATFORM_ID, Injector, Inject, NgZone, APP_ID, ChangeDetectorRef, HostListener} from '@angular/core';
import {TransferState, makeStateKey, Title, Meta} from '@angular/platform-browser';
import {isPlatformBrowser, isPlatformServer, Location} from '@angular/common';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import * as moment from 'moment';
import swal from 'sweetalert2';
import {EnvService} from '../../core/services';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'parent-comp',
  template: ``,
  providers: []
})

export class BaseComponent {

  public activatedRoute: ActivatedRoute;
  public routeUrl: any;
  public routeUrlDisplay: string = 'block';
  public titleService: Title;
  public metaService: Meta;
  public platformId: any;
  public appId: any;
  public router: Router;
  public baseUrl;
  public changedRef: ChangeDetectorRef;
  public spinner: NgxSpinnerService;
  public sysConfigDt: EnvService;
  public innerWidth: any;
  public platformName: string;
  public loc: Location;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    this.setPlatfrom();
  }

  constructor(injector: Injector) { // , @Inject(DOCUMENT) private document: Document
    this.innerWidth = window.innerWidth;
    this.router = injector.get(Router);
    this.platformId = injector.get(PLATFORM_ID);
    this.appId = injector.get(APP_ID);
    this.titleService = injector.get(Title);
    this.metaService = injector.get(Meta);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.changedRef = injector.get(ChangeDetectorRef);
    this.spinner = injector.get(NgxSpinnerService);
    this.sysConfigDt = injector.get(EnvService);
    this.loc = injector.get(Location);
    this.onResize();
    this.setPlatfrom();
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.routeUrl = event.urlAfterRedirects;
        this.routeUrlDisplay = 'block';
        const routerTmp = event.urlAfterRedirects.split('/');
        if (routerTmp.constructor === Array && routerTmp.length > 1 && routerTmp[1] !== 'home'
          && (routerTmp[1] === 'details' || routerTmp[1] === 'election-details' || routerTmp[1] === 'binary-details' || routerTmp[1] === 'cc-details' || routerTmp[1] === 'teenpatti')) {
          this.routeUrl = routerTmp[1];
          this.routeUrlDisplay = 'none';
          this.removeBodyClass('sidebar-enable');
        }
        if (routerTmp.constructor === Array && routerTmp.length > 1 && routerTmp[2] === 'casino' && routerTmp[3] === 'detail') {
          this.routeUrlDisplay = 'none';
          this.removeBodyClass('sidebar-enable');
        }
      }
    });
  }

  // *************************************************************//
  // @Purpose : To check server or browser
  // *************************************************************//
  isBrowser() {
    if (isPlatformBrowser(this.platformId)) {
      return true;
    } else {
      return false;
    }
  }

  // *************************************************************//
  // @Purpose : We can use following function to use localstorage
  // *************************************************************//
  setPlatfrom() {
    if (isPlatformBrowser(this.platformId)) {
      if (this.innerWidth < 992) {
        this.platformName = 'mobile';
      } else {
        this.platformName = 'desktop';
      }
    }
  }

  // *************************************************************//
  // @Purpose : We can use following function to use localstorage
  // *************************************************************//
  setToken(key, value) {
    if (isPlatformBrowser(this.platformId)) {
      // window.localStorage.setItem(key, value);
      sessionStorage.setItem(key, value);
    }
  }

  getToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      // return window.localStorage.getItem(key);
      return sessionStorage.getItem(key);
    }
  }

  removeToken(key) {
    if (isPlatformBrowser(this.platformId)) {
      // window.localStorage.removeItem(key);
      sessionStorage.removeItem(key);
    }
  }

  clearToken() {
    if (isPlatformBrowser(this.platformId)) {
      // window.localStorage.clear();
      sessionStorage.clear();
    }
  }

  // *************************************************************//

  // *************************************************************//
  // @Purpose : We can use following function to use Toaster Service.
  // *************************************************************//
  popToast(type, title) {
    swal.fire({
      position: 'center',
      // type: type,
      text: title,
      showConfirmButton: false,
      timer: 3000,
      // customClass: 'custom-toaster'
    });
  }

  /****************************************************************************
   @PURPOSE      : To restrict or allow some values in input.
   @PARAMETERS   : $event
   @RETURN       : Boolen
   ****************************************************************************/
  RestrictSpace(e) {
    if (e.keyCode == 32) {
      return false;
    } else {
      return true;
    }
  }

  AllowNumbers(e) {
    let input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    if (e.which === 43 || e.which === 45) {
      return true;
    }
    if (e.which === 36 || e.which === 35) {
      return true;
    }
    if (e.which === 37 || e.which === 39) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);
  }

  /****************************************************************************/
  getProfile() {
    const url = this.getToken('ss_pic');
    if (url == null || url === ' ') {
      return 'assets/images/NoProfile.png';
    } else {
      return url;
    }
  }

  /****************************************************************************
   //For COOKIE
   /****************************************************************************/
  setCookie(name, value, days) {
    let expires = '';
    if (days) {
      const date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = '; expires=' + date.toUTCString();
    }
    document.cookie = name + '=' + (value || '') + expires + '; path=/';
  }

  getCookie(name) {
    const nameEQ = name + '=';
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) == 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  }

  eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
  }

  /****************************************************************************
   //For Side menu toggle
   /****************************************************************************/
  slideLeft() {
    $('body').addClass('slide-open');
  }

  removeSlide() {
    $('body').removeClass('slide-open');
  }

  slideClose() {
    $('body').removeClass('slide-open');
  }

  /****************************************************************************/
  convertTimestamp(timeStamp) {
    // debugger
    return moment(timeStamp).format('lll');
  }

  getTodayDate() {
    return moment().format('YYYY-MM-DD');
  }

  /****************************************************************************
   //For SLUG TO TITLE CONVERT
   /****************************************************************************/
  generateTitle(slug: string) {
    const words = slug.split('-');
    for (let i = 0; i < words.length; i++) {
      const word = words[i];
      words[i] = word.charAt(0).toUpperCase() + word.slice(1);
    }
    return words.join(' ');
  }

  arraySorting(data) {
    // console.log(data);
    return data.sort((a, b) => (a.name > b.name) ? 1 : -1);
  }

  /****************************************************************************
   //For GET LAST CHAR
   /****************************************************************************/
  getLastChar(id: any) {
    return id.substr(id.length - 6); // => "1"
  }

  /****************************************************************************
   //For REMOVE BODY CLASS
   /****************************************************************************/
  removeBodyClass(classname: string) {
    const body = document.getElementsByTagName('body')[0]; // REMOVE OPEN CLASS IN BODY TAG
    body.classList.remove(classname);
  }

  /****************************************************************************
   //For BET STACK BUTTON SHOT CUT
   /****************************************************************************/
  setStackShortcut(data) {
    const tmp = data.replace(/\s/g, '').split(',');
    const newArr = new Array();
    if (tmp.length > 0) {
      tmp.forEach(item => {
        const ii = parseInt(item);
        if (ii > 999 && ii < 99999) {
          if (ii > 9999) {
            newArr.push({key: item.substring(0, 2) + 'K', value: item});
          } else {
            newArr.push({key: item.substring(0, 1) + 'K', value: item});
          }
        }

        if (ii > 999 && ii > 99999) {
          if (ii > 999999) {
            newArr.push({key: item.substring(0, 2) + 'Lac', value: item});
          } else {
            newArr.push({key: item.substring(0, 1) + 'Lac', value: item});
          }
        }

        if (ii <= 999) {
          newArr.push({key: item, value: item});
        }
      });
      return newArr;
    }
  }

  capitalizeFirstLetter(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  /****************************************************************************
   //For BOOK CALCULATION
   /****************************************************************************/
  fancyBookCal(betList) {
    try {
      let dataReturn = [];
      const newbetresult = [];
      const betresult = [];
      const result = [];

      if (betList.length) { // betList != null
        let min = 0;
        let max = 0;
        let index = 0;
        betList.forEach((item) => {
          const priceItem = Number(item.price);
          betresult.push({price: priceItem, bType: item.bType, loss: item.loss, win: item.win});
          if (index === 0) {
            min = priceItem;
            max = priceItem;
          }
          if (min > priceItem) {
            min = priceItem;
          }
          if (max < priceItem) {
            max = priceItem;
          }
          index++;
        });

        min = min - 1;
        max = max + 1;
        const betarray = [];
        let betType = '';
        const win = 0;
        let loss = 0;
        let count1 = min;
        const totalbetcount1 = betresult.length;

        betresult.forEach(($value) => {
          const val = $value.price - count1;
          const minval = $value.price - min;
          const maxval = max - $value.price;
          betType = $value.bType;
          loss = $value.loss;
          const newresult = [];
          let top, bottom, profitcount1, losscount1 = 0;

          for (let i = 0; i < minval; i++) {
            if (betType === 'no') {
              top = top + $value.win;
              profitcount1++;
              newresult.push({
                count: count1,
                price: $value.price,
                bType: $value.bType,
                totalbetcount: totalbetcount1,
                expose: $value.win
              });
            } else {
              bottom = bottom + $value.loss;
              losscount1++;
              newresult.push({
                count: count1,
                price: $value.price,
                bType: $value.bType,
                totalbetcount: totalbetcount1,
                expose: -$value.loss
              });
            }
            count1++;
          }

          for (let i = 0; i <= maxval; i++) {
            if (betType === 'no') {
              newresult.push({
                count: count1,
                price: $value.price,
                bType: $value.bType,
                totalbetcount: totalbetcount1,
                expose: -$value.loss
              });
              bottom = bottom + $value.loss;
              losscount1++;
            } else {
              top = top + $value.win;
              profitcount1++;
              newresult.push({
                count: count1,
                price: $value.price,
                bType: $value.bType,
                totalbetcount: totalbetcount1,
                expose: $value.win
              });
            }
            count1++;
          }
          result.push({
            count: $value.price,
            bType: $value.bType,
            profit: top,
            loss: bottom,
            profitcount: profitcount1,
            losscount: losscount1,
            newarray: newresult
          });

        });

        // create range
        const newbetarray = [];
        const newbetresult = [];
        const totalmaxcount = max - min;

        if (totalmaxcount > 0) {
          let minstart = min;
          for (let i = 0; i < totalmaxcount; i++) {
            const newbetarray1 = [];
            let finalexpose1 = 0;
            for (let x = 0; x < totalbetcount1; x++) {
              const expose1 = result[x].newarray[i].expose;
              finalexpose1 = finalexpose1 + expose1;
              newbetarray1.push({bet_price: result[x].count, bType: result[x].bType, expose: expose1});
            }
            dataReturn.push({
              price: minstart,
              profitLoss: Math.round(finalexpose1)
            });

            minstart++;
            newbetarray.push({exposearray: newbetarray1, finalexpose: finalexpose1});
          }
        }
      }

      if (dataReturn.length) { // if (dataReturn != null) {
        const dataReturnNew = [];
        let i = 0;
        let start = 0;
        let startPl = 0;
        let index = 0;
        let end = 0;
        dataReturn.forEach((item) => {
          if (index === 0) {
            dataReturnNew.push({
              price: item.price + ' or less', profitLoss: item.profitLoss
            });
          } else {
            if (startPl !== item.profitLoss) {
              if (end !== 0) {
                let priceVal: any = 0;
                if (start === end) {
                  priceVal = start;
                } else {
                  priceVal = start + ' - ' + end;
                }
                dataReturnNew.push({price: priceVal, profitLoss: startPl});
              }
              start = item.price;
              end = item.price;
            } else {
              end = item.price;
            }
            if (index === dataReturn.length - 1) {
              dataReturnNew.push({price: start + ' or more', profitLoss: item.profitLoss});
            }
          }
          startPl = item.profitLoss;
          i++;
          index++;
        });
        dataReturn = dataReturnNew;
      }
      return dataReturn;

    } catch (e) {
      console.log('Fancy Book', e);
      return null;
    }
  }

  getProfitLossOtherMarket(betListData: any) {
    try {
      if (betListData === null) {
        return null;
      }
      let yes_sum = 0;
      let yes_loss = 0;
      let no_win = 0;
      let no_loss = 0;

      betListData.forEach((bitem) => {
        if (bitem.bType === 'back') {
          yes_sum += bitem.win;
          yes_loss += bitem.loss;
        } else {
          no_win += bitem.win;
          no_loss += bitem.loss;
        }
      });

      const yesProfitloss = Math.round(yes_sum - no_loss);
      const noProfitloss = Math.round(no_win - yes_loss);
      let betList = null;
      if (betListData != null) {
        betList = betListData;
      }

      return {'yes_profitloss': yesProfitloss, 'no_profitloss': noProfitloss};
    } catch (e) {
      console.log('OTHER-MARKET-BOOK', e);
      return;
    }
  }

  getProfitLossKhado(betList: any) {
    try {
      const dataReturn = [];
      const result = [];
      const newbetresult = [];
      const betresult = [];
      if (betList != null) {
        let min = 0;
        let max = 0;
        let index = 0;
        betList.forEach((itemB) => {
          const itemPrice = Number(itemB.price);
          const itemDiff = Number(itemB.diff);
          betresult.push({
            price: itemPrice,
            bet_type: itemB.bType,
            loss: itemB.loss,
            win: itemB.win,
            difference: itemDiff
          });
          console.log(betresult);
          if (index === 0) {
            min = itemPrice;
            max = itemDiff;
          }
          if (min > itemPrice) {
            min = itemPrice;
          }
          if (max < itemDiff) {
            max = itemDiff;
          }
          index++;
        });


        min = min - 20;
        if (min < 0) {
          min = 0;
        }
        // $max = $max+1;
        max = max + 20;
        // let betarray = [];
        // let bet_type = '';
        // let win = 0;
        // let loss = 0;
        // let count = min;
        // let lossVal1 = 0;
        // let totalbetcount = betresult.length;

        for (let i = min; i <= max; i++) {
          let winVal = 0;
          let lossVal = 0;
          const currentVal = Number(i);

          betList.forEach((itemB) => {
            if (Number(itemB.diff) > currentVal && Number(itemB.price) <= currentVal) {
              winVal += Number(itemB.win);
            }

            if (Number(itemB.diff) <= currentVal || Number(itemB.price) > currentVal) {
              lossVal += Number(itemB.loss);
            }
          });

          const total = winVal - lossVal;
          dataReturn.push({
            price: i,
            profitLoss: Math.round(total)
          });
        }

      }
      return dataReturn;
    } catch (e) {
      console.log('KHADO MARKET BOOK', e);
      return;
    }
  }

  getProfitLossLotteryBook(betList: any) {
    try {
      const price = [];
      if (betList !== null) {
        for (let n = 0; n < 10; n++) {
          let profitLoss1 = 0;
          let totalWin = 0;
          let totalLoss = 0;
          betList.forEach((res) => {
            if (res.secId === n) {
              totalWin += Number(res.win);
            }

            if (res.secId !== n) {
              totalLoss += (-1) * Number(res.loss);
            }
            profitLoss1 = Math.round(totalWin + totalLoss);
          });
          price.push({
            price: n,
            'profitLoss': profitLoss1
          });
        }
      }
      return price;
    } catch (e) {
      console.log('LOTTERY MARKET BOOK', e);
      return;
    }
  }

}
